
$.datepicker.setDefaults($.datepicker.regional["it"]);

$.ui.tooltip.prototype.options.position = {my: 'left bottom', at: 'center-10 top-10'};
$.ui.tooltip.prototype.options.hide = false;
$.ui.tooltip.prototype.options.show = false;
$.ui.tooltip.prototype.options.content = function() { return $(this).attr('title'); };