
String.prototype.startsWith = function(string) {
    if(typeof string === "string") return this.indexOf(string) == 0
    else return false
};

var updateParameter = function(key, value) {
    var url = document.location.href;
    var re = new RegExp("([?|&])" + key + "=.*?(&|#|$)(.*)", "gi");
    if(re.test(url)) {
        if (typeof value !== 'undefined' && value !== null) return url.replace(re, '$1' + key + "=" + value + '$2$3');
        else return url.replace(re, '$1$3').replace(/(&|\?)$/, '');
    } else {
        if (typeof value !== 'undefined' && value !== null) {
            var separator = url.indexOf('?') !== -1 ? '&' : '?', hash = url.split('#');
            url = hash[0] + separator + key + '=' + value;
            if (hash[1]) url += '#' + hash[1];
            return url;
        } else return url;
    }
};