package libromatricola

import groovy.xml.*

class HtmlBuilder extends MarkupBuilder {
	
    HtmlBuilder(writer) {
        super(writer)
        omitNullAttributes = true
    }
    
}

