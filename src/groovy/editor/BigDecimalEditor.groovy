package editor

import java.beans.*
import java.text.*

class BigDecimalEditor extends PropertyEditorSupport {

    String getAsText() { return getValue().toString() }

    void setAsText(String number) throws IllegalArgumentException {
        if(number == "null" || number == "") setValue(null)
        else {
            def df = DecimalFormat.instance
            df.setParseBigDecimal(true)
            setValue(df.parse(number))
        }
    }

}
