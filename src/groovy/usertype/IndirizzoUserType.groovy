package usertype

import libromatricola.*
import org.hibernate.*
import org.hibernate.engine.*
import org.hibernate.type.*
import java.sql.*

class IndirizzoUserType extends MutableType {
	
    static final int INDIRIZZO = 0
    static final int CITTA = 1
    static final int CAP = 2
    static final int PROVINCIA = 3
    static final StringType sti = StringType.INSTANCE
    
    String[] getPropertyNames() { ['indirizzo', 'citta', 'cap', 'provincia'] }
	
    Object deepCopy(Object value) {
        if(value instanceof Indirizzo) return new Indirizzo(indirizzo: value.indirizzo, citta: value.citta, cap: value.cap, provincia: value.provincia)
        else return value
    }
    
    Type[] getPropertyTypes() { [sti, sti, sti, sti] }
    
    Object getPropertyValue(Object component, int index) {
        if(component == null) return null
        def indirizzo = (Indirizzo)component
        if(index == INDIRIZZO) return indirizzo.indirizzo
        else if(index == CITTA) return indirizzo.citta
        else if(index == CAP) return indirizzo.cap
        else if(index == PROVINCIA) return indirizzo.provincia
        else throw new HibernateException("Indice non valido [$index] per il tipo Indirizzo")
    }
    
    void setPropertyValue(Object component, int index, Object value) {
        if(component == null) return
        def indirizzo = (Indirizzo)component
        if(index == INDIRIZZO) indirizzo.indirizzo = (String)value
        else if(index == CITTA) indirizzo.citta = (String)value
        else if(index == CAP) indirizzo.cap = (String)value
        else if(index == PROVINCIA) indirizzo.provincia = (String)value
        else throw new HibernateException("Indice non valido [$index] per il tipo Indirizzo")
    }
    
    Object nullSafeGet(ResultSet rs, String[] names, SessionImplementor session, Object owner) {
        assert names.length == 4
        def indirizzo = sti.get(rs, names[INDIRIZZO])
        def citta = sti.get(rs, names[CITTA])
        def cap = sti.get(rs, names[CAP])
        def provincia = Provincia.get(sti.get(rs, names[PROVINCIA]))
        return new Indirizzo(indirizzo: indirizzo, citta: citta, cap: cap, provincia: provincia)
    }
    
    void nullSafeSet(PreparedStatement st, Object value, int index, SessionImplementor session) {
        if(value == null) {
            sti.set(st, null, index + INDIRIZZO)
            sti.set(st, null, index + CITTA)
            sti.set(st, null, index + CAP)
            sti.set(st, null, index + PROVINCIA)
        } else {
            def indirizzo = (Indirizzo)value
            sti.set(st, indirizzo.indirizzo, index + INDIRIZZO)
            sti.set(st, indirizzo.citta, index + CITTA)
            sti.set(st, indirizzo.cap, index + CAP)
            sti.set(st, indirizzo.provincia?.sigla, index + PROVINCIA)
        }
    }
    
    Class returnedClass() { Indirizzo.class }
    
}

