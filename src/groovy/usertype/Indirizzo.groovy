package usertype

import libromatricola.*

class Indirizzo {
	
    String indirizzo, citta, cap
    Provincia provincia
    
    String toString() { "$indirizzo, $citta $cap ($provincia.sigla)" }
}

