package libromatricola

import excel.reader.*
import polizze.Applicazione
import polizze.Flusso
import utenti.Log

class AnnullamentiController {
    def applicazioniService
    def index() {}
    def annullamenti(){
       //EsportazioneJob.triggerNow()
    }
    def generaFlussoiassicurAnnullaDaExcel() {
        def utente = session.utente
        def elenco = request.getFile("elenco").bytes
        def values = []
        def tracciati=[]
        def fogli=[]
        def resultTra = [tracciati: [], error: [], totaleTracci:0, totaleError:0]
        def elencoTracc=""
        def elencoError=""
        def listTracc=""
        def listErrorF=""
        def listError=""
        def logg=""
        if(elenco) {
            logg =new Log(parametri: "file excel caricato per generare tracciati", operazione: "generazione tracciati annullamenti tramite excel", pagina: "Annullamenti da Excel", utente:utente )
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            InputStream myInputStream = new ByteArrayInputStream(elenco)
            try {
                boolean tasse=false
                def rowData = [:]
                def wb= ExcelReader.readXlsx(myInputStream){
                    sheet (0) {
                        rows(from: 1) {
                            rowData = [:]
                                rowData.nome=cell("B")?.value?:""
                                rowData.numero=cell("C")?.value?:""
                                rowData.targa=cell("E")?.value?:""
                                rowData.inizio=cell("F")?.value?:""
                                rowData.dataAnnullo=cell("G")?.value?:""
                                rowData.fine=cell("H")?.value?:""
                                rowData.premio=cell("L")?.value?:0.0
                                rowData.imponibile=cell("N")?.value?:0.0
                                rowData.tasse=0.0
                                rowData.ppslip=cell("P")?.value?:0.0
                                values<<rowData
                                logg =new Log(parametri: "foglio tasse non rimborsate--> dati:${rowData}", operazione: "generazione tracciati annullamenti tramite excel", pagina: "Annullamenti da Excel", utente:utente )
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        }
                        if(rowData.containsValue("DRE:INIZIO") || rowData.containsValue("DRE:DATAANNULLO") || rowData.containsValue("NOME") || rowData.containsValue("NUMERO") || rowData.isEmpty()  ){
                            logg =new Log(parametri: "foglio tasse non rimborsate vuoto", operazione: "generazione tracciati annullamenti tramite excel", pagina: "Annullamenti da Excel", utente:utente )
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            resultTra.error<<("foglio tasse non rimborsate vuoto,")

                        }else{
                            tracciati<<applicazioniService.generaTracciatoAnnullamento(values, true, true,tasse)
                        }
                    }
                    sheet(1){
                        tasse=true
                        values=[]
                        rows(from: 1) {
                            rowData = [:]
                            rowData.nome=cell("B")?.value?:""
                            rowData.numero=cell("C")?.value?:""
                            rowData.targa=cell("E")?.value?:""
                            rowData.inizio=cell("F")?.value?:""
                            rowData.dataAnnullo=cell("G")?.value?:""
                            rowData.fine=cell("H")?.value?:""
                            rowData.premio=cell("L")?.value?:0.0
                            rowData.imponibile=cell("O")?.value?:0.0
                            rowData.tasse=cell("P")?.value?:0.0
                            rowData.ppslip=cell("R")?.value?:0.0
                            values<<rowData
                            logg =new Log(parametri: "foglio tasse rimborsate--> dati:${rowData}", operazione: "generazione tracciati annullamenti tramite excel", pagina: "Annullamenti da Excel", utente:utente )
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        }
                        if(rowData.containsValue("DRE:INIZIO") || rowData.containsValue("DRE:DATAANNULLO") || rowData.containsValue("NOME") || rowData.containsValue("NUMERO") || rowData.isEmpty()  ){
                            logg =new Log(parametri: "foglio tasse rimborsate vuoto", operazione: "generazione tracciati annullamenti tramite excel", pagina: "Annullamenti da Excel", utente:utente )
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            resultTra.error<<("foglio tasse rimborsate vuoto,")
                        }else{
                            tracciati<<applicazioniService.generaTracciatoAnnullamento(values, true, true,tasse)
                        }
                    }
                }
                fogli<<resultTra
                if (listErrorF!="") {
                    elencoError=fogli.error.flatten()
                    if(elencoError?.size()>0){
                        for (String s : elencoError)
                        {
                            listErrorF += s + "\r\n\r\n"
                        }
                        flash.error=listErrorF
                    }
                }
                if(tracciati){
                    elencoTracc=tracciati.tracciati
                    elencoError=tracciati.error
                    def totaleT=tracciati.totaleTracci.flatten().sum()
                    def totaleE=tracciati.totaleError.flatten().sum()
                    elencoTracc=elencoTracc.flatten()
                    elencoError=elencoError.flatten()
                    if(elencoTracc?.size()>0){
                        for (String s : elencoTracc)
                        {
                            listTracc += s + "\r\n\r\n"
                        }
                        flash.message=listTracc+="totale flussi generati ${totaleT}"
                        applicazioniService.uploadFlussoEsclusioni(utente)
                    }
                    if(elencoError?.size()>0){
                        for (String s : elencoError)
                        {
                            listError += s + "\r\n\r\n"
                        }
                        if (listErrorF!="") {
                            flash.error+=listError+="totale flussi non generati ${totaleE}"
                        }else{
                            flash.error=listError+="totale flussi non generati ${totaleE}"

                        }
                    }
                }


            } catch(e) {
                println "errore ${e.toString()}"
                flash.error = "Errore generazione flussi Annullamenti"
            }
        } else flash.error = "Specificare l'elenco di polizza da annullare"
        redirect action: "annullamenti"
    }


}
