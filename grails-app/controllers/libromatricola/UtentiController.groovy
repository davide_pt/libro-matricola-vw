package libromatricola

import utenti.*
import polizze.*

class UtentiController {

    static defaultAction = "elenco"

    def elenco() {
        def cerca = "%${params.q ?: ''}%"
        def count = Dealer.createCriteria().count {
            or {
                like 'codice', cerca
                like 'numeroLibroMatricola', cerca
                like 'ragioneSociale', cerca
                like 'partitaIva', cerca
            }
        }
        params.sort = params.sort ?: 'ragioneSociale'
        params.order = params.order ?: 'asc'
        params.max = Math.abs(Math.min(Math.min(params.int('max', 20), 100), count))
        params.offset = Math.abs(params.int('offset', 0))
        if(params.offset > 0 && params.offset >= count) params.offset = count - params.max
        def dealers = Dealer.createCriteria().list(params) {
            or {
                like 'codice', cerca
                like 'numeroLibroMatricola', cerca
                like 'ragioneSociale', cerca
                like 'partitaIva', cerca
            }
        }
        [dealers: dealers]
    }

    def cancella(long id) {
        if(Dealer.exists(id)) {
            def dealer = Dealer.get(id)
            try {
                dealer.delete()
                flash.message = "Dealer '$dealer' cancellato correttamente"
            } catch(e) { flash.error = "Si è verificato un errore imprevisto: $e.message" }
        } else flash.info = "Il dealer non esiste più"
        redirect action: 'elenco', params: params
    }

    def crea() {
        def dealer = new Dealer()
        render view: 'editor', model: [dealer: dealer, provincie: Provincia.list(), tariffe: Tariffa.list()]
    }

    def modifica(long id) {
        if(Dealer.exists(id)) {
            def dealer = Dealer.read(id)
            render view: 'editor', model: [dealer: dealer, provincie: Provincia.list(), tariffe: Tariffa.list()]
        } else {
            flash.info = "Il dealer non esiste più"
            redirect action: 'elenco', params: params
        }
    }

    def salva(long id) {
        def dealer = Dealer.exists(id) ? Dealer.get(id) : new Dealer()
        if(request.post) {
            if(params.password == '') params.remove('password')
            bindData(dealer, params)
            if(dealer.save()) {
                flash.message = "Dealer '$dealer' ${id ? 'modificato' : 'creato'} correttamente"
                redirect action: 'elenco', params: params.findAll { k, v -> k in ['max', 'offset', 'sort', 'order', 'q'] }
            } /*else println dealer.errors*/
        }
        render view: 'editor', model: [dealer: dealer, provincie: Provincia.list(), tariffe: Tariffa.list()]
    }

    def blocca(long id, boolean bloccato) {
        if(Dealer.exists(id)) {
            def dealer = Dealer.get(id)
            dealer.bloccato = bloccato
            if(dealer.save()) flash.message = "Dealer '$dealer' ${bloccato ? '' : 's'}bloccato correttamente"
            else flash.error = dealer.errors.collect { g.message(error: it) }.join('<br>')
        } else flash.info = "Il dealer non esiste più"
        redirect action: 'elenco', params: params
    }

    def login(String username, String password) {
        if(request.post) {
            def utente = Utente.findByUsernameAndPassword(username, password.encodeAsMD5())
            if(utente) {
                if(utente.bloccato) flash.error = "L'accesso per l'utente '$utente' è stato bloccato"
                else {
                    session.utente = utente
                    redirect controller: "applicazioni", action: "elenco"
                }
            } else flash.error = 'Credenziali di accesso errate o inesistenti'
        }
        render view: 'login', layout: false
    }

    def logout() {
        session.invalidate()
        redirect action: 'login'
    }

}
