package libromatricola

import polizze.*

class VersioniTariffaController {

    static defaultAction = "scaricaExcel"

    def scaricaExcel(long id) {
        if(VersioneTariffa.exists(id)) {
            def versione = VersioneTariffa.load(id)
            response.with {
                contentType = "application/vnd.ms-excel"
                setHeader "content-disposition", "attachment; filename=$versione.fileName"
                outputStream << versione.file
            }
        } else render status: 400
    }

}
