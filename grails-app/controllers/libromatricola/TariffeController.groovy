package libromatricola

import polizze.*

class TariffeController {

    static defaultAction = "elenco"

    def elenco() {
        def tariffe = Tariffa.withCriteria { versione { order "dataCaricamento" } }
        [tariffe: tariffe]
    }

    def modifica(long id) {
        if(Tariffa.exists(id)) {
            def tariffa = Tariffa.get(id)
            if(request.post) {
                7.times { i ->
                    def data = params."moltiplicatori[$i]"
                    bindData(tariffa.moltiplicatoriCVT.find { it.id == (data.id as int) }, data)
                }
                bindData(tariffa, params)
                if(tariffa.save()) {
                    flash.message = "Tariffa ${tariffa.codice} aggiornata correttamente"
                    redirect action: "elenco"
                } else return [tariffa: tariffa]
            } else return [tariffa: tariffa]
        } else {
            flash.info = "La tariffa non esiste più"
            redirect action: "elenco"
        }
    }
}
