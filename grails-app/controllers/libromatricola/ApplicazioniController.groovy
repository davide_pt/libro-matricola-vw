package libromatricola

import grails.converters.JSON
import grails.validation.Validateable
import polizze.*
import utenti.*
import groovy.time.TimeCategory as TC
import grails.web.RequestParameter
import static polizze.ProdottoCheck.*
import static polizze.StatoApplicazione.*
import static org.hibernate.criterion.CriteriaSpecification.*
import static polizze.TipoTracciato.ANNULLAMENTO

class ApplicazioniController {

    static defaultAction = "elenco"

    def calcoliService
    def applicazioniService
    def itextService
    def messageSource

    def elenco(long id) {
       //EsportazioneCompagniaJob.triggerNow()
        //EsportazioneCompagniaPrevJob.triggerNow()
       // EsportazioneJob.triggerNow()
        def utente = session.utente
        def cerca = "%${params.q ?: ''}%"
        if(utente instanceof Dealer || Dealer.exists(id)) {
            /**
             * Elenco applicazioni per singolo libro matricola
             */
            def dealer = utente instanceof Dealer ? utente : Dealer.read(id)
            def count = Applicazione.createCriteria().count {
                eq 'dealer', dealer
                or {
                    like 'numero', cerca
                    like 'marca', cerca
                    like 'modello', cerca
                    like 'targa', cerca
                    like 'telaio', cerca
                }
            }
            params.sort = params.sort ?: 'dateCreated'
            params.order = params.order ?: 'desc'
            params.max = Math.abs(Math.min(Math.min(params.int('max', 20), 100), count))
            params.offset = Math.abs(params.int('offset', 0))
            if(params.offset > 0 && params.offset >= count) params.offset = count - params.max
            def applicazioni = Applicazione.createCriteria().list(params) {
                eq 'dealer', dealer
                or {
                    like 'numero', cerca
                    like 'marca', cerca
                    like 'modello', cerca
                    like 'targa', cerca
                    like 'telaio', cerca
                }
            }
            def pagamenti = Applicazione.withCriteria {
                resultTransformer ALIAS_TO_ENTITY_MAP
                eq 'dealer', dealer
                projections {
                    min 'dataPagamento', 'min'
                    max 'dataPagamento', 'max'
                }
            }.first()
            def filter = [
                    statiApplicazione: Applicazione.withCriteria {
                        eq 'dealer', dealer
                        projections { distinct 'stato' }
                    },
                    pagamentoMin: pagamenti.min?.format(),
                    pagamentoMax: pagamenti.max?.format()
            ]
            render view: 'elenco_applicazioni', model: [applicazioni: applicazioni, dealer: dealer, filter: filter]
        } else {
            /**
             * Elenco libri matricola
             */
            def count = Applicazione.createCriteria().get {
                or {
                    dealer {
                        or {
                            like 'numeroLibroMatricola', cerca
                            like 'ragioneSociale', cerca
                        }
                    }
                    like 'targa', cerca
                    like 'numero', cerca
                }
                projections { countDistinct 'dealer' }
            }
            params.sort = params.sort ?: 'dealer.numeroLibroMatricola'
            params.order = params.order ?: 'asc'
            params.max = Math.abs(Math.min(Math.min(params.int('max', 20), 100), count))
            params.offset = Math.abs(params.int('offset', 0))
            if(params.offset > 0 && params.offset >= count) params.offset = count - params.max
            def libriMatricola = Applicazione.createCriteria().list(params) {
                resultTransformer ALIAS_TO_ENTITY_MAP
                or {
                    dealer {
                        or {
                            like 'numeroLibroMatricola', cerca
                            like 'ragioneSociale', cerca
                        }
                    }
                    like 'targa', cerca
                    like 'numero', cerca
                }
                projections {
                    sqlProjection "sum(if(stato in ('POLIZZA', 'SCADUTA'), 1, 0)) as polizze, sum(if(stato = 'PREVENTIVO', 1, 0)) as preventivi, sum(if(stato in ('POLIZZA', 'SCADUTA'), pcl, 0)) as totale, sum(if(pagata = 1, pcl, 0)) as pagato", ['polizze', 'preventivi', 'totale', 'pagato'], [INTEGER, INTEGER, BIG_DECIMAL, BIG_DECIMAL]
                    groupProperty 'dealer', 'dealer'
                }
            }
            libriMatricola.totalCount = count
            def pagamenti = Applicazione.withCriteria {
                resultTransformer ALIAS_TO_ENTITY_MAP
                isNotNull 'dataPagamento'
                projections {
                    min 'dataPagamento', 'min'
                    max 'dataPagamento', 'max'
                }
            }.first()
            def filter = [
                    statiApplicazione: Applicazione.withCriteria { projections { distinct 'stato' } },
                    pagamentoMin: pagamenti?.min?.format(),
                    pagamentoMax: pagamenti?.max?.format()
            ]
            render view: 'elenco_libri_matricola', model: [libriMatricola: libriMatricola, filter: filter]
        }
    }


    def crea(long id) {
        def utente = session.utente
        def applicazione = new Applicazione()
        def versioniTariffa = VersioneTariffa.listOrderByDataCaricamento()
        /*****NOVEMBRE 2016*/
        def nTipoVei=EnumSet.of(TipologiaVeicolo.AUTOVETTURA,TipologiaVeicolo.AUTOCARRO,TipologiaVeicolo.MOTO,TipologiaVeicolo.TARGAPROVA)
        def destinazioneUso=DestUso.withCriteria {
            ne "nome", "Targa prova"
        }
        def dati = [
                applicazione: applicazione,
                versioniTariffa: versioniTariffa,
                tariffe: Tariffa.findAllByVersione(versioniTariffa.first()),
                provincie: Provincia.list(),
                tipologieVeicolo: nTipoVei.toList(),
                destinazioniUso: destinazioneUso.toList().nome,
                alimentazione: Alimentazione.list(),
                veicoli: Veicolo.list(),
        ]
        if(utente instanceof Dealer) {
            applicazione.dealer = utente
            applicazione.tariffa = utente.tariffaEmissioni
            applicazione.provincia = utente.indirizzoSedeLegale.provincia
        } else if(Dealer.exists(id)) {
            dati.dealer = applicazione.dealer = Dealer.get(id)
            applicazione.tariffa = dati.dealer.tariffaEmissioni
            applicazione.provincia = dati.dealer.indirizzoSedeLegale.provincia
        } else dati.dealers = Dealer.withCriteria {
            resultTransformer ALIAS_TO_ENTITY_MAP
            projections {
                property 'id', 'id'
                property 'ragioneSociale', 'ragioneSociale'
            }
        }
        render view: 'editor', model: dati
    }

    def modifica(long app) {
        if(Applicazione.exists(app)) {
            def applicazione = Applicazione.read(app)
            def versioniTariffa = VersioneTariffa.listOrderByDataCaricamento()
            /*****NOVEMBRE 2016*/
            def nTipoVei=EnumSet.of(TipologiaVeicolo.AUTOVETTURA,TipologiaVeicolo.AUTOCARRO,TipologiaVeicolo.MOTO,TipologiaVeicolo.TARGAPROVA)
            def destinazioneUso=DestUso.withCriteria {
                //ne "nome", "Targa prova"
            }
            //EnumSet.of(TariffaRCCheck.T1,TariffaRCCheck.T2,TariffaRCCheck.T3),
            def dati = [
                    applicazione: applicazione,
                    versioniTariffa: versioniTariffa,
                    tariffe: Tariffa.findAllByVersione(applicazione.tariffa.versione ?: versioniTariffa.first()),
                    prodotti: applicazione.calcoli.keySet(),
                    tariffeACRC: applicazione.tariffaAutocarri.name,
                    provincie: Provincia.list(),
                    tipologieVeicolo: nTipoVei.toList(),
                    destinazioniUso: destinazioneUso.toList().nome,
                    alimentazione: Alimentazione.list(),
                    veicoli: Veicolo.list(),
                    dealer: applicazione.dealer,
                    veicolo: Veicolo.findByMarcaAndModello(applicazione.marca, applicazione.modello) ?: Veicolo.findByMarcaAndModelloIsNull(applicazione.marca) ?: Veicolo.findByMarca('Altro')
            ]
            //println "dati ${dati}"
            render view: 'editor', model: dati
        } else {
            flash.info = "L'applicazione non esiste più"
            redirect action: 'elenco', params: params
        }
    }

    def salva(long id, long app, @RequestParameter("op") String view) {
        def dealerTariffa = view == "editor" ? "tariffaEmissioni" : "tariffaRinnovi"
        def utente = session.utente
        def applicazione = Applicazione.exists(app) ? Applicazione.get(app) : new Applicazione()
        def versioniTariffa = VersioneTariffa.listOrderByDataCaricamento()
        view = view ?: "editor"
        def destinazioneUso/*=params.destinazioneUso*/
        /*****NOVEMBRE 2016*/
        def nTipoVei=EnumSet.of(TipologiaVeicolo.AUTOVETTURA,TipologiaVeicolo.AUTOCARRO,TipologiaVeicolo.MOTO,TipologiaVeicolo.TARGAPROVA)
        if(params.tipologiaVeicolo=="TARGAPROVA"){
            destinazioneUso=DestUso.withCriteria {
                eq "nome", "Targa prova"
            }
            applicazione.destinazioneUso="Targa prova"
        }else{
            destinazioneUso=DestUso.withCriteria {
                eq "nome", params.destinazioneUso
            }
            applicazione.destinazioneUso=params.destinazioneUso
        }
        def dati = [
                applicazione: applicazione,
                versioniTariffa: versioniTariffa,
                tariffe: Tariffa.findAllByVersione(versioniTariffa.first()),
                provincie: Provincia.list(),
                tipologieVeicolo: nTipoVei.toList(),
                destinazioniUso: destinazioneUso.toList().nome,
                alimentazione: Alimentazione.list(),
                veicoli: Veicolo.list()
        ]

        if(utente instanceof Dealer) {
            applicazione.dealer = utente
            applicazione.tariffa = utente."$dealerTariffa"
            applicazione.provincia = utente.indirizzoSedeLegale.provincia
        } else if(Dealer.exists(id)) {
            dati.dealer = applicazione.dealer = Dealer.get(id)
            applicazione.tariffa = dati.dealer."$dealerTariffa"
            applicazione.provincia = dati.dealer.indirizzoSedeLegale.provincia
        } else dati.dealers = Dealer.withCriteria {
            resultTransformer ALIAS_TO_ENTITY_MAP
            projections {
                property 'id', 'id'
                property 'ragioneSociale', 'ragioneSociale'
            }
        }
        if(request.post) {
            if(params.valoreAssicurato?.endsWith('€')) params.valoreAssicurato = params.valoreAssicurato[0..-2].trim()
            if((params.alimentazione=="null" || !(params.alimentazione)) ) params.alimentazione = Alimentazione.ALTRO
            println "i parametri sono ${params}"
            bindData(applicazione, params)
            def destUso=applicazione.destinazioneUso
            def dealer = applicazione.dealer
            def tariffa = applicazione.tariffa
            dati.tariffe = Tariffa.findAllByVersione(tariffa.versione)
            def valore = applicazione.valoreAssicurato ?: 0.0
            def decorrenza = applicazione.dataDecorrenza
            def scadenza = applicazione.dataScadenza ?: "31-12-${decorrenza.format("yyyy")}".parseDate()
            def provincia = applicazione.provincia
            def prodotti = params.list('prodotti')
            def tariffeACRC = params.list('tariffeACRC')
            def veicolo = Veicolo.findByMarcaAndModello(params.marca, params.modello) ?: Veicolo.findByMarcaAndModelloIsNull(params.marca) ?: Veicolo.read(params.veicolo != 'null' ? params.veicolo : -1L) ?: Veicolo.findByMarca("Altro")
            params.veicolo = veicolo?.id
            def tipoVeicolo = applicazione.tipologiaVeicolo
            def targaProva = (destUso == 'Targa prova')
            /**novembre 2016**/
            def kw = applicazione.kw
            def cv = applicazione.cv
            def alimentazione = applicazione.alimentazione ?: Alimentazione.ALTRO
            def nposti = applicazione.nposti
            def cilindrata = applicazione.cilindrata
            def datanuovat = new Date().parse('yyyy/MM/dd', '2017/10/01')
            //if(decorrenza>=datanuovat){
            def marca = applicazione.marca
            def modello = applicazione.modello
            def quintali = applicazione.quintali
            def errors = []
            if(dealer == null) errors += 'Selezionare un dealer'
            if(tariffa == null) errors += 'Selezionare una tariffa valida'
            if(provincia == null) errors += 'Selezionare una provincia valida'
            if(!prodotti) errors += 'Selezionare almeno un prodotto'
            if(tipoVeicolo == TipologiaVeicolo.AUTOCARRO && !tariffeACRC  && (decorrenza == null && decorrenza>=datanuovat)) errors += 'Selezionare almeno una tariffa'
            if(veicolo == null) errors += 'Selezionare un veicolo valido'
            if(tipoVeicolo == null) errors += 'Selezionare una tipologia di veicolo valida'
            if(decorrenza == null) errors += 'Specificare la data di decorrenza'
            if(destUso == 'Targa prova') applicazione.targaProva=true
            if(kw == 0.0 && !(params.tipologiaVeicolo=="TARGAPROVA") && (decorrenza == null && decorrenza>=datanuovat)) errors += 'Specificare il kw'
            if(cv == 0.0 &&(decorrenza == null && decorrenza>=datanuovat)) errors += 'Specificare il cv'
            if(cilindrata == '' && !(params.tipologiaVeicolo=="TARGAPROVA") &&(decorrenza == null && decorrenza>=datanuovat)) errors += 'Specificare la cilindrata'
            if(alimentazione == null && !(params.tipologiaVeicolo=="TARGAPROVA")) errors += 'Specificare l\'alimentazione'
            if((marca == null || marca=='') && !targaProva) errors += 'Specificare la marca'
            if((modello == null  || modello=='') && !targaProva) errors += 'Specificare il modello'
            if((quintali == null  || quintali=='' || quintali ==0) && tipoVeicolo == TipologiaVeicolo.AUTOCARRO) errors += 'Specificare i quintali'
            if((quintali != null  && quintali >35) && tipoVeicolo == TipologiaVeicolo.AUTOCARRO && params.list('prodotti').contains("rc")) errors += 'I quintali sono superiori 35. Contattare la Compagnia per richiedere deroga tariffa'
            //println "params -->${params}"
            if(valore == 0.0 && params.list('prodotti').contains("cvt") && !(params.tipologiaVeicolo=="TARGAPROVA")) errors += 'Specificare il valore assicurato'
            if(tipoVeicolo==TipologiaVeicolo.MOTO && nposti==''){errors += 'Specificare il no. posti'}
            if(errors) flash.error = errors.join('<br>')
            else {
                tariffeACRC = tariffeACRC.contains('T1') ? TariffaRCCheck.T1 : tariffeACRC.contains('T2') ? TariffaRCCheck.T2 : tariffeACRC.contains('T3') ? TariffaRCCheck.T3 : TariffaRCCheck.NO
                println "tariffe scelta ${tariffeACRC}"
                applicazione.rischio = veicolo.rischio
                applicazione.codiceVeicolo = veicolo.codice
                applicazione.dataScadenza = scadenza
                /*novembre 2016*/
                applicazione.destinazioneUso=destUso
                applicazione.alimentazione=alimentazione
                def calcoli = calcoliService.calcola(tipoVeicolo, targaProva, prodotti, tariffa, provincia, veicolo, valore, decorrenza, scadenza,cv, destUso, tariffeACRC)
                if(!applicazione.calcoli) applicazione.calcoli = calcoli.collectEntries { prodotto, risultati -> [prodotto, new Calcoli(risultati)]}
                else {
                    //Aggiungo eventuali nuove garanzie e aggiorno quelle modificate
                    calcoli.each { prodotto, risultati ->
                        if(applicazione.calcoli[prodotto]) risultati.each { var, val -> if(applicazione.calcoli[prodotto][var] != val) applicazione.calcoli[prodotto][var] = val }
                        else applicazione.calcoli[prodotto] = new Calcoli(risultati)
                    }
                    //Tolgo le garanzie tolte
                    def prodottiTolti = applicazione.calcoli.keySet() - calcoli.keySet()
                    prodottiTolti.each { prodotto -> applicazione.calcoli.remove(prodotto) }
                    //Aggiorno i totali
                    applicazione.with {
                        def values = calcoli.values()
                        pcl = values.sum { it.pcl }
                        imponibile = values.sum { it.imponibile }
                        imposte = values.sum { it.imposte }
                        pdu = values.sum { it.pdu }
                        provvigioniVW = values.sum { it.provvigioniVW }
                        provvigioniMach1 = values.sum { it.provvigioniMach1 }
                    }
                }
                applicazione.cvt = applicazione.calcoli.cvt ? SI : NO
                applicazione.kasko = applicazione.calcoli.kasko ? SI : NO
                applicazione.rc = applicazione.calcoli.rc ? SI : NO
                applicazione.tutela = applicazione.calcoli."tutela giudiziaria" ? SI : NO
                applicazione.infortuni = prodotti.contains('infortuni 100.000') ? A : prodotti.contains('infortuni 200.000') ? B : NO
                applicazione.tariffaAutocarri = tariffeACRC
                applicazione.premiorimborso = 0.0
                if(tariffa != dealer."$dealerTariffa") {
                    dealer."$dealerTariffa" = tariffa
                    dealer.save()
                }
                def success = "Applicazione ${applicazione.id ? 'modificata' : 'inserita'} correttamente"
                if(params.submit == 'Calcola') {
                    applicazione.validate()
                    applicazione.discard()
                } else if(applicazione.save()) {
                    flash.message = success
                    if(params.submit == 'Salva e continua') redirect action: 'crea', id: id ?: dealer.id, params: params.findAll { k, v -> k in ['max', 'offset', 'sort', 'order', 'q'] }
                    else redirect action: 'elenco', id: id ?: dealer.id, params: params.findAll { k, v -> k in ['max', 'offset', 'sort', 'order', 'q'] }
                } else println "Errore salvataggio applicazione $applicazione.numero: $applicazione.errors"
            }
        }
        render view: view, model: dati
    }

    def cancella(long app, String codice, String dEsclu, String pagata, String motivoEsclusione) {
        if(Applicazione.exists(app)) {
            if(motivoEsclusione.trim()!='' && motivoEsclusione.trim()!=null){
                def applicazione = Applicazione.get(app)
                def utente = session.utente
                if(utente instanceof Dealer && applicazione.dealer != utente) flash.error = "Applicazione non esistente"
                else {
                    try {
                        applicazioniService.escludiApplicazione(applicazione, codice, dEsclu, pagata, motivoEsclusione)
                        flash.message = "Applicazione esclusa correttamente"
                    } catch(e) {
                        flash.error = "Si è verificato un errore imprevisto: $e.message"
                        e.printStackTrace()
                    }

                }
            }else flash.error = "Compilare la motivazione"

        } else flash.error = "Applicazione non esistente"
        redirect action: 'elenco', id: params.id
    }

    def mostra(long app) {
        def applicazione = Applicazione.read(app)
        def utente = session.utente
        if(applicazione && (utente instanceof Admin || applicazione.dealer == utente)) return [applicazione: applicazione]
        else {
            flash.info = "L'applicazione non esiste più"
            redirect action: 'elenco', params: params
        }
    }

    def mostraTracciato(long id) {
        def tracciato = Tracciato.read(id)
        def scomposto = applicazioniService.scomponiTracciato(tracciato.tracciato)
        render template: "tracciatoScomposto", model: [scomposto: scomposto]
    }
    def mostraTracciatoAnn(long id) {
        def tracciato = TracciatoIass.read(id)
        def scomposto = applicazioniService.scomponiTracciato(tracciato.tracciato)
        render template: "tracciatoScomposto", model: [scomposto: scomposto]
    }

    def emetti(long id, long app) {
        def applicazione = Applicazione.get(app)
        def isAdmin = session.utente instanceof Admin
        if(request.post && isAdmin) {
            bindData(applicazione, params)
            applicazione.stato = POLIZZA
            applicazione.numero = applicazioniService.generaNumeroApplicazione(applicazione.dealer)
            applicazione.dataEmissione = new Date()
            if(applicazione.validate()) {
                applicazioniService.generaTracciato(applicazione)
                //applicazioniService.generaTracciatoANIA(applicazione)
            }
            def targheDuplicate = Applicazione.withCriteria {
                resultTransformer ALIAS_TO_ENTITY_MAP
                ne "id", applicazione.id
                eq "targa", applicazione.targa
                projections {
                    property "id", "id"
                    property "numero", "numero"
                    property "stato", "stato"
                }
            }
            if(targheDuplicate) flash.alert = g.render template: "targheDuplicate", model: [targa: applicazione.targa, targheDuplicate: targheDuplicate]
            if(applicazione.save()) {
                flash.message = "Applicazione emessa correttamete"
                redirect action: 'elenco', params: params, id: id ?: null
            } else println "Errore emissione applicazione $applicazione.numero: $applicazione.errors"
        }
        if(applicazione && isAdmin) return [applicazione: applicazione]
        else {
            flash.info = "L'applicazione non esiste più"
            redirect action: 'elenco', params: params, id: id ?: null
        }
    }

    def rinnova(long id) {
        def utente = session.utente
        def applicazioneDaRinnovare = Applicazione.read(params.remove("app"))   //fondamentale rimuovere app da params per non incorrere nel bug che svuota i calcoli della polizza esclusa
        if(!applicazioneDaRinnovare) {
            flash.info = "L'applicazione non esiste più"
            redirect action: "elenco", id: id
        }
        def applicazione = applicazioneDaRinnovare.with {
            new Applicazione(
                    applicazioneRinnovata: it,
                    dealer: dealer,
                    tariffa: dealer.tariffaRinnovi ?: dealer.tariffaEmissioni,
                    provincia: provincia,
                    tipologiaVeicolo: tipologiaVeicolo,
                    cvt: cvt,
                    kasko: kasko,
                    rc: rc,
                    tutela: tutela,
                    infortuni: infortuni,
                    rischio: rischio,
                    marca: marca,
                    modello: modello,
                    targa: targa,
                    telaio: telaio,
                    codiceVeicolo: codiceVeicolo,
                    vincolo: vincolo,
                    dataImmatricolazione: dataImmatricolazione,
                    valoreAssicurato: valoreAssicurato,
                    quintali: quintali,
                    kw: kw,
                    cv: cv,
                    cilindrata: cilindrata,
                    nposti: nposti,
                    destinazioniUso: destinazioneUso,
                    alimentazione: alimentazione,
                    scadenzaVincolo: scadenzaVincolo,
                    vincololeasing: vincololeasing
            )
        }
        /**novembre 2016**/
        def destinazioneUso
        def nTipoVei=EnumSet.of(TipologiaVeicolo.AUTOVETTURA,TipologiaVeicolo.AUTOCARRO,TipologiaVeicolo.MOTO,TipologiaVeicolo.TARGAPROVA)
        if(params.tipologiaVeicolo=="TARGAPROVA"){
            destinazioneUso=DestUso.withCriteria {
                eq "nome", "Targa prova"
            }
            applicazione.destinazioneUso="Targa prova"
        }else{
            destinazioneUso=DestUso.withCriteria {
                ne "nome", "Targa prova"
            }
            applicazione.destinazioneUso=params.destinazioneUso
        }
        def dati = [
                applicazione: applicazione,
                tariffe: Tariffa.list(),
                provincie: Provincia.list(),
                prodotti: applicazioneDaRinnovare.calcoli.keySet(),
                tipologieVeicolo: nTipoVei.toList(),
                veicoli: Veicolo.list(),
                veicolo: Veicolo.findByMarcaAndModello(applicazione.marca, applicazione.modello) ?: Veicolo.findByMarcaAndModelloIsNull(applicazione.marca) ?: Veicolo.findByMarca('Altro'),
                destinazioniUso: destinazioneUso.toList().nome,
                alimentazione: Alimentazione.list()
        ]
        if(utente instanceof Dealer) {
            applicazione.dealer = utente
            applicazione.tariffa = utente.tariffaRinnovi ?: utente.tariffaEmissioni
            applicazione.provincia = utente.indirizzoSedeLegale.provincia
        } else if(Dealer.exists(id)) {
            dati.dealer = applicazione.dealer = Dealer.get(id)
            applicazione.tariffa = dati.dealer.tariffaRinnovi ?: dati.dealer.tariffaEmissioni
            applicazione.provincia = dati.dealer.indirizzoSedeLegale.provincia
        } else dati.dealers = Dealer.withCriteria {
            resultTransformer ALIAS_TO_ENTITY_MAP
            projections {
                property 'id', 'id'
                property 'ragioneSociale', 'ragioneSociale'
            }
        }
        dati
    }

    def riattiva(long id) {
        def utente = session.utente
        def applicazioneEsclusa = Applicazione.read(params.remove("app"))   //fondamentale rimuovere app da params per non incorrere nel bug che svuota i calcoli della polizza esclusa
        if(!applicazioneEsclusa) {
            flash.info = "L'applicazione non esiste più"
            redirect action: "elenco", id: id
        }
        def applicazione = applicazioneEsclusa.with {
            new Applicazione(
                    applicazioneEsclusa: it,
                    dealer: dealer,
                    tariffa: tariffa,
                    provincia: provincia,
                    tipologiaVeicolo: tipologiaVeicolo,
                    cvt: cvt,
                    kasko: kasko,
                    rc: rc,
                    tutela: tutela,
                    infortuni: infortuni,
                    rischio: rischio,
                    marca: marca,
                    modello: modello,
                    targa: targa,
                    telaio: telaio,
                    codiceVeicolo: codiceVeicolo,
                    vincolo: vincolo,
                    dataImmatricolazione: dataImmatricolazione,
                    valoreAssicurato: valoreAssicurato,
                    quintali: quintali,
                    kw: kw,
                    cv:cv,
                    cilindrata: cilindrata,
                    nposti: nposti,
                    destinazioniUso: destinazioneUso,
                    alimentazione: alimentazione,
                    scadenzaVincolo: scadenzaVincolo,
                    vincololeasing: vincololeasing
            )
        }
        def versioniTariffa = VersioneTariffa.listOrderByDataCaricamento()
        /**novembre 2016**/
        def destinazioneUso
        def nTipoVei=EnumSet.of(TipologiaVeicolo.AUTOVETTURA,TipologiaVeicolo.AUTOCARRO,TipologiaVeicolo.MOTO,TipologiaVeicolo.TARGAPROVA)
        if(params.tipologiaVeicolo=="TARGAPROVA"){
            destinazioneUso=DestUso.withCriteria {
                eq "nome", "Targa prova"
            }
            applicazione.destinazioneUso="Targa prova"
        }else{
            destinazioneUso=DestUso.withCriteria {
                ne "nome", "Targa prova"
            }
            applicazione.destinazioneUso=params.destinazioneUso
        }
        def dati = [
                applicazione: applicazione,
                versioniTariffa: versioniTariffa,
                tariffe: Tariffa.findAllByVersione(applicazione.tariffa.versione ?: versioniTariffa.first()),
                //tariffe: Tariffa.list(),
                provincie: Provincia.list(),
                prodotti: applicazioneEsclusa.calcoli.keySet(),
                tipologieVeicolo: nTipoVei.toList(),
                veicoli: Veicolo.list(),
                veicolo: Veicolo.findByMarcaAndModello(applicazione.marca, applicazione.modello) ?: Veicolo.findByMarcaAndModelloIsNull(applicazione.marca) ?: Veicolo.findByMarca('Altro'),
                destinazioniUso: destinazioneUso.toList().nome,
                alimentazione: Alimentazione.list()
        ]
        if(utente instanceof Dealer) {
            applicazione.dealer = utente
            applicazione.tariffa = utente.tariffaEmissioni
            applicazione.provincia = utente.indirizzoSedeLegale.provincia
        } else if(Dealer.exists(id)) {
            dati.dealer = applicazione.dealer = Dealer.get(id)
            applicazione.tariffa = dati.dealer.tariffaEmissioni
            applicazione.provincia = dati.dealer.indirizzoSedeLegale.provincia
        } else dati.dealers = Dealer.withCriteria {
            resultTransformer ALIAS_TO_ENTITY_MAP
            projections {
                property 'id', 'id'
                property 'ragioneSociale', 'ragioneSociale'
            }
        }
        render view: 'editor', model: dati
    }

    def tariffe(long id) {
        def versioneTariffa = VersioneTariffa.load(id)
        def result = [
                tariffe: [[id: null, text: "Scegli..."]] + Tariffa.findAllByVersione(versioneTariffa).collect { versione -> [id: versione.id, text: versione.codice] }
        ]
        render result as JSON
    }

    def dealer(long id) {
        def dealer = Dealer.read(id)
        def result = [
                tariffaEmissioni: dealer?.tariffaEmissioni?.id ?: 'null',
                tariffaRinnovi: dealer?.tariffaRinnovi?.id ?: 'null',
                provincia: dealer?.indirizzoSedeLegale?.provincia?.nome ?: 'Scegli un dealer',
                numero: dealer?.numeroLibroMatricola ?: 'Scegli un dealer'
        ]
        render result as JSON
    }

    def veicolo(long id, boolean targaProva) {
        def veicolo = Veicolo.read(id)
        if(targaProva) render f.field(type: 'text', name: 'marca', readonly: true)
        else if(veicolo == null || veicolo?.marca == 'Altro') render f.field(type: 'text', name: 'marca', required: true)
        else render f.field(type: 'text', name: 'marca', readonly: true, value: veicolo?.marca, required: true)
        if(targaProva) render f.field(type: 'text', name: 'modello', readonly: true)
        else if(veicolo?.modello == null || veicolo?.modello == 'Camper e roulottes') render f.field(type: 'text', name: 'modello', required: true)
        else render f.field(type: 'text', name: 'modello', readonly: true, value: veicolo.modello, required: true)
    }

    def emissioni(long id) {
        if(Dealer.exists(id)) {
            def dealer = Dealer.read(id)
            def dateEmissione = Applicazione.withCriteria {
                resultTransformer ALIAS_TO_ENTITY_MAP
                eq 'dealer', dealer
                eq 'stato', POLIZZA
                projections {
                    min 'dataEmissione', 'min'
                    max 'dataEmissione', 'max'
                }
            }.first()
            render render(contentType: "application/json") {
                min = dateEmissione.min.format()
                max = dateEmissione.max.format()
            }
        } else render(contentType: "application/json") { error = "Il dealer selezionato non esiste" }
    }

    def pagamenti(long id) {
        if(Dealer.exists(id)) {
            def dealer = Dealer.read(id)
            def datePagamento = Applicazione.withCriteria {
                resultTransformer ALIAS_TO_ENTITY_MAP
                eq 'dealer', dealer
                ne 'stato', PREVENTIVO
                isNotNull 'dataPagamento'
                projections {
                    min 'dataPagamento', 'min'
                    max 'dataPagamento', 'max'
                }
            }.first()
            render render(contentType: "application/json") {
                min = datePagamento.min.format()
                max = datePagamento.max.format()
            }
        } else render(contentType: "application/json") { error = "Il dealer selezionato non esiste" }
    }

    def anniPreventivi(long id) {
        def dealer = Dealer.read(id)
        def anniPreventivi = Applicazione.withCriteria {
            eq 'dealer', dealer
            eq 'stato', PREVENTIVO
            projections {
                distinct "annoValidita"
            }
        }
        render(contentType: "application/json") { anniPreventivi }
    }

    def datePreventivi(long id) {
        def dealer = Dealer.read(id)
        def datePreventivi = Applicazione.withCriteria {
            resultTransformer ALIAS_TO_ENTITY_MAP
            eq 'dealer', dealer
            eq 'stato', PREVENTIVO
            projections {
                min "dataDecorrenza", "minDecorrenza"
                max "dataDecorrenza", "maxDecorrenza"
                min "dateCreated", "minCreazione"
                max "dateCreated", "maxCreazione"
            }
        }.first()
        render(contentType: "application/json") {
            min = datePreventivi.minDecorrenza?.format() ?: datePreventivi.minCreazione.format()
            max = datePreventivi.maxDecorrenza?.format() ?: datePreventivi.maxCreazione.format()
        }
    }

    def contrassegno(long id) {
        def applicazione = Applicazione.read(id)
        def contrassegno = itextService.generaContrassegno(applicazione)
        response.setContentType("application/pdf")
        response.setHeader("Content-disposition", "inline; filename=Contrassegno ${applicazione.numero}.pdf")
        response.outputStream << contrassegno.toByteArray()
    }

    def certificato(long id) {
        def dealer = Dealer.read(id)
        def from = use(TC) { params.from.parseDate() - 1.days }, to = use(TC) { params.to.parseDate() + 1.days }, split = params.split ? true : false
        //def applicazioni = Applicazione.findAllByDealerAndStatoAndDataEmissioneBetweenAndPagata(dealer, POLIZZA, from, to, true)
        def applicazioni = Applicazione.findAllByDealerAndDataPagamentoBetween(dealer, from, to)
        def certificato = itextService.generaCertificato(dealer, applicazioni, split)
        response.setContentType("application/pdf")
        response.setHeader("Content-disposition", "attachment; filename=Certificato libro matricola ${dealer.numeroLibroMatricola}.pdf")
        response.outputStream << certificato.toByteArray()
    }

    def preventivi(long id) {
        def dealer = Dealer.read(id)
        def from = params.preventiviFrom.parseDate(), to = use(TC) { params.preventiviTo.parseDate() + 1.days - 1.minutes }
        def applicazioni = Applicazione.findAllByDealerAndStatoAndDataDecorrenzaBetween(dealer, PREVENTIVO, from, to) ?: Applicazione.findAllByDealerAndStatoAndDateCreatedBetween(dealer, PREVENTIVO, from, to)
        def preventivi = applicazioniService.generaPreventivo(applicazioni, from, to.clearTime())
        response.setContentType("application/vnd.ms-excel")
        response.setHeader("Content-disposition", "attachment; filename=Preventivi libro matricola ${dealer.numeroLibroMatricola} dal ${from.format()} al ${to.format()}.xlsx")
        response.outputStream << preventivi
    }

    def paga(long id) {
        if(Applicazione.exists(id)) {
            def applicazione = Applicazione.get(id)
            if(applicazione.pagata) {
                applicazione.pagata = false
                applicazione.dataPagamento = null
            } else {
                applicazione.pagata = true
                applicazione.dataPagamento = new Date()
            }
            if(applicazione.save()) render(contentType: "application/json") { pagataIl = applicazione.dataPagamento?.format() ?: 'Non ancora pagata' }
            else render(contentType: "application/json") { error = "Si è verificato un errore imprevisto: ${applicazione.errors.collect { message(error: it) }.join('<br>')}" }
        } else render(contentType: "application/json") { error = "L'applicazione non esiste più" }
    }

    def pagaTutte(long id) {
        def logg
        def utente = session.utente
        def stringaresponse="null"
        def pagata=[]
        def errore=[]
        if(Dealer.exists(id)){
            def dealer=Dealer.get(id)
            logg =new Log(parametri: "dealer ${dealer.ragioneSociale}", operazione: "pagamento tutte polizze appartenenti al dealer", pagina: "Elenco libri matricola", utente:utente )
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            def elencoAppli=Applicazione.createCriteria().list {
                eq 'dealer', dealer
                eq 'pagata', false
                eq 'deleted', false
                eq 'stato', StatoApplicazione.POLIZZA
            }
            if(elencoAppli.size()>0){
                elencoAppli.each(){applicazione->
                    logg =new Log(parametri: "dealer ${dealer.ragioneSociale}", operazione: "pagamento applicazione ${applicazione.numero}", pagina: "Elenco libri matricola", utente:utente )
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    applicazione.pagata = true
                    applicazione.dataPagamento = new Date()
                    if(applicazione.save(flush: true)) {
                        logg =new Log(parametri: "dealer ${dealer.ragioneSociale}", operazione: " applicazione ${applicazione.numero} pagata il ${applicazione.dataPagamento?.format()}", pagina: "Elenco libri matricola", utente:utente )
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        pagata.add("applicazione ${applicazione.numero} pagata Il ${applicazione.dataPagamento?.format()}")
                    }
                    else {
                        logg =new Log(parametri: "dealer ${dealer.ragioneSociale}", operazione: " Si è verificato un errore imprevisto: ${applicazione.errors.toString()}", pagina: "Elenco libri matricola", utente:utente )
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        errore.add("Si è verificato un errore imprevisto: ${applicazione.errors.toString()}")
                    }
                }
            }else{
                logg =new Log(parametri: "dealer ${dealer.ragioneSociale}", operazione: "non ci sono applicazioni da pagare", pagina: "Elenco libri matricola", utente:utente )
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                errore.add("non ci sono applicazioni da pagare")
            }


        }
        else {
            logg =new Log(parametri: "dealer ${dealer.ragioneSociale}", operazione: "L'applicazione non esiste più", pagina: "Elenco libri matricola", utente:utente )
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            errore.add("L'applicazione non esiste più")
        }
        /*if(Applicazione.exists(id)) {
            def applicazione = Applicazione.get(id)
            if(applicazione.pagata) {
                applicazione.pagata = false
                applicazione.dataPagamento = null
            } else {
                applicazione.pagata = true
                applicazione.dataPagamento = new Date()
            }
            if(applicazione.save()) render(contentType: "application/json") { pagataIl = applicazione.dataPagamento?.format() ?: 'Non ancora pagata' }
            else render(contentType: "application/json") { error = "Si è verificato un errore imprevisto: ${applicazione.errors.collect { message(error: it) }.join('<br>')}" }
        } else render(contentType: "application/json") { error = "L'applicazione non esiste più" }*/
        // render(contentType: "application/json") {  'Non ancora pagata' }
        stringaresponse=[risposta: pagata, errore:errore]
        render stringaresponse as JSON
    }

    def estrazione(FilterCommand filtro) {
        def excel = applicazioniService.estraiApplicazioni(filtro)
        response.setContentType("application/vnd.ms-excel")
        response.setHeader("Content-disposition", "attachment; filename=Estrazione applicazioni del ${new Date().format()}.xlsx")
        response.outputStream << excel
    }

    def appenaEmessa(long id) {
        render(contentType: "application/json") { check = applicazioniService.appenaEmessa(id) }
    }

}

class FilterCommand {

    Dealer dealer
    BigDecimal premioFrom, premioTo
    StatoApplicazione stato
    Date pagamentoFrom, pagamentoTo, decorrenzaTo,decorrenzaFrom, esclusioneFrom, esclusioneTo, scadenzaTo, scadenzaFrom
    List garanzie
    boolean riattivazioni

}
