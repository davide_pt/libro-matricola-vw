package libromatricola

import utenti.*
import polizze.*
import grails.converters.*
import static polizze.StatoApplicazione.*
import static polizze.ProdottoCheck.*
import static polizze.TipoTracciato.*
import static polizze.Operazione.*
import static org.hibernate.criterion.CriteriaSpecification.*

class SviluppoController {

    def mailService
    def applicazioniService
    def sviluppoService

    def index() {
        [versioniTariffa: VersioneTariffa.listOrderByDataCaricamento()]
    }

    def esportazioneJob() {
        EsportazioneJob.triggerNow()
        flash.message = 'EsportazioneJob triggered'
        redirect action: "index"
    }

    def scadenzaJob() {
        ScadenzaJob.triggerNow()
        flash.message = 'ScadenzaJob triggered'
        redirect action: "index"
    }

    def rigeneraTracciatiAnia() {
        def applicazioniRc = Applicazione.withCriteria {
            ne 'stato', PREVENTIVO
            eq 'rc', SI
            projections { property 'id' }
        }, flusso = []
        applicazioniRc.each { id ->
            if(Applicazione.exists(id)) {
                def applicazione = Applicazione.read(id)
                def tracciato = applicazioniService.generaTracciatoANIA(applicazione, false)
                if(tracciato) flusso << tracciato
            }
        }
        try {
            mailService.sendMail {
                multipart true
                to "priscila@presstoday.com"
                from "priscila@presstoday.com"
                subject "Flusso ANIA rigenerato da Libro Matricola"
                text "In allegato il flusso ANIA rigenerato"
                attach "clper441_${new Date().format('yyyyMMddkkmm')}_rcigio.txt", "text/plain", flusso.join("\n").bytes
            }
            flash.message = "Inoltro flusso ANIA rigenerato con successo"
        } catch(e) { flash.error = "Errore inoltro flusso ANIA" }
        redirect action: "index"
    }

    def rigeneraFlussoAniaDaElenco() {
        def elenco = new String(request.getFile("elenco").bytes).split("\n")
        if(elenco) {
            def flusso = []
            elenco.each { numero ->
                def applicazione = Applicazione.findByNumero(numero)
                if(applicazione) {
                    if(applicazione.operazione == INSERIMENTO) flusso << applicazioniService.generaTracciatoANIA(applicazione, false, "NA")
                    else if(applicazione.operazione == ANNULLAMENTO) {
                        def vecchiTracciatiAnia = TracciatoIass.findByApplicazioneAndTipoAndOperazione(applicazione, ANNULLAMENTOANIA,ANNULLAMENTO)
                        def vecchioSuffisso = vecchiTracciatiAnia ? vecchiTracciatiAnia.first().tracciato.substring(228, 230) : "AN"
                        def dataCreazione=Date.parse("yyyy-MM-dd","2017-10-02")
                        if(applicazione.rc == SI && applicazione.dateCreated.clearTime()<=dataCreazione){
                            flusso << applicazioniService.generaTracciatoANIAAnnulla(applicazione, false, vecchioSuffisso,applicazione.dataEsclusione)
                            flash.message = "Flusso ANIA rigenerato con successo (${flusso.size()} su ${elenco.size()} tracciati irgenerati)!"
                        }
                    }
                }
            }
            try {
                mailService.sendMail {
                    multipart true
                    to "priscila@presstoday.com"
                    from "priscila@presstoday.com"
                    subject "Flusso ANIA rigenerato da Libro Matricola"
                    text "In allegato il flusso ANIA rigenerato"
                    attach "clper441_${new Date().format('yyyyMMddkkmm')}_rcigio.txt", "text/plain", flusso.join("\n").bytes
                }
                flash.message += "<br>Inoltro flusso ANIA avvenuto con successo"
            } catch(e) { flash.error = "Errore inoltro flusso ANIA" }
        } else flash.error = "Specificare l'elenco di applicazioni"
        redirect action: "index"
    }

    def rigeneraTracciatiApplicazione(long id) {
        if(Applicazione.exists(id)) {
            def applicazione = Applicazione.read(id)
            def tracciati = applicazioniService.generaTracciato(applicazione, null, false, "006", false)
            def destinatario = "priscila@presstoday.com"
            mailService.sendMail {
                multipart true
                to destinatario
                subject "Tracciati rigenerati per apploicazione $applicazione.numero"
                html "In allegato troverai i tracciati rigenerati per l'applicazione <b>$applicazione.numero</b>, targata <b>$applicazione.targa</b>."
                attach "Tracciati ${applicazione.numero}.txt", "text/plain", tracciati.join("\r\n").bytes
            }
            render "Tracciati correttamente inoltrati a $destinatario"
        } else render "Non esiste l'applicazione con id: $id"
    }

    def cercaApplicazione(String term) {
        def cerca = "%${term}%"
        def applicazioni = Applicazione.withCriteria {
            resultTransformer ALIAS_TO_ENTITY_MAP
            or {
                dealer {
                    or {
                        like 'numeroLibroMatricola', cerca
                        like 'ragioneSociale', cerca
                    }
                }
                like 'targa', cerca
                like 'numero', cerca
            }
            projections {
                property "id", "value"
                sqlProjection "concat(numero, ' (', targa, ')') as label", "label", STRING
            }
        }
        if(request.xhr) render applicazioni as JSON
    }

    def caricaNuovaVersioneTariffa(String versione) {
        def errors = sviluppoService.caricaNuovaVersioneTariffa(request.getFile("file"), versione)
        if(errors) flash.error = errors.join("<br>")
        else flash.message = "Versione tariffa <b>$versione</b> correttamente creata"
        redirect action: "index"
    }

    def modificaVersioneTariffa(long id, String versione) {
        if(VersioneTariffa.exists(id)) {
            def versioneTariffa = VersioneTariffa.get(id)
            def errors = sviluppoService.modificaVersioneTariffa(versioneTariffa, request.getFile("file"), versione)
            if(errors) flash.error = errors.join("<br>")
            else flash.message = "Versione tariffa <b>$versione</b> correttamente modificata"
            redirect action: "index"
        } else render status: 400
    }

    def cancellaVersioneTariffa(long id) {
        if(VersioneTariffa.exists(id)) {
            def versioneTariffa = VersioneTariffa.get(id)
            versioneTariffa.delete()
            flash.message = "Versione tariffa <b>$versioneTariffa.versione</b> correttamente cancellata"
        } else flash.error = "La versione richiesta è gia stata cancellata"
        redirect action: "index"
    }

    def datiVersioneTariffa(long id) {
        if(VersioneTariffa.exists(id)) {
            def versioneTariffa = VersioneTariffa.read(id)
            render versioneTariffa as JSON
        } else render status: 400
    }

}
