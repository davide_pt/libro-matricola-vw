package libromatricola

import polizze.*
import static org.hibernate.criterion.CriteriaSpecification.*

class FlussiController {

    static defaultAction = "elenco"

    def elenco() {
        params.sort = params.sort ?: 'dateCreated'
        params.order = params.order ?: 'desc'
        def flussi = Flusso.withCriteria {
            resultTransformer ALIAS_TO_ENTITY_MAP
            projections {
                property "id", "id"
                property "fileName", "fileName"
                property "dateCreated", "dateCreated"
                property "size", "size"
            }
            order params.sort, params.order
        }
        [flussi: flussi]
    }

    def download(long id) {
        if(Flusso.exists(id)) {
            def flusso = Flusso.read(id)
            response.with {
                contentType = "text"
                setHeader("Content-disposition", "attachment; filename=$flusso.fileName")
                outputStream << flusso.file
            }
        } else {
            flash.info = "Il flusso non esiste più"
            redirect action: "elenco"
        }
    }
}
