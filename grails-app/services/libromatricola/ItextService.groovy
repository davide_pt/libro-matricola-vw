package libromatricola

import com.itextpdf.text.*
import com.itextpdf.text.pdf.*
import polizze.*
import utenti.Log

import static libromatricola.TipologiaVeicolo.*
import static polizze.ProdottoCheck.*

class ItextService {

    static transactional = false

    def grailsApplication

    def generaContrassegno(applicazione) {
        def logg
        def file = grailsApplication.mainContext.getResource('pdf/Contrassegno.pdf')?.file
        if(file == null) throw RuntimeException('Il file "Contrassegno.pdf" non è stato trovato')
        logg =new Log(parametri: "prendo il file del contrassegno", operazione: "caricamento constrassegni", pagina: "esportazione JOB")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def output = new ByteArrayOutputStream()
        def document = new PdfReader(file.bytes)
        def writer = new PdfStamper(document, output)
        def canvas = writer.getOverContent(1)
        def tipologiaVeicolo = applicazione.with {
            if(tipologiaVeicolo in [AUTOVETTURA, AUTOVETTURA_NOLEGGIO]) return [esteso: "AUTOVETTURA", abbreviato: "A"]
            else if(tipologiaVeicolo in [AUTOCARRO, AUTOCARRO_NOLEGGIO]) return [esteso: "AUTOCARRO", abbreviato: "C"]
            else if(tipologiaVeicolo in [MOTO]) return [esteso: "MOTOCICLO", abbreviato: "B"]
            else return [esteso: "TARGAPROVA", abbreviato: "T"]
        }
        canvas.saveState()
        writeText(canvas, 30, 775, '8427374 M')
        writeText(canvas, 190, 775, applicazione.numero)
        writeText(canvas, 30, 753, applicazione.dealer.ragioneSociale)
        writeText(canvas, 240, 753, applicazione.dealer.partitaIva)
        writeText(canvas, 30, 731, applicazione.dealer.indirizzoSedeLegale.indirizzo)
        writeText(canvas, 240, 731, applicazione.dealer.indirizzoSedeLegale.cap)
        writeText(canvas, 290, 731, applicazione.dealer.indirizzoSedeLegale.citta)
        writeText(canvas, 30, 708, tipologiaVeicolo.esteso)
        writeText(canvas, 168, 708, applicazione.modello)
        writeText(canvas, 290, 708, applicazione.targa)
        //writeText(canvas, 65, 666, '00:00')
        writeText(canvas, 127, 672, applicazione.dataDecorrenza.format())
        //writeText(canvas, 225, 666, '24:00')
        writeText(canvas, 294, 672, applicazione.dataScadenza.format())
        //writeText(canvas, 412, 730, applicazione.targa, 10)
        //writeText(canvas, 412, 706, tipologiaVeicolo.esteso, 10)
        //writeText(canvas, 483, 679, applicazione.dataScadenza.format('dd'), 10)
        //writeText(canvas, 507, 679, applicazione.dataScadenza.format('MM'), 10)
        //writeText(canvas, 530, 679, applicazione.dataScadenza.format('yyyy'), 10)
        writeText(canvas, 37, 501, applicazione.dataDecorrenza.format('dd'), 10)
        writeText(canvas, 69, 501, applicazione.dataDecorrenza.format('MM'), 10)
        writeText(canvas, 95, 501, applicazione.dataDecorrenza.format('yyyy'), 10)
        writeText(canvas, 132, 501, applicazione.dataScadenza.format('dd'), 10)
        writeText(canvas, 163, 501, applicazione.dataScadenza.format('MM'), 10)
        writeText(canvas, 190, 501, applicazione.dataScadenza.format('yyyy'), 10)
        //writeText(canvas, 226, 524, '539', 10)
        writeText(canvas, 253, 517, applicazione.numero, 10)
        writeText(canvas, 62, 460, applicazione.numero)
        writeText(canvas, 237, 460, tipologiaVeicolo.abbreviato)
        writeText(canvas, 270, 460, applicazione.marca)
        writeText(canvas, 350, 570, applicazione.dealer.ragioneSociale, 10)
        writeText(canvas, 350, 558, applicazione.dealer.indirizzoSedeLegale.indirizzo, 10)
        writeText(canvas, 350, 546, applicazione.with { dealer.indirizzoSedeLegale.with { "$cap $citta $provincia.sigla" } }, 10)
        //DUPLICATO
        writeText(canvas, 37, 207, applicazione.dataDecorrenza.format('dd'), 10)
        writeText(canvas, 69, 207, applicazione.dataDecorrenza.format('MM'), 10)
        writeText(canvas, 95, 207, applicazione.dataDecorrenza.format('yyyy'), 10)
        writeText(canvas, 132, 207, applicazione.dataScadenza.format('dd'), 10)
        writeText(canvas, 163, 207, applicazione.dataScadenza.format('MM'), 10)
        writeText(canvas, 190, 207, applicazione.dataScadenza.format('yyyy'), 10)
        //writeText(canvas, 226, 226, '539', 10)
        writeText(canvas, 253, 222, applicazione.numero, 10)
        writeText(canvas, 62, 166, applicazione.numero)
        writeText(canvas, 237, 166, tipologiaVeicolo.abbreviato)
        writeText(canvas, 270, 166, applicazione.marca)
        writeText(canvas, 350, 273, applicazione.dealer.ragioneSociale, 10)
        writeText(canvas, 350, 261, applicazione.dealer.indirizzoSedeLegale.indirizzo, 10)
        writeText(canvas, 350, 249, applicazione.with { dealer.indirizzoSedeLegale.with { "$cap $citta $provincia.sigla" } }, 10)
        canvas.restoreState()
        writer.close()
        document.close()
        return output
    }

    def generaCertificato(dealer, applicazioni, split = true) {
        def file = grailsApplication.mainContext.getResource('pdf/Certificato di assicurazione.pdf')?.file
        if(file == null) throw RuntimeException('Il file "Certificato di assicurazione.pdf" non è stato trovato')
        def today = new Date()
        return mergePdf(applicazioni.collate(20).collect { subApp ->
            def output = new ByteArrayOutputStream()
            def document = new PdfReader(file.bytes)
            def writer = new PdfStamper(document, output)
            def canvas = writer.getOverContent(1)
            canvas.saveState()
            writeText(canvas, 130, 703 - 17, dealer.ragioneSociale)
            writeText(canvas, 470, 703 - 17, dealer.numeroLibroMatricola)
            writeText(canvas, 90, 685 - 20, dealer.indirizzoSedeLegale.citta)
            writeText(canvas, 308 + 50, 685 - 20, dealer.indirizzoSedeLegale.cap)
            writeText(canvas, 470, 685 - 20, dealer.telefono)
            writeText(canvas, 60 + 30, 667 - 24, dealer.indirizzoSedeLegale.indirizzo)
            writeText(canvas, 308, 667 - 24, dealer.fax)
            writeText(canvas, 90, 648 - 27, dealer.email)
            writeText(canvas, 470, 648 - 27, dealer.partitaIva)
            writeText(canvas, 205, 577 - 30, "${subApp?subApp.dataDecorrenza?.min().format():""}")
            writeText(canvas, 470, 577 - 30, "${subApp?subApp.dataScadenza?.max().format():""}")
            def firstRow = 524 - 42, lineHeight = 14.5
            subApp.sort { it.dateCreated }.eachWithIndex { applicazione, index ->
                def vincolo = applicazione.vincolo.rightPad(12)
                if(vincolo.size() < applicazione.vincolo.size()) vincolo += "..."
                writeText(canvas, 52, firstRow - (lineHeight * index), applicazione.marca)
                writeText(canvas, 146, firstRow - (lineHeight * index), applicazione.modello.rightPad(8))
                writeText(canvas, 220 - 20, firstRow - (lineHeight * index), applicazione.targa)
                if(applicazione.tipologiaVeicolo in [AUTOCARRO,AUTOCARRO_NOLEGGIO]) writeText(canvas, 279 - 20, firstRow - (lineHeight * index), "$applicazione.quintali")
                writeText(canvas, 302 - 20, firstRow - (lineHeight * index), "${applicazione.valoreAssicurato.format()} €")
                writeText(canvas, 383 - 17, firstRow - (lineHeight * index), "$applicazione.cvt")
                writeText(canvas, 405 - 13, firstRow - (lineHeight * index), "$applicazione.kasko")
                writeText(canvas, 487 - 52.5, firstRow - (lineHeight * index), "$applicazione.rc")
                writeText(canvas, 511 - 40, firstRow - (lineHeight * index), "${applicazione.infortuni == A ? "Si" : "No"}")
                writeText(canvas, 511 - 10, firstRow - (lineHeight * index), "${applicazione.infortuni == B ? "Si" : "No"}")
                writeText(canvas, 540, firstRow - (lineHeight * index), "$applicazione.tutela")
            }
            if(split) {
                writeText(canvas, 60, 249 - 87, "${subApp?subApp.sum { it.pcl }.format():0.0} €", 10)
                writeText(canvas, 164 + 20, 249 - 87, "${subApp?subApp.sum { it.imponibile }.format():0.0} €", 10)
                writeText(canvas, 268 + 35, 249 - 87, "${subApp?subApp.sum { it.imposte }.format():0.0} €", 10)
                writeText(canvas, 370 + 35, 249 - 87, "${subApp?subApp.sum { it.rc == SI ? it.calcoli["rc"].imposte * (16 / 26.5) : 0.00 }.format():0.0} €", 10)
            } else {
                writeText(canvas, 60, 249 - 87, "${applicazioni.sum { it.pcl }.format()} €", 10)
                writeText(canvas, 164 + 20, 249 - 87, "${applicazioni.sum { it.imponibile }.format()} €", 10)
                writeText(canvas, 268 + 35, 249 - 87, "${applicazioni.sum { it.imposte }.format()} €", 10)
                writeText(canvas, 370 + 35, 249 - 87, "${applicazioni.sum { it.rc == SI ? it.calcoli["rc"].imposte * (16 / 26.5) : 0.00 }.format()} €", 10)
            }
            writeText(canvas, 490, 249 - 87, "${subApp?subApp.dataPagamento.max().format():0.0}", 10)
            writeText(canvas, 110 - 45, 75 - 15, "${today.format()}")
            canvas.restoreState()
            writer.close()
            document.close()
            return output
        })
    }

    def generaPreventivo(applicazioni) {
        def file = grailsApplication.mainContext.getResource('pdf/Preventivo.pdf')?.file
        if(file == null) throw RuntimeException('Il file "Preventivo.pdf" non è stato trovato')
        def output = new ByteArrayOutputStream()
        def document = new PdfReader(file.bytes)
        def writer = new PdfStamper(document, output)
        def canvas = writer.getOverContent(1)
        canvas.saveState()
        def firstLine = 450, lineHeight = 20
        applicazioni.eachWithIndex { Applicazione applicazione, index ->
            writeText(canvas, 12, firstLine - (lineHeight * index), applicazione.dealer.ragioneSociale, 5)
        }
        canvas.restoreState()
        writer.close()
        document.close()
        return output
    }

    private void writeText(canvas, x, y, String text, fontSize = 8, color = [0, 0, 0]){
        if(text) {
            canvas.beginText()
            canvas.moveText(x as float,y as float)
            canvas.setFontAndSize(BaseFont.createFont(), fontSize as float)
            canvas.setRGBColorFill(color[0], color[1], color[2])
            canvas.showText(text)
            canvas.endText()
        }
    }

    def mergePdf(streams) {
        def pdf = new Document()
        def output = new ByteArrayOutputStream()
        PdfCopy merged = PdfCopy.newInstance(pdf, output)
        pdf.open()
        streams.each { stream ->
            def pdfReader = new PdfReader(stream instanceof byte[] ? stream : stream.toByteArray())
            for(def i = 1; i <= pdfReader.getNumberOfPages(); i++) { merged.addPage(merged.getImportedPage(pdfReader, i)) }
            merged.freeReader(pdfReader)
        }
        merged.close()
        pdf.close()
        output.close()
        return output
    }
}
