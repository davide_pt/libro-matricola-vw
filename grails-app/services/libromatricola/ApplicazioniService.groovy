package libromatricola


import grails.converters.JSON
import groovy.time.TimeCategory
import it.mach1.excel.builder.ExcelBuilder
import org.codehaus.groovy.grails.web.util.WebUtils
import polizze.*
import polizze.TracciatoIass.*
import org.apache.poi.xssf.usermodel.*
import org.apache.poi.ss.util.CellRangeAddress as CRA
import utenti.Log

import java.awt.*
import java.text.SimpleDateFormat

import static polizze.Operazione.ANNULLAMENTO
import static polizze.ProdottoCheck.*
import static polizze.Operazione.*
import static polizze.StatoApplicazione.*
import static polizze.TipoTracciato.*
import static libromatricola.TipologiaVeicolo.*
import static java.util.Calendar.*

class ApplicazioniService {

    static transactional = false
    def mailService
    def mandService
    def calcoliService
    def ftpService
    def grailsApplication

    def generaNumeroApplicazione(dealer) {
        def numero = ++dealer.numerazioneApplicazioni
        //dealer.save()
        if(!dealer.save()) {
            println "Errori aggiornamento numerazione dealer"
            return "GVW$dealer.numeroLibroMatricola" + "$numero".leftPad(4, '0')

        }else{
            return "GVW$dealer.numeroLibroMatricola" + "$numero".leftPad(4, '0')

        }
    }

    def escludiApplicazione(applicazione, codice, dEsclu, pagata, motivoEsclusione) {
        if(applicazione.stato == PREVENTIVO) applicazione.deleted = true
        else if(codice == "-1") {
            applicazione.stato = ELIMINATA
            applicazione.deleted = true
            applicazione.motivoEsclusione=motivoEsclusione.trim()
            if(!applicazione.save()) {
                throw new RuntimeException("Errore annullamento applicazione $applicazione.numero: $applicazione.errors")
            }else{

            }
        }
        else if(applicazione.stato == POLIZZA) {
            applicazione.stato = ESCLUSA
            applicazione.operazione = ANNULLAMENTO
            //applicazione.dataEsclusione = new Date()
            applicazione.dataEsclusione = Date.parse("dd-MM-yyyy", dEsclu)
            applicazione.motivoEsclusione=motivoEsclusione.trim()
            def tracciatiAnnulla
            if(!applicazione.save()) {
                throw new RuntimeException("Errore annullamento applicazione $applicazione.numero: $applicazione.errors")
            }else{
                def tracciatiInserimentoNuovi = Tracciato.findByApplicazioneAndOperazioneAndDataEsportazioneIsNull(applicazione, INSERIMENTO)
                tracciatiAnnulla= TracciatoIass.findByApplicazioneAndOperazioneAndDataEsportazioneIsNull(applicazione, ANNULLAMENTO)
                if(tracciatiInserimentoNuovi) {
                    tracciatiInserimentoNuovi*.delete()
                    /*tracciatiInserimentoNuovi.dataEsportazione=Date.parse("dd-MM-yyyy", dEsclu)
                    if(!tracciatiInserimentoNuovi.save()) throw new RuntimeException("Errore aggiornamento tracciato iassicur $tracciatiInserimentoNuovi.id: $tracciatiInserimentoNuovi.errors")*/
                }
                else {
                    /*def isSameMonth = applicazione.with { return dataDecorrenza.year == dataEsclusione.year && dataDecorrenza.month == dataEsclusione.month }
                    def tipologiaVeicolo = applicazione.tipologiaVeicolo
                    def targaProva = applicazione.targaProva
                    def prodotti = applicazione.with {
                        def selezionati = []
                        if(rc == SI) selezionati << "rc"
                        if(cvt == SI) selezionati << "cvt"
                        if(kasko == SI && cvt == SI) selezionati << "kasko"
                        if(tutela == SI) selezionati << "tutela giudiziaria"
                        if(infortuni == A) selezionati << "infortuni 100.000"
                        if(infortuni == B) selezionati << "infortuni 200.000"
                        return selezionati
                    }
                    def tariffa = applicazione.tariffa
                    def provincia = applicazione.provincia
                    def veicolo = Veicolo.findByMarcaAndModello(applicazione.marca, applicazione.modello) ?: Veicolo.findByMarcaAndModelloIsNull(applicazione.marca) ?: Veicolo.findByMarca("Altro")
                    def valore = applicazione.valoreAssicurato
                    def calcoli = calcoliService.calcola(tipologiaVeicolo, targaProva, prodotti, tariffa, provincia, veicolo, valore, codice == "002" ? applicazione.dataDecorrenza : applicazione.dataEsclusione).collectEntries { garanzia, calcoliGaranzia ->
                        def calcoliCorretti = calcoliGaranzia.collectEntries { var, val ->
                            if(codice == "002" || (codice == "006" && (!applicazione.pagata || isSameMonth))) [var, -val]
                            else if(codice == "006" && !isSameMonth) {
                                if(var == "imposte") [var, 0.0]
                                else [var, -val]
                            }
                        }
                        if(calcoliCorretti.with { return imposte == 0.0 && pcl != imponibile }) calcoliCorretti.pcl = calcoliCorretti.imponibile
                        [garanzia, calcoliCorretti]
                    }
                    generaTracciato(applicazione, calcoli, false, codice)*/
                    def dataCreazione=Date.parse("yyyy-MM-dd","2017-10-03")
                    if(applicazione.rc == SI && applicazione.dateCreated.clearTime()<=dataCreazione) generaTracciatoANIAAnnulla(applicazione, dEsclu)
                }
                if (tracciatiAnnulla){
                    tracciatiAnnulla*.delete()
                    /* tracciatiAnnulla.dataEsportazione=Date.parse("dd-MM-yyyy", dEsclu)
                     if(!tracciatiAnnulla.save()) throw new RuntimeException("Errore aggiornamento tracciato annullamento $tracciatiAnnulla.id: $tracciatiAnnulla.errors")*/
                    generaTracciatoAnnullamento(applicazione,false,true,false,dEsclu,pagata)
                }else{
                    generaTracciatoAnnullamento(applicazione,false,true,false,dEsclu,pagata)
                }
            }
        }
    }

    def appenaEmessa(id) {
        if(Applicazione.exists(id)) return Tracciato.countByApplicazioneAndOperazioneAndDataEsportazioneIsNull(Applicazione.read(id), INSERIMENTO) > 0
        else return false
    }
/*****novembre 2016*/
    def generaTracciatoAnnullamento(applicazione, boolean daExcel, persist = true, boolean tasse, dEsclu=null,pagata=null) {
        def logg
        def session = WebUtils.retrieveGrailsWebRequest().session
        def utente = session.utente
        def tracciati=[]
        def resultTra = [tracciati: [], error: [], totaleTracci:0, totaleError:0]
        def totalTracc=0
        def totalErr=0
        def totalErrData=0
        if(daExcel){
            logg =new Log(parametri: "inizio a preparare i dati da usare nel tracciato", operazione: "generazione tracciati annullamenti tramite excel", pagina: "Annullamenti da Excel", utente:utente )
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            def nome,risc,tasseCol,premio,impo,ppslip,pdu
            applicazione.each(){  valore ->
                if(valore.nome=='' || valore.nome==null){
                    // resultTra.error<<("riga vuota,")
                    //totalErr++
                }
                else{
                    totalErrData=0
                    risc=valore.targa
                    nome=valore.nome
                    def numAppli=valore.numero.substring(0,8)
                    def rcNum=valore.numero.substring(8,10)
                    ppslip=  new BigDecimal(valore.ppslip)
                    impo=new BigDecimal(valore.imponibile)
                    premio=new BigDecimal(valore.premio)
                    pdu=impo-ppslip
                    tasseCol=new BigDecimal(valore.tasse)
                    tasseCol=tasseCol.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                    impo=impo.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                    ppslip=ppslip.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                    pdu=pdu.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                    premio=premio.setScale(2, BigDecimal.ROUND_HALF_EVEN)

                    def formatter = new SimpleDateFormat("dd/MM/yyyy")
                    def newFormat = new SimpleDateFormat("yyyyMMdd")
                    def dataInizio=""
                    def dataDeco=""
                    def datascadenza= ""
                    def datascade= ""
                    def dataAnnulla= ""
                    if(valore.inizio instanceof Date){
                        dataDeco=newFormat.format(valore.inizio)
                    }else{
                        logg =new Log(parametri: "il formato della colonna data inizio deve essere di tipo DATE (valore: ${valore.inizio})", operazione: "generazione tracciati annullamenti tramite excel", pagina: "Annullamenti da Excel", utente:utente )
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        resultTra.error<<("il formato della colonna data inizio deve essere di tipo DATE valore: ${valore.inizio},")
                        totalErr++
                        totalErrData++
                    }
                    if(valore.fine instanceof Date){
                        datascade= newFormat.format(valore.fine)
                    }else{
                        logg =new Log(parametri: "il formato della colonna data scadenza deve essere di tipo DATE valore: ${valore.fine}", operazione: "generazione tracciati annullamenti tramite excel", pagina: "Annullamenti da Excel", utente:utente )
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        resultTra.error<<("il formato della colonna data scadenza deve essere di tipo DATE  (valore: ${valore.fine}),")
                        totalErr++
                        totalErrData++
                    }
                    if(valore.dataAnnullo instanceof Date){
                        dataAnnulla= valore.dataAnnullo
                    }else{
                        logg =new Log(parametri: "il formato della colonna data annullamento deve essere di tipo DATE (valore: ${valore.dataAnnullo})", operazione: "generazione tracciati annullamenti tramite excel", pagina: "Annullamenti da Excel", utente:utente )
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        resultTra.error<<("il formato della colonna data annullamento deve essere di tipo DATE valore: ${valore.dataAnnullo},")
                        totalErr++
                        totalErrData++
                    }
                    def applicaExcel=Applicazione.findByNumeroIlike("%${numAppli}%")
                    def vecchiTracciatiAnnulla
                    if(applicaExcel && totalErrData==0){
                        vecchiTracciatiAnnulla = TracciatoIass.findAllByApplicazioneAndGaranziaAndOperazione(applicaExcel,rcNum,ANNULLAMENTO)
                        if(!vecchiTracciatiAnnulla){
                            def operazioni = ""
                            if(rcNum=='FI'){ operazioni="LMC"}
                            else if(rcNum=='I1' || rcNum=='I2'){operazioni="LMP"}
                            else if(rcNum=='RC'){operazioni="LMR"}
                            else if(rcNum=='TL'){operazioni="LMT"}

                            def kasko = [
                                    presente      : rcNum == "FI" && applicaExcel.kasko == SI ? "K" : "I",                                        //K = si, I = no
                            ]
                            def tracciato = [
                                    tipoAssic              : "I".leftPad(1),
                                    codMod                 : "${applicaExcel.codiceVeicolo}".leftPad(5),
                                    desver                 : "${(applicaExcel.modello?: "")}".leftPad(20),
                                    targa                  : risc.leftPad(15),
                                    cognomeAssicurato      : nome.leftPad(40),
                                    nomeAssicurato         : "".leftPad(40),
                                    prov                   : "${applicaExcel.dealer.indirizzoSedeLegale.provincia.sigla}".leftPad(2),
                                    valutaImporti          : "E".leftPad(1),
                                    valoreAssicurato       : "${(applicaExcel.valoreAssicurato * 100).round()}".leftPad(9),
                                    dataImmatricolazione   : "${applicaExcel.dataImmatricolazione.format('yyyyMMdd')}".leftPad(8),
                                    durataAssicurazione    : "${(durata(applicaExcel.dataDecorrenza, applicaExcel.dataScadenza) as String)}".leftPad(3),
                                    indirizzo              : "${applicaExcel.dealer.indirizzoSedeLegale.indirizzo}".leftPad(30),
                                    cap                    : "${applicaExcel.dealer.indirizzoSedeLegale.cap}".leftPad(5),
                                    citta                  : "${applicaExcel.dealer.indirizzoSedeLegale.citta}".leftPad(30),
                                    dataNascitaCliente     : "".leftPad(8),
                                    provinciaNascitaCliente: "".leftPad(2),
                                    sesso                  : "".leftPad(1),
                                    numeroPolizza          : "${valore.numero}".leftPad(10),
                                    codOperazione          : "${operazioni}".leftPad(6),
                                    dataAperturaContratto  : "".leftPad(8),
                                    importoAssicurato      : "".leftPad(9),
                                    partitaIva             : "${applicaExcel.dealer.partitaIva}".leftPad(16),
                                    marca                  : "${(applicaExcel.marca ?: "")}".leftPad(5),
                                    autocarro              : (applicaExcel.tipologiaVeicolo in [AUTOCARRO, AUTOCARRO_NOLEGGIO] ? "S" : applicaExcel.tipologiaVeicolo in [AUTOVETTURA, AUTOVETTURA_NOLEGGIO] ? "N" : "").leftPad(1),
                                    uso                    : "".leftPad(1),
                                    scadenzaVincolo        : "".leftPad(8),
                                    codiceDealer           : "${applicaExcel.dealer.codice}".leftPad(5),
                                    numerotelaio           : "${(applicaExcel.telaio ?: '')}".leftPad(17),
                                    numerorinnovo          : "".leftPad(2),
                                    vincolo                : "".leftPad(1),
                                    pcl                    : "".leftPad(9),
                                    oldContra              : "".leftPad(10),
                                    cvfisc                 : "".leftPad(4),
                                    quintali               : "${(applicaExcel.tipologiaVeicolo in [AUTOCARRO, AUTOCARRO_NOLEGGIO] ? "${applicaExcel.quintali}" : "")}".leftPad(4),
                                    dataDecorrenza         : "${dataDeco}".leftPad(8),
                                    buyBack                : "".leftPad(1),
                                    satellitare            : "".leftPad(1),
                                    dataScadenza           : "${datascade}".leftPad(8),
                                    rc                     : "".leftPad(1),
                                    telefonocasa           : "${(applicaExcel.dealer.telefono ?: '')}".leftPad(15),
                                    telefonoufficio        : "".leftPad(15),
                                    telefono               : "".leftPad(15),
                                    cellulare              : "".leftPad(15),
                                    compconc               : "${(ppslip*100).round()}".leftPad(9),
                                    provvMach1             : "0".leftPad(9),
                                    provvSocCommer         : "0".leftPad(9),
                                    totaleCaricamenti      : "${(ppslip*100).round()}".leftPad(9),
                                    imponibile             : "${(impo*100).round()}".leftPad(9),
                                    imposte                : "${(tasseCol*100).round()}".leftPad(9),
                                    pdu                    : "${(pdu*100).round()}".leftPad(9),
                                    codiceIBAN             : "".leftPad(34),
                                    collisione             : "".leftPad(1),
                                    codliscass             : "".leftPad(5),
                                    pduscont               : "".leftPad(1),
                                    valorenuo              : "".leftPad(1),
                                    tipoVeicolo            : "".leftPad(1),
                                    codStatus              : "".leftPad(1),
                                    codcanalevendita       : "".leftPad(4),
                                    codiceCanaVendRinn     : "".leftPad(4),
                                    codInvioCliente        : "".leftPad(4),
                                    tipoMotore             : "".leftPad(2),
                                    emaildealer            : "".leftPad(30),
                                    indidealer             : "".leftPad(30),
                                    faxdealer              : "".leftPad(15),
                                    pclclientelistino      : "".leftPad(7),
                                    mailCliente            : "".leftPad(30),
                                    secureplus             : "".leftPad(1),
                                    codiceCompagniaAssicurazione:  "".leftPad(4),
                                    codiceCompagniaSecurplus: "".leftPad(4),
                                    modalitaPagamento      : "".leftPad(2),
                                    pack                   : "".leftPad(1),
                                    tipoCopertura          : "".leftPad(1),
                                    twoSafe                : "".leftPad(1),
                                    pduAssistenza          : "".leftPad(9),
                                    imponibileAssistenza   : "".leftPad(9),
                                    tasseassistenza        : "".leftPad(9),
                                    cilindrata             :  "".leftPad(5),
                                    tipoPolizza            : "".leftPad(4)
                            ].values().join()
                            if (persist) {
                                logg =new Log(parametri: "${tracciato}", operazione: "generazione tracciati annullamenti tramite excel", pagina: "Annullamenti da Excel", utente:utente )
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                if(applicaExcel.stato == PREVENTIVO) applicaExcel.deleted = true
                                else if(applicaExcel.stato == POLIZZA) {
                                    applicaExcel.stato = ESCLUSA
                                    applicaExcel.operazione = ANNULLAMENTO
                                    applicaExcel.dataEsclusione = new Date()
                                }
                                tracciati.add(applicaExcel.addToTracciAssicur(operazione: Operazione.ANNULLAMENTO, garanzia: rcNum.toString().toLowerCase(), tracciato: tracciato, tipo: TipoTracciato.ANNULLAMENTO, dataAnnullamento:dataAnnulla))
                                if(!applicaExcel.save(flush: true)){
                                    logg =new Log(parametri: "il tracciato per la polizza ${valore.numero} non e' stato salvato errore--> ${applicaExcel.errors}", operazione: "generazione tracciati annullamenti tramite excel", pagina: "Annullamenti da Excel", utente:utente )
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    println "${applicaExcel.errors.fieldErrors}"
                                    resultTra.error<<("errore genrazione tracciato per polizza ${valore.numero},")
                                    totalErr++
                                }else{
                                    logg =new Log(parametri: "il tracciato per la polizza ${valore.numero} e' stato salvato", operazione: "generazione tracciati annullamenti tramite excel", pagina: "Annullamenti da Excel", utente:utente )
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    resultTra.tracciati<<("tracciato generato per la polizza ${valore.numero},")
                                    totalTracc++
                                }
                            }
                        }else {
                            resultTra.error<<("il tracciato per questa polizza ${valore.numero} e' stato gia' generato,")
                            totalErr++
                        }
                    }else if (!applicaExcel){
                        resultTra.error<<("la polizza non esiste nel sistema ${valore.numero},")
                        totalErr++
                    }else if (totalErrData>0){

                    }
                }

            }

        }else{
            logg =new Log(parametri: "inizio a preparare i dati da usare nel tracciato", operazione: "generazione tracciati annullamenti", pagina: "Elenco applicazioni", utente:utente )
            if(!logg.save()) println "Errori salvataggio log reponse: ${logg.errors}"
            def operazioni = [
                    "cvt": [numero: "${applicazione.numero.replace("GVW", "")}FI", (AUTOVETTURA): [ramo: "A25.01", operazione: "LMC"], (AUTOCARRO): [ramo: "A25.01", operazione: "LMC"],(AUTOVETTURA_NOLEGGIO): [ramo: "A25.01", operazione: "LMC"], (AUTOCARRO_NOLEGGIO): [ramo: "A25.01", operazione: "LMC"], targheProva: [ramo: "A25.01", operazione: "LMC"], (MOTO): [ramo: "A25.01", operazione: "LMC"],(TARGAPROVA): [ramo: "A25.01", operazione: "LMC"]],
                    "rc": [numero: "${applicazione.numero.replace("GVW", "")}RC", (AUTOVETTURA): [ramo: "A25.02", operazione: "LMR"], (AUTOCARRO): [ramo: "A25.02", operazione: "LMR"], (AUTOVETTURA_NOLEGGIO): [ramo: "A25.02", operazione: "LMR"], (AUTOCARRO_NOLEGGIO): [ramo: "A25.02", operazione: "LMR"], targheProva: [ramo: "A25.02", operazione: "LMR"], (MOTO): [ramo: "A25.02", operazione: "LMR"],(TARGAPROVA): [ramo: "A25.02", operazione: "LMR"]],
                    "tutela giudiziaria": [numero: "${applicazione.numero.replace("GVW", "")}TL", (AUTOVETTURA): [ramo: "A25.03", operazione: "LMT"], (AUTOCARRO): [ramo: "A25.03", operazione: "LMT"],(AUTOVETTURA_NOLEGGIO): [ramo: "A25.03", operazione: "LMT"], (AUTOCARRO_NOLEGGIO): [ramo: "A25.03", operazione: "LMT"], targheProva: [ramo: "A25.03", operazione: "LMT"], (MOTO): [ramo: "A25.03", operazione: "LMT"],(TARGAPROVA): [ramo: "A25.03", operazione: "LMT"]],
                    "infortuni 100.000": [numero: "${applicazione.numero.replace("GVW", "")}I1", (AUTOVETTURA): [ramo: "A25.04", operazione: "LMP"], (AUTOCARRO): [ramo: "A25.04", operazione: "LMP"], (AUTOVETTURA_NOLEGGIO): [ramo: "A25.04", operazione: "LMP"], (AUTOCARRO_NOLEGGIO): [ramo: "A25.04", operazione: "LMP"], targheProva: [ramo: "A25.04", operazione: "LMP"], (MOTO): [ramo: "A25.04", operazione: "LMP"],(TARGAPROVA): [ramo: "A25.04", operazione: "LMP"]],
                    "infortuni 200.000": [numero: "${applicazione.numero.replace("GVW", "")}I2", (AUTOVETTURA): [ramo: "A25.04", operazione: "LMP"], (AUTOCARRO): [ramo: "A25.04", operazione: "LMP"], (AUTOVETTURA_NOLEGGIO): [ramo: "A25.04", operazione: "LMP"], (AUTOCARRO_NOLEGGIO): [ramo: "A25.04", operazione: "LMP"], targheProva: [ramo: "A25.04", operazione: "LMP"], (MOTO): [ramo: "A25.04", operazione: "LMP"],(TARGAPROVA): [ramo: "A25.04", operazione: "LMP"]]
            ]
            def garanzie = [
                    applicazione.cvt == SI ? "cvt" : null,
                    applicazione.rc == SI ? "rc" : null,
                    applicazione.tutela == SI ? "tutela giudiziaria" : null,
                    applicazione.infortuni == A ? "infortuni 100.000" : null,
                    applicazione.infortuni == B ? "infortuni 200.000" : null
            ].grep()
            def calcoli = applicazione.calcoli
            def tracciato = garanzie.collect { garanzia ->
                def ggtotali=0
                def ggrimborso=0
                def decorrenza1 = applicazione.dataDecorrenza, scadenza1 = applicazione.dataScadenza
                ggtotali = (scadenza1 - decorrenza1)+1
                def today=new Date()
                def dataEsclu=Date.parse("dd-MM-yyyy", dEsclu)
                def gggoduti = (dataEsclu - decorrenza1)+1
                ggrimborso=ggtotali-(gggoduti)
                println "${decorrenza1.getClass()}"
                println "${decorrenza1}"
                def ggdif = calcoliService.diffDateInDays(dataEsclu,scadenza1)
                logg =new Log(parametri: "dati delle date da passare al tracciato-->ggdifrci: ${ggdif}, garanzia: ${garanzia},decorrenza: $decorrenza1, scadenza:$scadenza1,ggtotali:$ggtotali,dataEsclu:$dataEsclu,ggconsumati:$gggoduti,ggrimborso:$ggrimborso, pcl>${calcoli[garanzia].pcl}, imponibile: ${calcoli[garanzia].imponibile}", operazione: "generazione tracciati annullamenti tramite applicazione", pagina: "Elenco applicazioni", utente:utente )
                if(!logg.save()) println "Errori salvataggio log reponse: ${logg.errors}"
                def kasko = [
                        presente: garanzia == "cvt" && applicazione.kasko == SI ? "K" : "I",                                        //K = si, I = no
                        pcl: garanzia == "cvt" && applicazione.kasko == SI ? calcoli["kasko"].pcl  : 0.0,
                        totaleCompenso: garanzia == "cvt" && applicazione.kasko == SI ? calcoli["kasko"].provvigioniVW  : 0.0,
                        imponibile: garanzia == "cvt" && applicazione.kasko == SI ? calcoli["kasko"].imponibile  : 0.0,
                        imposte: garanzia == "cvt" && applicazione.kasko == SI ? calcoli["kasko"].imposte  : 0.0,
                        pdu: garanzia == "cvt" && applicazione.kasko == SI ? calcoli["kasko"].pdu  : 0.0,
                ]
                println "pcl ${calcoli[garanzia].pcl}"
                println "imponibile ${calcoli[garanzia].imponibile}"
                if(applicazione.kasko==SI && garanzia=="cvt"){
                    logg =new Log(parametri: "l'applicazione ha il prodotto KASKO questi sono i dati -->$kasko", operazione: "generazione tracciati annullamenti tramite applicazione", pagina: "Elenco applicazioni", utente:utente )
                    if(!logg.save()) println "Errori salvataggio log reponse: ${logg.errors}"
                }

                /**nuova implementazione per la nuova tariffa rc**/
                def datanuovat = new Date().parse('yyyy/MM/dd', '2017/10/01')
                def premioRimborso,imponiA,dslip,ppslip,pdu,provMach1,tasseA,pslip
                if(applicazione.dataDecorrenza>=datanuovat){
                    logg =new Log(parametri: "l'applicazione ha la nuova tariffa rci ", operazione: "generazione tracciati annullamenti tramite applicazione", pagina: "Elenco applicazioni", utente:utente )
                    if(!logg.save()) println "Errori salvataggio log reponse: ${logg.errors}"
                    /**tariffa rc**/
                    def zRC="Z1"
                    def tassazione=1.2650

                    def z1=["AO","VC","NO","BI","VB","AT","PN","AL","CN","COR","BL","BG","BS","UD","CR","SO","MN","PV","BZ","LO","LC","TN","PC","RO","VA","MB","COR", "SI","CB","FE",
                            "VR","VT","IM","SV","GO","PR","VI","TR","MI","MT","MO","GR","TO (provincia)"]
                    def z2=["VS","NU","IS","RE","PU","TV","CI","PD","AR","PZ","OG","VE","TO (comune)","OT","FC","AQ","PG","TS","RA","CS","RG","EN","MC","TP","FM","RI","AP","GE","SR","CA","CH",
                            "SS","FR","TE","LI","AN","BO","RN","LE","CL","PA","AG","CZ"]
                    def z3=["ME","BN","CT","PI","FI","SP","KR","BA","LT","BT","RM","PE","FG","LU","AV","BR","SA","VV","PT","PO","CE","MS","RC","TA","NA"]

                    def t1950=["AO","BZ","TN"]
                    def t2300=["AV","SCV","GO","OR","PN","SO","TS","UD"]
                    def t2550=["TV"]
                    def t2600=["AQ"]
                    def t2650=["AG","AL","AN","AR","AP","AT","BA","BT","BL","BN","BG","BI","BO","BS","BR","CA","CL","CB","CI","CE","CT","CZ","CH","CO","CS","CR","KR","CN","EN","FE","FI","FM","FG","FC","FR","GE","GR","IM","IS","SP","LT","LE","LC","LI","LO","LU","MC","MN","MS","MT","VS","ME","MI","MO","MB","NA","NO","NU","OG","OT","PD","PA","PR","PV","PG","PU","PE","PC","PI","PT","PZ","PO","RG","RA","RC","RE","RI","RN","RM","RO","SA","SS","SV","SI","SR","TA","TE","TR","TO","TP","VA","VE","VB","VC","VR","VV","VI","VT"]
                    if( applicazione.provincia.sigla in z1) {
                        zRC="Z1"
                    }else if (applicazione.provincia.sigla in z2){
                        zRC="Z2"
                    }else if(applicazione.provincia.sigla in z3){
                        zRC="Z3"
                    }
                    if(applicazione.provincia.sigla in t1950){tassazione=1.1950}
                    else if(applicazione.provincia.sigla in t2300){tassazione=1.2300}
                    else if(applicazione.provincia.sigla in t2550){tassazione=1.2550}
                    else if(applicazione.provincia.sigla in t2600){tassazione=1.2600}
                    else if(applicazione.provincia.sigla in t2650){tassazione=1.2650}
                    else {tassazione=1.2650}
                    println "zRC --> ${zRC}"
                    println "tassazione app --> ${tassazione}"

                    /********/
                    def imponibile2017= calcoliService.imponibileRC2017(zRC,applicazione.tipologiaVeicolo, applicazione.targaProva, applicazione.cv, applicazione.destinazioneUso, applicazione.tariffaAutocarri)
                    println "imponibile rc 2017 $imponibile2017"
                     premioRimborso=[
                            assente:  garanzia== "rc" ? imponibile2017/360*ggdif : ((calcoli[garanzia].pcl + kasko.pcl)/ggtotali)*ggrimborso,
                            presente: 0.0
                    ]
                     imponiA = [
                            assente: (garanzia == "cvt"  || garanzia == "tutela giudiziaria") ? premioRimborso.assente /1.135 : garanzia== "rc" ? premioRimborso.assente  : premioRimborso.assente /1.025,
                            presente:  0.0
                    ]
                     dslip = [
                            presente: garanzia== "rc" ? imponibile2017/360*ggdif : ((calcoli[garanzia].imponibile +kasko.imponibile)/ggtotali)* ggrimborso,
                            assente:0.0
                    ]


                     ppslip=[
                            assente: garanzia == "cvt" ? premioRimborso.assente*0.15 : garanzia== "rc" ? premioRimborso.assente*0.05 :0.0,
                            presente: garanzia == "cvt" ? dslip.presente *0.15 : garanzia== "rc" ? dslip.presente *0.05 :0.0
                    ]

                     pdu=[
                            assente:  imponiA.assente-ppslip.assente,
                            presente: dslip.presente - ppslip.presente
                    ]
                     provMach1=[
                            assente: garanzia == "cvt" ? pdu.assente*0.20 : garanzia== "rc" ? pdu.assente*0.01 :garanzia== "tutela giudiziaria" ? pdu.assente*0.17 :pdu.assente*0.22,
                            presente: garanzia == "cvt" ? pdu.presente*0.20 : garanzia== "rc" ? pdu.presente*0.01 :garanzia== "tutela giudiziaria" ? pdu.presente*0.17 :pdu.presente*0.22,
                    ]
                     tasseA=[
                            assente: premioRimborso.assente-imponiA.assente,
                            presente: 0.0
                    ]
                    pslip=[
                            assente: ppslip.assente+provMach1.assente,
                            presente: ppslip.presente+provMach1.presente,
                    ]
                    logg =new Log(parametri: "dati degli importi da passare al tracciato-->valore pagata: $pagata, premioRimborso: $premioRimborso, imponiA:$imponiA, dslip:$dslip, ppslip:$ppslip, tasseA:$tasseA, kasko:$kasko", operazione: "generazione tracciati annullamenti tramite applicazione", pagina: "Elenco applicazioni", utente:utente )
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    //println "PREMIO ${premioRimborso.assente}"
                    //println "dslip ${dslip.presente}"
                   /* println "PREMIO1 ${premioRimborso.assente}"
                    println "PREMIO1 ${premioRimborso.assente}"
                    println "ggtotali ${ggtotali}"
                    println "ggrimborso ${ggrimborso}"*/
                }else{
                    logg =new Log(parametri: "l'applicazione ha la vecchia tariffa rci ", operazione: "generazione tracciati annullamenti tramite applicazione", pagina: "Elenco applicazioni", utente:utente )
                    if(!logg.save()) println "Errori salvataggio log reponse: ${logg.errors}"
                     premioRimborso=[
                            assente:   ((calcoli[garanzia].pcl + kasko.pcl)/ggtotali)*ggrimborso,
                            presente: 0.0
                    ]
                     imponiA = [
                            assente: (garanzia == "cvt" || garanzia == "tutela giudiziaria" )? premioRimborso.assente /1.135 : garanzia== "rc" ? premioRimborso.assente/1.2650  : premioRimborso.assente /1.025,
                            presente:  0.0
                    ]

                     dslip = [
                            presente:  ((calcoli[garanzia].imponibile +kasko.imponibile)/ggtotali)* ggrimborso,
                            assente:0.0
                    ]


                     ppslip=[
                            assente: garanzia == "cvt" ? premioRimborso.assente*0.15 : garanzia== "rc" ? premioRimborso.assente*0.05 :0.0,
                            presente: garanzia == "cvt" ? dslip.presente *0.15 : garanzia== "rc" ? dslip.presente *0.05 :0.0
                    ]

                     pdu=[
                            assente:  imponiA.assente-ppslip.assente,
                            presente: dslip.presente - ppslip.presente
                    ]
                     provMach1=[
                            assente: garanzia == "cvt" ? pdu.assente*0.20 : garanzia== "rc" ? pdu.assente*0.05 :garanzia== "tutela giudiziaria" ? pdu.assente*0.17 :pdu.assente*0.22,
                            presente: garanzia == "cvt" ? pdu.presente*0.20 : garanzia== "rc" ? pdu.presente*0.05 :garanzia== "tutela giudiziaria" ? pdu.presente*0.17 :pdu.presente*0.22,
                    ]
                     tasseA=[
                            assente: premioRimborso.assente-imponiA.assente,
                            presente: 0.0
                    ]
                     pslip=[
                            assente: ppslip.assente+provMach1.assente,
                            presente: ppslip.presente+provMach1.presente,
                    ]
                    logg =new Log(parametri: "dati degli importi da passare al tracciato-->valore pagata: $pagata, premioRimborso: $premioRimborso, imponiA:$imponiA, dslip:$dslip, ppslip:$ppslip, tasseA:$tasseA, kasko:$kasko", operazione: "generazione tracciati annullamenti tramite applicazione", pagina: "Elenco applicazioni", utente:utente )
                    if(!logg.save()) println "Errori salvataggio log reponse: ${logg.errors}"

                    println "PAGATA ${pagata}"
                    println "PREMIO assente ${premioRimborso.assente}"
                    println "imponibile  assente ${imponiA.assente}"
                    println "PREMIO1 presente ${dslip.presente}"
                    println "ggtotali ${ggtotali}"
                    println "ggrimborso ${ggrimborso}"
                }
                def tracciat = [
                        kasko: kasko.presente,
                        codiceModello: applicazione.codiceVeicolo.leftPad(5),
                        descrizioneVersione: (applicazione.modello ?: "").leftPad(20),
                        targaVeicolo: applicazione.targa.leftPad(15),
                        cognomeAssicurato: applicazione.dealer.ragioneSociale.leftPad(40),
                        nomeAssicurato: ''.leftPad(40),
                        provinciaResidenza: applicazione.dealer.indirizzoSedeLegale.provincia.sigla.leftPad(2),
                        valutaImporti: 'E',
                        valoreAssicurato: "${(applicazione.valoreAssicurato * 100).round()}".leftPad(9),
                        dataImmatricolazione: applicazione.dataImmatricolazione.format('yyyyMMdd').leftPad(8),
                        durataAssicurazione: (durata(applicazione.dataDecorrenza, applicazione.dataScadenza) as String).leftPad(3),
                        indirizzo: applicazione.dealer.indirizzoSedeLegale.indirizzo.leftPad(30),
                        cap: applicazione.dealer.indirizzoSedeLegale.cap.leftPad(5),
                        citta: applicazione.dealer.indirizzoSedeLegale.citta.leftPad(30),
                        dataNascitaCliente: ''.leftPad(8),
                        provinciaNascitaCliente: ''.leftPad(2),
                        sessoCilente: ''.leftPad(1),
                        contratto: operazioni[garanzia].numero.leftPad(10),
                        tipoOperazione: ( operazioni[garanzia][applicazione.tipologiaVeicolo].operazione).leftPad(6),
                        dataAperturaContratto: ''.leftPad(8),
                        importoAssicurato: ''.leftPad(9),
                        partitaIva: applicazione.dealer.partitaIva.leftPad(16),
                        marchioVettura: (applicazione.marca ?: "").leftPad(5),
                        autocarro: (applicazione.tipologiaVeicolo in [AUTOCARRO, AUTOCARRO_NOLEGGIO] ? "S" : applicazione.tipologiaVeicolo in [AUTOVETTURA, AUTOVETTURA_NOLEGGIO] ? "N" : "").leftPad(1),
                        uso: ''.leftPad(1),
                        dataFineVincolo: ''.leftPad(8),
                        codiceConcessionario: applicazione.dealer.codice.leftPad(5),
                        telaio: (applicazione.telaio ?: '').leftPad(17),
                        rinnovo: ''.leftPad(2),
                        vincolo: ''.leftPad(1),
                        pcl: "".leftPad(9),
                        oldContra: ''.leftPad(10),
                        cvfisc: ''.leftPad(4),
                        quintali: (applicazione.tipologiaVeicolo in [AUTOCARRO,AUTOCARRO_NOLEGGIO] ? "${applicazione.quintali}" : "").leftPad(4),
                        dataDecorrenza: applicazione.dataDecorrenza.format('yyyyMMdd').leftPad(8),
                        buyBack: ''.leftPad(1),
                        satellitare: ''.leftPad(1),
                        dataScadenza: applicazione.dataScadenza.format('yyyyMMdd').leftPad(8),
                        rcAuto: (garanzia == "rc" ? "S" : "N").leftPad(1),
                        telefono: (applicazione.dealer.telefono ?: '').leftPad(15),
                        telefono2: (applicazione.dealer.fax ?: '').leftPad(15),
                        telefono3: ''.leftPad(15),
                        cellulare: ''.leftPad(15),
                        compensoConcessionario: (pagata=="true" ? "${(ppslip.presente *100).round()}" : "${(ppslip.assente *100).round()}").leftPad(9),
                        compensoVenditore: '0'.leftPad(9),
                        provvigioneLordaRC: '0'.leftPad(9),
                        totaleCompenso: ((pagata=="true" || pagata==true)? "${(ppslip.presente *100).round()}" : "${(ppslip.assente *100).round()}").leftPad(9),
                        imponibile: ((pagata=="true" || pagata==true )? "${( dslip.presente* 100).round()}"  : "${(imponiA.assente * 100).round()}" ).leftPad(9),
                        tasse: ((pagata=="true" || pagata==true) ? "${(tasseA.presente * 100 ).round()}":"${(tasseA.assente * 100 ).round()}").leftPad(9),
                        pdu: ((pagata=="true" || pagata==true) ? "${(pdu.presente * 100).round()}":"${(pdu.assente * 100 ).round()}").leftPad(9),
                        codiceIBAN: ''.leftPad(34),
                        collisione: ''.leftPad(1),
                        codiceListino: ''.leftPad(5),
                        scontoPdu: ''.leftPad(1),
                        valoreNuovo: "".leftPad(1),
                        tipoVeicolo: (applicazione.tipologiaVeicolo == MOTO ? 'M' : '').leftPad(1),
                        codiceStatus: "".leftPad(1),
                        codiceCanaleVenditaPrimaPolizza: "".leftPad(4),
                        codiceCanaleVenditaRinnovo: "".leftPad(4),
                        codiceInvioDocumentoCliente: "".leftPad(4),
                        tipoMotore: "".leftPad(2),
                        mailDealer: "".leftPad(30),
                        indirizzoDealer: "".leftPad(30),
                        faxDealer: "".leftPad(15),
                        percPclClienteSuPclListino: "".leftPad(7),
                        mailCliente: "".leftPad(30),
                        securplus: "".leftPad(1),
                        codiceCompagniaAssicurazione: "".leftPad(4),
                        codiceCompagniaSecurplus: "".leftPad(4),
                        modalitaPagamento: "".leftPad(2),
                        pack: "".leftPad(1),
                        tipoCopertura: "".leftPad(1),
                        twoSafe: "".leftPad(1),
                        // 2 luglio 2014: aggiornamento tracciato RCI
                        pduAssistenza: "".leftPad(9),
                        imponibileAssistenza: "".leftPad(9),
                        imposteAssistenza: "".leftPad(9),
                        cilindrata: "".leftPad(5),
                        tipoPolizza: "".leftPad(4)
                ].values().join()

                if(persist){
                    logg =new Log(parametri: "${tracciat}", operazione: "generazione tracciati annullamenti", pagina: "Elenco applicazioni", utente:utente )
                    if(!logg.save()) println "Errori salvataggio log reponse: ${logg.errors}"
                    if(applicazione.stato == PREVENTIVO) applicazione.deleted = true
                    else if(applicazione.stato == POLIZZA) {
                        applicazione.stato = ESCLUSA
                        applicazione.operazione = ANNULLAMENTO
                        applicazione.dataEsclusione = dataEsclu
                        applicazione.premiorimborso=(premioRimborso.assente).round(2)

                    }

                    tracciati.add(applicazione.addToTracciAssicur(operazione:Operazione.ANNULLAMENTO ,garanzia: garanzia, tracciato: tracciat, tipo: TipoTracciato.ANNULLAMENTO, dataAnnullamento:dataEsclu))
                    if(!applicazione.save(flush: true)){
                        logg =new Log(parametri: "il tracciato per la polizza ${applicazione.numero} non e' stato salvato errore--> ${applicazione.errors}", operazione: "generazione tracciati annullamenti", pagina: "Elenco applicazioni", utente:utente )
                        if(!logg.save()) println "Errori salvataggio log reponse: ${logg.errors}"
                        println "${applicazione.errors.fieldErrors}"
                        resultTra.error<<("errore genrazione tracciato per polizza ${applicazione.numero},")
                        totalErr++
                    }else{

                        //applicazione.addToTracciAssicur(operazione:Operazione.ANNULLAMENTO ,garanzia: garanzia, tracciato: tracciat, tipo: TipoTracciato.ANNULLAMENTO, dataAnnullamento:dataEsclu)
                        println "${applicazione.note}"
                        logg =new Log(parametri: "il tracciato per la polizza ${applicazione.numero} e' stato salvato", operazione: "generazione tracciati annullamenti", pagina: "Elenco applicazioni", utente:utente )
                        if(!logg.save()) println "Errori salvataggio log reponse: ${logg.errors}"
                        resultTra.tracciati<<("tracciato generato per la polizza ${applicazione.numero},")
                        totalTracc++
                    }
                }
                //return tracciato
            }

        }
        resultTra.totaleError=totalErr
        resultTra.totaleTracci=totalTracc
        return resultTra
    }
    def generaTracciato(applicazione, calcoliAnnullamento = null, modifica = false, codice = "006", persist = true) {
        def targaProva=(applicazione.destinazioneUso=="Targa prova")? true:false
        def destUso=applicazione.destinazioneUso
        def tipooper=""
        if(targaProva){
            tipooper="RCTRE"
        }else if (applicazione.tipologiaVeicolo==TipologiaVeicolo.MOTO){
            tipooper="RCMRE"
        }else if ((applicazione.tipologiaVeicolo ==TipologiaVeicolo.AUTOCARRO || applicazione.tipologiaVeicolo ==TipologiaVeicolo.AUTOVETTURA) && applicazione.destinazioneUso=="Noleggio"){
            tipooper="RCNRE"
        }else if ((applicazione.tipologiaVeicolo ==TipologiaVeicolo.AUTOCARRO || applicazione.tipologiaVeicolo ==TipologiaVeicolo.AUTOVETTURA) && applicazione.destinazioneUso=="Proprio"){
            tipooper="RCPRE"
        }

        def operazioni = [
                "cvt": [numero: "${applicazione.numero.replace("GVW", "")}FI", (AUTOVETTURA): [ramo: "A25.01", operazione: "FIP"], (AUTOCARRO): [ramo: "A25.01", operazione: "FIP"], (AUTOVETTURA_NOLEGGIO): [ramo: "A25.01", operazione: "FIN"], (AUTOCARRO_NOLEGGIO): [ramo: "A25.01", operazione: "FIN"], targheProva: [ramo: "A25.01", operazione: "FIT"], (MOTO): [ramo: "A25.01", operazione: "FIM"],(TARGAPROVA): [ramo: "A25.01", operazione: "FIN"]],
                "rc": [numero: "${applicazione.numero.replace("GVW", "")}RC", (AUTOVETTURA): [ramo: "A25.02", operazione: "RCP"], (AUTOCARRO): [ramo: "A25.02", operazione: "RCP"],(AUTOVETTURA_NOLEGGIO): [ramo: "A25.02", operazione: "RCN"], (AUTOCARRO_NOLEGGIO): [ramo: "A25.02", operazione: "RCN"], targheProva: [ramo: "A25.02", operazione: "RCT"], (MOTO): [ramo: "A25.02", operazione: "RCM"],(TARGAPROVA): [ramo: "A25.01", operazione: "RCN"]],
                "tutela giudiziaria": [numero: "${applicazione.numero.replace("GVW", "")}TL", (AUTOVETTURA): [ramo: "A25.03", operazione: "TLP"], (AUTOCARRO): [ramo: "A25.03", operazione: "TLP"], (AUTOVETTURA_NOLEGGIO): [ramo: "A25.03", operazione: "TLN"], (AUTOCARRO_NOLEGGIO): [ramo: "A25.03", operazione: "TLN"], targheProva: [ramo: "A25.03", operazione: "TLT"], (MOTO): [ramo: "A25.03", operazione: "TLM"],(TARGAPROVA): [ramo: "A25.01", operazione: "TLN"]],
                "infortuni 100.000": [numero: "${applicazione.numero.replace("GVW", "")}I1", (AUTOVETTURA): [ramo: "A25.04", operazione: "I1P"], (AUTOCARRO): [ramo: "A25.04", operazione: "I1P"], (AUTOVETTURA_NOLEGGIO): [ramo: "A25.04", operazione: "I1N"], (AUTOCARRO_NOLEGGIO): [ramo: "A25.04", operazione: "I1N"],  targheProva: [ramo: "A25.04", operazione: "I1T"], (MOTO): [ramo: "A25.04", operazione: "I1M"],(TARGAPROVA): [ramo: "A25.01", operazione: "I1N"]],
                "infortuni 200.000": [numero: "${applicazione.numero.replace("GVW", "")}I2", (AUTOVETTURA): [ramo: "A25.04", operazione: "I2P"], (AUTOCARRO): [ramo: "A25.04", operazione: "I2P"], (AUTOVETTURA_NOLEGGIO): [ramo: "A25.04", operazione: "I2N"], (AUTOCARRO_NOLEGGIO): [ramo: "A25.04", operazione: "I2N"], targheProva: [ramo: "A25.04", operazione: "I2T"], (MOTO): [ramo: "A25.04", operazione: "I2M"],(TARGAPROVA): [ramo: "A25.01", operazione: "I2N"]]
        ]
        def garanzie = [
                applicazione.cvt == SI ? "cvt" : null,
                applicazione.rc == SI ? "rc" : null,
                applicazione.tutela == SI ? "tutela giudiziaria" : null,
                applicazione.infortuni == A ? "infortuni 100.000" : null,
                applicazione.infortuni == B ? "infortuni 200.000" : null
        ].grep()
        def calcoli = calcoliAnnullamento ?: applicazione.calcoli
        def tracciati = garanzie.collect { garanzia ->
            def kasko = [
                    presente: garanzia == "cvt" && applicazione.kasko == SI ? "K" : "I",                                        //K = si, I = no
                    pcl: garanzia == "cvt" && applicazione.kasko == SI ? calcoli["kasko"].pcl * 100 : 0.0,
                    totaleCompenso: garanzia == "cvt" && applicazione.kasko == SI ? calcoli["kasko"].provvigioniVW * 100 : 0.0,
                    imponibile: garanzia == "cvt" && applicazione.kasko == SI ? calcoli["kasko"].imponibile * 100 : 0.0,
                    imposte: garanzia == "cvt" && applicazione.kasko == SI ? calcoli["kasko"].imposte * 100 : 0.0,
                    pdu: garanzia == "cvt" && applicazione.kasko == SI ? calcoli["kasko"].pdu * 100 : 0.0,
            ]
            def tracciato = [
                    kasko: kasko.presente,
                    codiceModello: applicazione.codiceVeicolo.leftPad(5),
                    descrizioneVersione: (applicazione.modello ?: "").leftPad(20),
                    targaVeicolo: applicazione.targa.leftPad(15),
                    cognomeAssicurato: applicazione.dealer.ragioneSociale.leftPad(40),
                    nomeAssicurato: ''.leftPad(40),
                    provinciaResidenza: applicazione.dealer.indirizzoSedeLegale.provincia.sigla.leftPad(2),
                    valutaImporti: 'E',
                    valoreAssicurato: "${(applicazione.valoreAssicurato * 100).round()}".leftPad(9),
                    dataImmatricolazione: applicazione.dataImmatricolazione.format('yyyyMMdd').leftPad(8),
                    durataAssicurazione: (durata(applicazione.dataDecorrenza, applicazione.dataScadenza) as String).leftPad(3),
                    indirizzo: applicazione.dealer.indirizzoSedeLegale.indirizzo.leftPad(30),
                    cap: applicazione.dealer.indirizzoSedeLegale.cap.leftPad(5),
                    citta: applicazione.dealer.indirizzoSedeLegale.citta.leftPad(30),
                    dataNascitaCliente: ''.leftPad(8),
                    provinciaNascitaCliente: ''.leftPad(2),
                    sessoCilente: ''.leftPad(1),
                    contratto: operazioni[garanzia].numero.leftPad(10),
                    //tipoOperazione: (applicazione.operazione == ANNULLAMENTO ? codice : modifica ? "003" : targaProva ? operazioni[garanzia].targheProva.operazione : operazioni[garanzia][applicazione.tipologiaVeicolo].operazione).leftPad(6),
                    tipoOperazione: (applicazione.operazione == ANNULLAMENTO ? codice : modifica ? "003" : (targaProva && garanzia != "rc")? operazioni[garanzia].targheProva.operazione : garanzia == "rc" ? tipooper : operazioni[garanzia][applicazione.tipologiaVeicolo].operazione).leftPad(6),
                    dataAperturaContratto: ''.leftPad(8),
                    importoAssicurato: ''.leftPad(9),
                    partitaIva: applicazione.dealer.partitaIva.leftPad(16),
                    marchioVettura: (applicazione.marca ?: "").leftPad(5),
                    autocarro: (applicazione.tipologiaVeicolo in [AUTOCARRO, AUTOCARRO_NOLEGGIO] ? "S" : applicazione.tipologiaVeicolo in [AUTOVETTURA, AUTOVETTURA_NOLEGGIO] ? "N" : "").leftPad(1),
                    uso: ''.leftPad(1),
                    dataFineFinanziamento: ''.leftPad(8),
                    codiceConcessionario: applicazione.dealer.codice.leftPad(5),
                    telaio: (applicazione.telaio ?: '').leftPad(17),
                    rinnovo: ''.leftPad(2),
                    prelazione: ''.leftPad(1),
                    pcl: "${(calcoli[garanzia].pcl * 100 + kasko.pcl).round()}".leftPad(9),
                    oldContra: ''.leftPad(10),
                    cvfisc: ''.leftPad(4),
                    quintali: (applicazione.tipologiaVeicolo in [AUTOCARRO, AUTOCARRO_NOLEGGIO] ? "${applicazione.quintali}" : "").leftPad(4),
                    dataDecorrenza: applicazione.dataDecorrenza.format('yyyyMMdd').leftPad(8),
                    byuBack: ''.leftPad(1),
                    satellitare: ''.leftPad(1),
                    dataScadenza: applicazione.dataScadenza.format('yyyyMMdd').leftPad(8),
                    rcAuto: (garanzia == "rc" ? "S" : "N").leftPad(1),
                    telefono: (applicazione.dealer.telefono ?: '').leftPad(15),
                    fax: (applicazione.dealer.fax ?: '').leftPad(15),
                    telefono2: ''.leftPad(15),
                    telefono3: ''.leftPad(15),
                    compensoConcessionario: '0'.leftPad(9),
                    compensoVenditore: '0'.leftPad(9),
                    provvigioneLordaRC: '0'.leftPad(9),
                    totaleCompenso: "${(calcoli[garanzia].provvigioniVW * 100 + kasko.totaleCompenso).round()}".leftPad(9),
                    imponibile: "${(calcoli[garanzia].imponibile * 100 + kasko.imponibile).round()}".leftPad(9),
                    tasse: "${(calcoli[garanzia].imposte * 100 + kasko.imposte).round()}".leftPad(9),
                    pdu: "${(calcoli[garanzia].pdu * 100 + kasko.pdu).round()}".leftPad(9),
                    codiceIBAN: ''.leftPad(34),
                    collisione: ''.leftPad(1),
                    codiceListino: ''.leftPad(5),
                    scontoPdu: ''.leftPad(1),
                    valoreNuovo: "".leftPad(1),
                    tipoVeicolo: (applicazione.tipologiaVeicolo == MOTO ? 'M' : '').leftPad(1),
                    codiceStatus: "".leftPad(1),
                    codiceCanaleVenditaPrimaPolizza: "".leftPad(4),
                    codiceCanaleVenditaRinnovo: "".leftPad(4),
                    codiceInvioDocumentoCliente: "".leftPad(4),
                    tipoMotore: "".leftPad(2),
                    mailDealer: "".leftPad(30),
                    indirizzoDealer: "".leftPad(30),
                    faxDealer: "".leftPad(15),
                    percPclClienteSuPclListino: "".leftPad(7),
                    mailCliente: "".leftPad(30),
                    securplus: "".leftPad(1),
                    codiceCompagniaAssicurazione: "".leftPad(4),
                    codiceCompagniaSecurplus: "".leftPad(4),
                    modalitaPagamento: "".leftPad(2),
                    pack: "".leftPad(1),
                    tipoCopertura: "".leftPad(1),
                    twoSafe: "".leftPad(1),
                    // 2 luglio 2014: aggiornamento tracciato RCI
                    pduAssistenza: "".leftPad(9),
                    imponibileAssistenza: "".leftPad(9),
                    imposteAssistenza: "".leftPad(9),
                    cilindrata: "".leftPad(5),
                    tipoPolizza: "".leftPad(4)
            ].values().join()
            if(persist) applicazione.addToTracciati(operazione: applicazione.operazione ,garanzia: garanzia, tracciato: tracciato)
            return tracciato
        }
        //if(applicazione.rc == SI) tracciati << generaTracciatoANIA(applicazione, persist)
        return tracciati
    }

    def generaTracciatoANIA(applicazione, persist = true, suffissoNumerazione = "AN") {
        if(applicazione.rc == SI) {
            def tracciato = [
                    kasko: 'I',
                    codiceModello: applicazione.codiceVeicolo.leftPad(5),
                    descrizioneVersione: (applicazione.modello ?: "").leftPad(20),
                    targaVeicolo: applicazione.targa.leftPad(15),
                    cognomeAssicurato: applicazione.dealer.ragioneSociale.leftPad(40),
                    nomeAssicurato: ''.leftPad(40),
                    provinciaResidenza: applicazione.dealer.indirizzoSedeLegale.provincia.sigla.leftPad(2),
                    valutaImporti: 'E',
                    valoreAssicurato: "${(applicazione.valoreAssicurato * 100).round()}".leftPad(9),
                    dataImmatricolazione: applicazione.dataImmatricolazione.format('yyyyMMdd').leftPad(8),
                    durataAssicurazione: (durata(applicazione.dataDecorrenza, applicazione.dataScadenza) as String).leftPad(3),
                    indirizzo: applicazione.dealer.indirizzoSedeLegale.indirizzo.leftPad(30),
                    cap: applicazione.dealer.indirizzoSedeLegale.cap.leftPad(5),
                    citta: applicazione.dealer.indirizzoSedeLegale.citta.leftPad(30),
                    dataNascitaCliente: ''.leftPad(8),
                    provinciaNascitaCliente: ''.leftPad(2),
                    sessoCilente: ''.leftPad(1),
                    contratto: "${applicazione.numero.replace("GVW", "")}$suffissoNumerazione".leftPad(10),
                    tipoOperazione: (applicazione.operazione == ANNULLAMENTO ? '2AN' : 'ANI').leftPad(6),
                    dataAperturaContratto: ''.leftPad(8),
                    importoAssicurato: ''.leftPad(9),
                    partitaIva: applicazione.dealer.partitaIva.leftPad(16),
                    marchioVettura: (applicazione.marca ?: "").leftPad(5),
                    autocarro: (applicazione.tipologiaVeicolo in [AUTOCARRO, AUTOCARRO_NOLEGGIO] ? "S" : applicazione.tipologiaVeicolo in [AUTOVETTURA, AUTOVETTURA_NOLEGGIO] ? "N" : "").leftPad(1),
                    uso: ''.leftPad(1),
                    dataFineFinanziamento: ''.leftPad(8),
                    codiceConcessionario: applicazione.dealer.codice.leftPad(5),
                    telaio: (applicazione.telaio ?: applicazione.targa).leftPad(17),
                    rinnovo: ''.leftPad(2),
                    prelazione: ''.leftPad(1),
                    pcl: '0'.leftPad(9),
                    oldContra: ''.leftPad(10),
                    cvfisc: ''.leftPad(4),
                    quintali: (applicazione.tipologiaVeicolo in [AUTOCARRO,AUTOCARRO_NOLEGGIO] ? "${applicazione.quintali}" : "").leftPad(4),
                    dataDecorrenza: applicazione.dataDecorrenza.format('yyyyMMdd').leftPad(8),
                    byuBack: ''.leftPad(1),
                    satellitare: ''.leftPad(1),
                    dataScadenza: applicazione.dataScadenza.format('yyyyMMdd').leftPad(8),
                    rcAuto: 'S',
                    telefono: (applicazione.dealer.telefono ?: '').leftPad(15),
                    fax: (applicazione.dealer.fax ?: '').leftPad(15),
                    telefono2: ''.leftPad(15),
                    telefono3: ''.leftPad(15),
                    compensoConcessionario: '0'.leftPad(9),
                    compensoVenditore: '0'.leftPad(9),
                    provvigioneLordaRC: '0'.leftPad(9),
                    totaleCompenso: '0'.leftPad(9),
                    imponibile: '0'.leftPad(9),
                    tasse: '0'.leftPad(9),
                    pdu: '0'.leftPad(9),
                    codiceIBAN: ''.leftPad(34),
                    collisione: ''.leftPad(1),
                    codiceListino: ''.leftPad(5),
                    scontoPdu: ''.leftPad(1),
                    valoreNuovo: "".leftPad(1),
                    tipoVeicolo: (applicazione.tipologiaVeicolo == MOTO ? 'M' : '').leftPad(1),
                    codiceStatus: "".leftPad(1),
                    codiceCanaleVenditaPrimaPolizza: "".leftPad(4),
                    codiceCanaleVenditaRinnovo: "".leftPad(4),
                    codiceInvioDocumentoCliente: "".leftPad(4),
                    tipoMotore: "".leftPad(2),
                    mailDealer: "".leftPad(30),
                    indirizzoDealer: "".leftPad(30),
                    faxDealer: "".leftPad(15),
                    percPclClienteSuPclListino: "".leftPad(7),
                    mailCliente: "".leftPad(30),
                    securplus: "".leftPad(1),
                    codiceCompagniaAssicurazione: "".leftPad(4),
                    codiceCompagniaSecurplus: "".leftPad(4),
                    modalitaPagamento: "".leftPad(2),
                    pack: "".leftPad(1),
                    tipoCopertura: "".leftPad(1),
                    twoSafe: "".leftPad(1),
                    // 2 luglio 2014: aggiornamento tracciato RCI
                    pduAssistenza: "".leftPad(9),
                    imponibileAssistenza: "".leftPad(9),
                    imposteAssistenza: "".leftPad(9),
                    cilindrata: "".leftPad(5),
                    tipoPolizza: "".leftPad(4)
            ].values().join()
            if(persist) applicazione.addToTracciati(operazione: applicazione.operazione ,garanzia: 'rc', tracciato: tracciato, tipo: ANIA)
            else return tracciato
        }
    }
    def generaTracciatoANIAAnnulla(applicazione, persist = true, suffissoNumerazione = "AN",dataEsclu ) {
        if(applicazione.rc == SI) {
            def tracciato = [
                    kasko: 'I',
                    codiceModello: applicazione.codiceVeicolo.leftPad(5),
                    descrizioneVersione: (applicazione.modello ?: "").leftPad(20),
                    targaVeicolo: applicazione.targa.leftPad(15),
                    cognomeAssicurato: applicazione.dealer.ragioneSociale.leftPad(40),
                    nomeAssicurato: ''.leftPad(40),
                    provinciaResidenza: applicazione.dealer.indirizzoSedeLegale.provincia.sigla.leftPad(2),
                    valutaImporti: 'E',
                    valoreAssicurato: "${(applicazione.valoreAssicurato * 100).round()}".leftPad(9),
                    dataImmatricolazione: applicazione.dataImmatricolazione.format('yyyyMMdd').leftPad(8),
                    durataAssicurazione: (durata(applicazione.dataDecorrenza, applicazione.dataScadenza) as String).leftPad(3),
                    indirizzo: applicazione.dealer.indirizzoSedeLegale.indirizzo.leftPad(30),
                    cap: applicazione.dealer.indirizzoSedeLegale.cap.leftPad(5),
                    citta: applicazione.dealer.indirizzoSedeLegale.citta.leftPad(30),
                    dataNascitaCliente: ''.leftPad(8),
                    provinciaNascitaCliente: ''.leftPad(2),
                    sessoCilente: ''.leftPad(1),
                    contratto: "${applicazione.numero.replace("GVW", "")}$suffissoNumerazione".leftPad(10),
                    tipoOperazione: (applicazione.operazione == ANNULLAMENTO ? '2AN' : 'ANI').leftPad(6),
                    dataAperturaContratto: ''.leftPad(8),
                    importoAssicurato: ''.leftPad(9),
                    partitaIva: applicazione.dealer.partitaIva.leftPad(16),
                    marchioVettura: (applicazione.marca ?: "").leftPad(5),
                    autocarro: (applicazione.tipologiaVeicolo in [AUTOCARRO, AUTOCARRO_NOLEGGIO] ? "S" : applicazione.tipologiaVeicolo in [AUTOVETTURA, AUTOVETTURA_NOLEGGIO] ? "N" : "").leftPad(1),
                    uso: ''.leftPad(1),
                    dataFineFinanziamento: ''.leftPad(8),
                    codiceConcessionario: applicazione.dealer.codice.leftPad(5),
                    telaio: (applicazione.telaio ?: applicazione.targa).leftPad(17),
                    rinnovo: ''.leftPad(2),
                    prelazione: ''.leftPad(1),
                    pcl: '0'.leftPad(9),
                    oldContra: ''.leftPad(10),
                    cvfisc: ''.leftPad(4),
                    quintali: (applicazione.tipologiaVeicolo in [AUTOCARRO,AUTOCARRO_NOLEGGIO] ? "${applicazione.quintali}" : "").leftPad(4),
                    dataDecorrenza: applicazione.dataDecorrenza.format('yyyyMMdd').leftPad(8),
                    byuBack: ''.leftPad(1),
                    satellitare: ''.leftPad(1),
                    dataScadenza: applicazione.dataScadenza.format('yyyyMMdd').leftPad(8),
                    rcAuto: 'S',
                    telefono: (applicazione.dealer.telefono ?: '').leftPad(15),
                    fax: (applicazione.dealer.fax ?: '').leftPad(15),
                    telefono2: ''.leftPad(15),
                    telefono3: ''.leftPad(15),
                    compensoConcessionario: '0'.leftPad(9),
                    compensoVenditore: '0'.leftPad(9),
                    provvigioneLordaRC: '0'.leftPad(9),
                    totaleCompenso: '0'.leftPad(9),
                    imponibile: '0'.leftPad(9),
                    tasse: '0'.leftPad(9),
                    pdu: '0'.leftPad(9),
                    codiceIBAN: ''.leftPad(34),
                    collisione: ''.leftPad(1),
                    codiceListino: ''.leftPad(5),
                    scontoPdu: ''.leftPad(1),
                    valoreNuovo: "".leftPad(1),
                    tipoVeicolo: (applicazione.tipologiaVeicolo == MOTO ? 'M' : '').leftPad(1),
                    codiceStatus: "".leftPad(1),
                    codiceCanaleVenditaPrimaPolizza: "".leftPad(4),
                    codiceCanaleVenditaRinnovo: "".leftPad(4),
                    codiceInvioDocumentoCliente: "".leftPad(4),
                    tipoMotore: "".leftPad(2),
                    mailDealer: "".leftPad(30),
                    indirizzoDealer: "".leftPad(30),
                    faxDealer: "".leftPad(15),
                    percPclClienteSuPclListino: "".leftPad(7),
                    mailCliente: "".leftPad(30),
                    securplus: "".leftPad(1),
                    codiceCompagniaAssicurazione: "".leftPad(4),
                    codiceCompagniaSecurplus: "".leftPad(4),
                    modalitaPagamento: "".leftPad(2),
                    pack: "".leftPad(1),
                    tipoCopertura: "".leftPad(1),
                    twoSafe: "".leftPad(1),
                    // 2 luglio 2014: aggiornamento tracciato RCI
                    pduAssistenza: "".leftPad(9),
                    imponibileAssistenza: "".leftPad(9),
                    imposteAssistenza: "".leftPad(9),
                    cilindrata: "".leftPad(5),
                    tipoPolizza: "".leftPad(4)
            ].values().join()
            if(persist) applicazione.addToTracciAssicur(operazione: Operazione.ANNULLAMENTO ,garanzia: 'rc', tracciato: tracciato, tipo: ANNULLAMENTOANIA, dataAnnullamento:dataEsclu)
            else return tracciato
        }
    }

    def scomponiTracciato(String tracciato) {
        def scomposto = [
                "kasko": tracciato[0..<1],
                "codice modello": tracciato[1..<6],
                "modello": tracciato[6..<26],
                "targa": tracciato[26..<41],
                "cognome assicurato": tracciato[41..<81],
                "nome assicurato": tracciato[81..<121],
                "provincia": tracciato[121..<123],
                "valuta": tracciato[123..<124],
                "valore assicurato": tracciato[124..<133],
                "data immatricolazione": tracciato[133..<141],
                "durata": tracciato[141..<144],
                "indirizzo": tracciato[144..<174],
                "cap": tracciato[174..<179],
                "città": tracciato[179..<209],
                "data di nascita": tracciato[209..<217],
                "provincia di nascita": tracciato[217..<219],
                "sesso": tracciato[219..<220],
                "numero applicazione": tracciato[220..<230],
                "codice operazione": tracciato[230..<236],
                "data apertura contratto": tracciato[236..<244],
                "importo assicurato": tracciato[244..<253],
                "partita iva": tracciato[253..<269],
                "marca": tracciato[269..<274],
                "autocarro": tracciato[274..<275],
                "uso": tracciato[275..<276],
                "data fine finanziamento": tracciato[276..<284],
                "codice dealer": tracciato[284..<289],
                "telaio": tracciato[289..<306],
                "rinnovo": tracciato[306..<308],
                "vincolo": tracciato[308..<309],
                "pcl": tracciato[309..<318],
                "old/contra": tracciato[318..<328],
                "cavalli fiscali": tracciato[328..<332],
                "quintali": tracciato[332..<336],
                "data decorrenza": tracciato[336..<344],
                "buy back": tracciato[344..<345],
                "satellitare": tracciato[345..<346],
                "data scadenza": tracciato[346..<354],
                "rc auto": tracciato[354..<355],
                "telefono": tracciato[355..<370],
                "fax": tracciato[370..<385],
                "telefono 2": tracciato[385..<400],
                "telefono 3": tracciato[400..<415],
                "provvigioni dealer": tracciato[415..<424],
                "provvigioni venditore": tracciato[424..<433],
                "provvigioni lorde rc": tracciato[433..<442],
                "totale provvigioni": tracciato[442..<451],
                "imponibile": tracciato[451..<460],
                "imposte": tracciato[460..<469],
                "pdu": tracciato[469..<478],
                "iban": tracciato[478..<512],
                "collisione": tracciato[512..<513],
                "codice listino": tracciato[513..<518],
                "sconto pdu": tracciato[518..<519],
                "valore a nuovo": tracciato[519..<520]
        ]
        if(tracciato.size() > 520) {
            scomposto += [
                    "tipo veicolo": tracciato[520..<521],
                    "codice status": tracciato[521..<522],
                    "cod. canale vendita prima polizza": tracciato[522..<526],
                    "cod. canale vendita rinnovo": tracciato[526..<530],
                    "cod. invio doc. cliente": tracciato[530..<534],
                    "tipo motore": tracciato[534..<536],
                    "email dealer": tracciato[536..<566],
                    "indirizzo dealer": tracciato[566..<596],
                    "fax dealer": tracciato[596..<611],
                    "% pcl cliente / pcl listino": tracciato[611..<618],
                    "email cliente": tracciato[618..<648],
                    "secureplus": tracciato[648..<649],
                    "cod. compagnia assicurativa": tracciato[649..<653],
                    "cod. compagnia securplus": tracciato[653..<657],
                    "modalità pagamento": tracciato[657..<659],
                    "pack": tracciato[659..<660],
                    "tipo copertura": tracciato[660..<661],
                    "2safe": tracciato[661..<662]
            ]
        }
        if(tracciato.size() > 662) {
            scomposto += [
                    "pdu assistenza": tracciato[662..<671],
                    "imponibile assistenza": tracciato[671..<680],
                    "imposte assistenza": tracciato[680..<689],
                    "colindrata": tracciato[689..<694],
                    "tipo polizza": tracciato[694..<698]
            ]
        }
        return scomposto
    }

    def estraiApplicazioni(filtro) {
        def applicazioni = Applicazione.withCriteria {
            if(filtro.dealer) eq 'dealer', filtro.dealer
            if(filtro.stato) eq 'stato', filtro.stato
            /*if(filtro.pagamentoFrom) ge 'dataPagamento', filtro.pagamentoFrom
            if(filtro.pagamentoTo) le 'dataPagamento', filtro.pagamentoTo*/
            /**luglio 2017*/
            if(filtro.decorrenzaFrom) ge 'dataDecorrenza', filtro.decorrenzaFrom
            if(filtro.decorrenzaTo) le 'dataDecorrenza', filtro.decorrenzaTo
            if(filtro.esclusioneFrom) ge 'dataEsclusione', filtro.esclusioneFrom
            if(filtro.esclusioneTo) le 'dataEsclusione', filtro.esclusioneTo
            if(filtro.scadenzaFrom) ge 'dataScadenza', filtro.scadenzaFrom
            if(filtro.scadenzaTo) le 'dataScadenza', filtro.scadenzaTo

            if(filtro.premioFrom != null) ge 'pcl', filtro.premioFrom
            if(filtro.premioTo != null) le 'pcl', filtro.premioTo
            or {
                if("cvt" in filtro.garanzie) eq 'cvt', SI
                if("k" in filtro.garanzie) eq 'kasko', SI
                if("rc" in filtro.garanzie) eq 'rc', SI
                if("tg" in filtro.garanzie) eq 'tutela', SI
                if("i100" in filtro.garanzie) eq 'infortuni', A
                if("i200" in filtro.garanzie) eq 'infortuni', B
            }
            if(filtro.riattivazioni) isNotNull "applicazioneEsclusa"
            projections { property 'id' }
        }
        def wb = new XSSFWorkbook(), dataFormat = wb.creationHelper.createDataFormat()
        def sheet = wb.createSheet("Applicazioni ${filtro.dealer ? filtro.dealer.ragioneSociale : 'tutti i dealer'}")
        sheet.createFreezePane(0, 1)
        sheet.setAutoFilter(CRA.valueOf("A1:AI1"))
        def headerStyle = wb.createCellStyle().with { style ->
            style.alignment = ALIGN_CENTER
            style.font = wb.createFont().with { font ->
                font.bold = true
                font.setColor(xssfColor(28, 99, 156))
                return font
            }
            style.borderTop = BORDER_THIN
            style.borderRight = BORDER_THIN
            style.borderBottom = BORDER_THIN
            style.borderLeft = BORDER_THIN
            style.setFillForegroundColor(xssfColor(214, 232, 246))
            style.fillPattern = SOLID_FOREGROUND
            return style
        }, dataStyle = wb.createCellStyle().with { style ->
            style.dataFormat = dataFormat.getFormat("dd/mm/yyyy")
            style.alignment = ALIGN_CENTER
            style.borderTop = BORDER_THIN
            style.borderRight = BORDER_THIN
            style.borderBottom = BORDER_THIN
            style.borderLeft = BORDER_THIN
            style.setFillForegroundColor(xssfColor(255, 255, 255))
            style.fillPattern = SOLID_FOREGROUND
            return style
        }, currencyStyle = wb.createCellStyle().with { style ->
            style.dataFormat = dataFormat.getFormat("€ #,##0.00")
            style.alignment = ALIGN_RIGHT
            style.borderTop = BORDER_THIN
            style.borderRight = BORDER_THIN
            style.borderBottom = BORDER_THIN
            style.borderLeft = BORDER_THIN
            style.setFillForegroundColor(xssfColor(255, 255, 255))
            style.fillPattern = SOLID_FOREGROUND
            return style
        }, defaultStyle = wb.createCellStyle().with { style ->
            style.alignment = ALIGN_LEFT
            style.borderTop = BORDER_THIN
            style.borderRight = BORDER_THIN
            style.borderBottom = BORDER_THIN
            style.borderLeft = BORDER_THIN
            style.setFillForegroundColor(xssfColor(255, 255, 255))
            style.fillPattern = SOLID_FOREGROUND
            return style
        }
        def fields = ["Dealer", "Numero libro matricola", "Tariffa", "Numero applicazione", "Creata il", "Provincia", "Zona", "Zona RC","Tipologia veicolo", "Marca", "Modello", "Rischio", "Quintali","Cilindrata", "CV", "Valore assicurato",
                      "Targa", "Telaio", "Immatricolato il", "Vincolo", "Stato", "Data decorrenza", "Data scadenza", "Data emissione", "Pagata", "Data pagamento", "Data esclusione", "CVT", "Kasko", "RC", "Tutela giudiziaria", "Infortuni",
                      "PCL", "Imponibile", "Imposte", "PDU", "Provvigioni Volkswagen", "Provvigioni Mach1"]

        def header = sheet.createRow(0)
        fields.eachWithIndex{ value, index ->
            header.createCell(index).with {
                cellStyle = headerStyle
                setCellValue(value)
            }
        }
        applicazioni.eachWithIndex{ id, index ->
            def row = sheet.createRow(index + 1), i = 0
            def applicazione = Applicazione.read(id)
            row.createCell(i++).with {
                cellStyle = defaultStyle
                setCellValue(applicazione.dealer.ragioneSociale)
            }
            row.createCell(i++).with {
                cellStyle = defaultStyle
                setCellValue(applicazione.dealer.numeroLibroMatricola)
            }
            row.createCell(i++).with {
                cellStyle = defaultStyle
                setCellValue(applicazione.tariffa.codice)
            }
            row.createCell(i++).with {
                cellStyle = defaultStyle
                setCellValue(applicazione.numero)
            }
            row.createCell(i++).with {
                cellStyle = dataStyle
                setCellValue(applicazione.dateCreated.format())
            }
            row.createCell(i++).with {
                cellStyle = defaultStyle
                setCellValue(applicazione.provincia.nome)
            }
            row.createCell(i++).with {
                cellStyle = defaultStyle
                setCellValue(applicazione.provincia.zona)
            }
            row.createCell(i++).with {
                cellStyle = defaultStyle
                setCellValue(applicazione.provincia.zonaRC)
            }
            row.createCell(i++).with {
                cellStyle = defaultStyle
                setCellValue((applicazione.targaProva ? "(P) " : "") + applicazione.tipologiaVeicolo.toString())
            }
            row.createCell(i++).with {
                cellStyle = defaultStyle
                setCellValue(applicazione.marca)
            }
            row.createCell(i++).with {
                cellStyle = defaultStyle
                setCellValue(applicazione.modello)
            }
            row.createCell(i++).with {
                cellStyle = defaultStyle
                setCellValue(applicazione.rischio.toString())
            }
            row.createCell(i++).with {
                cellStyle = defaultStyle
                setCellValue(applicazione.cilindrata)
            }
            row.createCell(i++).with {
                cellStyle = defaultStyle
                setCellValue(applicazione.quintali)
            }
            row.createCell(i++).with {
                cellStyle = defaultStyle
                setCellValue(applicazione.cv)
            }
            row.createCell(i++).with {
                cellStyle = currencyStyle
                setCellValue(applicazione.valoreAssicurato)
            }
            row.createCell(i++).with {
                cellStyle = defaultStyle
                setCellValue(applicazione.targa)
            }
            row.createCell(i++).with {
                cellStyle = defaultStyle
                setCellValue(applicazione.telaio)
            }
            row.createCell(i++).with {
                cellStyle = dataStyle
                setCellValue(applicazione.dataImmatricolazione?.format())
            }
            row.createCell(i++).with {
                cellStyle = defaultStyle
                setCellValue(applicazione.vincolo)
            }
            row.createCell(i++).with {
                cellStyle = defaultStyle
                setCellValue(applicazione.stato.toString())
            }
            row.createCell(i++).with {
                cellStyle = dataStyle
                setCellValue(applicazione.dataDecorrenza?.format())
            }
            row.createCell(i++).with {
                cellStyle = dataStyle
                setCellValue(applicazione.dataScadenza?.format())
            }
            row.createCell(i++).with {
                cellStyle = dataStyle
                setCellValue(applicazione.dataEmissione?.format())
            }
            row.createCell(i++).with {
                cellStyle = defaultStyle
                setCellValue(applicazione.pagata ? "Si" : "No")
            }
            row.createCell(i++).with {
                cellStyle = dataStyle
                setCellValue(applicazione.dataPagamento?.format())
            }
            row.createCell(i++).with {
                cellStyle = dataStyle
                setCellValue(applicazione.dataEsclusione?.format())
            }
            row.createCell(i++).with {
                cellStyle = currencyStyle
                setCellValue(applicazione.cvt == SI ? applicazione.calcoli["cvt"].pcl : 0.0)
            }
            row.createCell(i++).with {
                cellStyle = currencyStyle
                setCellValue(applicazione.kasko == SI ? applicazione.calcoli["kasko"].pcl : 0.0)
            }
            row.createCell(i++).with {
                cellStyle = currencyStyle
                setCellValue(applicazione.rc == SI ? applicazione.calcoli["rc"]?.pcl : 0.0)
            }
            row.createCell(i++).with {
                cellStyle = currencyStyle
                setCellValue(applicazione.tutela == SI ? applicazione.calcoli["tutela giudiziaria"].pcl : 0.0)
            }
            row.createCell(i++).with {
                cellStyle = currencyStyle
                setCellValue(applicazione.infortuni == A ? applicazione.calcoli["infortuni 100.000"].pcl : applicazione.infortuni == B ? applicazione.calcoli["infortuni 200.000"].pcl : 0.0)
            }
            row.createCell(i++).with {
                cellStyle = currencyStyle
                setCellValue(applicazione.pcl)
            }
            row.createCell(i++).with {
                cellStyle = currencyStyle
                setCellValue(applicazione.imponibile)
            }
            row.createCell(i++).with {
                cellStyle = currencyStyle
                setCellValue(applicazione.imposte)
            }
            row.createCell(i++).with {
                cellStyle = currencyStyle
                setCellValue(applicazione.pdu)
            }
            row.createCell(i++).with {
                cellStyle = currencyStyle
                setCellValue(applicazione.provvigioniVW)
            }
            row.createCell(i++).with {
                cellStyle = currencyStyle
                setCellValue(applicazione.provvigioniMach1)
            }
        }
        fields.size().times { index -> sheet.autoSizeColumn(index) }
        def excel = new ByteArrayOutputStream()
        wb.write(excel)
        excel.close()
        return excel.toByteArray()
    }

    def generaPreventivo(applicazioni, from, to) {
        def file = new FileInputStream(grailsApplication.mainContext.getResource('xls/Preventivi.xlsx')?.file)
        def wb = new XSSFWorkbook(file), dataFormat = wb.creationHelper.createDataFormat()
        def currencyStyle = wb.createCellStyle().with { style ->
            style.dataFormat = dataFormat.getFormat("€ #,##0.00")
            style.alignment = ALIGN_RIGHT
            style.borderTop = BORDER_THIN
            style.borderRight = BORDER_THIN
            style.borderBottom = BORDER_THIN
            style.borderLeft = BORDER_THIN
            style.setFillForegroundColor(xssfColor(255, 255, 255))
            style.fillPattern = SOLID_FOREGROUND
            return style
        }, defaultStyle = wb.createCellStyle().with { style ->
            style.alignment = ALIGN_LEFT
            style.borderTop = BORDER_THIN
            style.borderRight = BORDER_THIN
            style.borderBottom = BORDER_THIN
            style.borderLeft = BORDER_THIN
            style.setFillForegroundColor(xssfColor(255, 255, 255))
            style.fillPattern = SOLID_FOREGROUND
            return style
        }
        def sheet = wb.getSheetAt(0)
        sheet.getRow(0).getCell(0).setCellValue("Preventivi ${from.format()} - ${to.format()}")
        def firstRowIndex = sheet.getLastRowNum() + 1
        def columnIndexes = [
                [property: "dealer.ragioneSociale", columnIndex: 0, style: defaultStyle],
                [property: "dealer.indirizzoSedeLegale.indirizzo", columnIndex: 1, style: defaultStyle],
                [property: "dealer.indirizzoSedeLegale.citta", columnIndex: 2, style: defaultStyle],
                [property: "dealer.indirizzoSedeLegale.provincia.sigla", columnIndex: 3, style: defaultStyle],
                [property: "tipologiaVeicolo.value", columnIndex: 4, style: defaultStyle],
                [property: "tariffa.codice", columnIndex: 5, style: defaultStyle],
                [property: "valoreAssicurato", columnIndex: 6, style: currencyStyle],
                [property: "targa", columnIndex: 7, style: defaultStyle],
                [property: "modello", columnIndex: 8, style: defaultStyle],
                [property: "provincia.zona", columnIndex: 9, style: defaultStyle],
                [property: "rischio.name", columnIndex: 10, style: defaultStyle],
                [property: "rc.name", columnIndex: 11, style: defaultStyle],
                [property: "cvt.name", columnIndex: 12, style: defaultStyle],
                [property: "kasko.name", columnIndex: 13, style: defaultStyle],
                [property: "infortuni", columnIndex: 14, style: defaultStyle, formatter: { infortuni -> infortuni == A ? "SI" : "NO" }],
                [property: "infortuni", columnIndex: 15, style: defaultStyle, formatter: { infortuni -> infortuni == B ? "SI" : "NO" }],
                [property: "tutela.name", columnIndex: 16, style: defaultStyle],
                [property: "calcoli", columnIndex: 17, style: currencyStyle, formatter: { calcoli -> calcoli["rc"]?.pcl ?: 0.0 }],
                [property: "calcoli", columnIndex: 18, style: currencyStyle, formatter: { calcoli -> calcoli["cvt"]?.pcl ?: 0.0 }],
                [property: "calcoli", columnIndex: 19, style: currencyStyle, formatter: { calcoli -> calcoli["kasko"]?.pcl ?: 0.0 }],
                [property: "calcoli", columnIndex: 20, style: currencyStyle, formatter: { calcoli -> calcoli["infortuni 100.000"]?.pcl ?: 0.0 }],
                [property: "calcoli", columnIndex: 21, style: currencyStyle, formatter: { calcoli -> calcoli["infortuni 200.000"]?.pcl ?: 0.0 }],
                [property: "calcoli", columnIndex: 22, style: currencyStyle, formatter: { calcoli -> calcoli["tutela giudiziaria"]?.pcl ?: 0.0 }],
                [property: "pcl", columnIndex: 23, style: currencyStyle]
        ]
        applicazioni.eachWithIndex { Applicazione applicazione, index ->
            def row = sheet.createRow(firstRowIndex + index)
            columnIndexes.each { config ->
                def cell = row.createCell(config.columnIndex)
                def value = beanProperty(applicazione, config.property)
                if(config.formatter) value = config.formatter.call(value)
                cell.setCellStyle(config.style)
                cell.setCellValue(value)
            }
        }
        columnIndexes.size().times { index -> sheet.autoSizeColumn(index) }
        def excel = new ByteArrayOutputStream()
        wb.write(excel)
        excel.close()
        return excel.toByteArray()
    }

    private durata(decorrenza, scadenza) { return scadenza[MONTH] - decorrenza[MONTH] + 1 + (scadenza[YEAR] - decorrenza[YEAR]) * 12 }

    private xssfColor(r = 0, g = 0 ,b = 0) { new XSSFColor( new Color(r, g, b)) }

    private beanProperty(bean, property) {
        def path = property.tokenize(".[]")
        def value = bean
        for(sub in path) value = value != null ? value[sub.isNumber() ? sub as int : sub] : null
        //if(value instanceof Date || value instanceof BigDecimal) value = value.format()
        if(value instanceof Date) value = value.format()
        return value
    }

    def uploadFlussoEsclusioni( def utente=null) {
        def dir=null
        def utentecollegato=utente
        def logg
        def today = new Date()
        def tracciatiDaEsportare = Tracciatiannulla.list()

        def tracciati = tracciatiDaEsportare*.tracciato.join('\r\n')
        if(tracciati) {
            logg =new Log(parametri: "ho trovato dei tracciati da esportare", operazione: "caricamento tracciati annullamenti su ftp", pagina: "Annullamenti da Excel")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            def traccragg=tracciatiDaEsportare.groupBy({it.dataAnnullamento})
            traccragg.each{ k, v ->
                def tracci=v.join('\r\n')
                def file = tracci.bytes, fileName = "clper441_${k.format('yyyyMMddkkmm')}_rcigio.txt"
                ftpService.ftpsuploadFile(file, fileName, dir, utentecollegato,true)
                def flusso = new Flusso(file: file, fileName: fileName)
                if(!flusso.save()) {
                    mailService.sendMail {
                        multipart true
                        to 'priscila@presstoday.com'
                        subject 'Errore salvataggio flusso'
                        text "Si sono verificati degli errori durante il salvataggio del flusso '$fileName': $flusso.errors"
                        attach fileName, 'text/plain', file
                    }
                }
            }
            /*tracciatiDaEsportare.each{ tracciato->
                def file=tracciato.tracciato.bytes
                def dataAnn=tracciato.dataAnnullamento.format("yyyyMMddkkmm")
                def fileName = "clper441_${dataAnn}_rcigio.txt"
                def flusso = new Flusso(file: file, fileName: fileName)
                if(!flusso.save()) {
                    mailService.sendMail {
                        multipart true
                        to 'priscila@presstoday.com'
                        subject 'Errore salvataggio flusso'
                        text "Si sono verificati degli errori durante il salvataggio del flusso '$fileName': $flusso.errors"
                        attach fileName, 'text/plain', file
                    }
                }
            }*/
            tracciatiDaEsportare.each { tracciato -> tracciato.dataEsportazione = today }*.save()
            /*def flusso = new Flusso(file: file, fileName: fileName)
            if(!flusso.save()) {
                mailService.sendMail {
                    multipart true
                    to 'priscila@presstoday.com'
                    subject 'Errore salvataggio flusso'
                    text "Si sono verificati degli errori durante il salvataggio del flusso '$fileName': $flusso.errors"
                    attach fileName, 'text/plain', file
                }
            }*/
        } else {
            logg =new Log(parametri: "Non ci sono tracciati da esportare", operazione: "caricamento tracciati annullamenti su ftp", pagina: "Annullamenti da Excel")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            log.info "Non ci sono tracciati da esportare"
        }
    }
    def uploadFlussoCompagniaEsc(){
        def logg
        def now = new Date()
        def dataattivazioni=Date.parse("yyyy-MM-dd HH:mm:ss", "2017-10-03 00:00:00")
//        def dataggprima
//        dataggprima= use(TimeCategory) {Date.parse("dd.MM.yyyy", dataOdierna) -10.day}.format("dd.MM.yyyy")
        def fileName, fileContent
        def applicazioni=Applicazione.createCriteria().list(){
            //ge 'dateCreated', now
            //ge 'lastUpdated', now
            ge ('dataDecorrenza', dataattivazioni)
            between('lastUpdated', now-7, now-1)
            eq 'operazione', Operazione.ANNULLAMENTO
            eq 'rc', SI
        }

        //println "$dataattivazioni"
        if(applicazioni){
            def wb = ExcelBuilder.createXls {
                style("header") {
                    border style: "thin", color: "black"
                    align vertical: "center", horizontal: "center"

                }
                sheet("Tracciati") {
                    row(style: "header") {
                        cell("DATA INCLUSIONE")
                        cell("DATA ESCLUSIONE")
                        cell("MOTIVO ESCLUSIONE")
                        cell("DATA SCADENZA POLIZZA")
                        cell("MARCA")
                        cell("MODELLO")
                        cell("TIPO")
                        //cell("USO")
                        cell("TARGA")
                        cell("CV")
                        cell("KW")
                        cell("CILINDRATA")
                        cell("Q.LI")
                        cell("Q.LI RIMORCHIO")
                        cell("N. POSTI")
                        cell("ALIMENT.")
                        cell("VINCOLO/LEASING")
                        cell("SCAD LEASING/ ENTE VINCOL")
                        cell("2011")
                        cell("2012")
                        cell("2013")
                        cell("2014")
                        cell("2015")
                        cell("2016")
                        cell("RAGIONE SOCIALE CONCESSIONARIO")
                        cell("SEDE LEGALE CONCESSIONARIO")
                        cell("SIGLA PROVINCIA SEDE LEGALE")
                        cell("PARTITA IVA CONCESSIONARIO")
                        //cell("CODICE CONCESSIONARIO")
                    }
                        applicazioni.each{ applicazione ->
                            /*def cavalliF= CavalliFiscali.executeQuery('select potenzafiscale from CavalliFiscali as a where :parametro between  a.cilindrataMin and a.cilindrataMax',[parametro:applicazione.cilindrata])
                            logg =new Log(parametri: "i cavalli fiscali per l'applicazione ${applicazione.numero} sono ${cavalliF[0]}", operazione: "generazione file ESCLUSIONI compagnia", pagina: "JOB GENERAZIONE DOCUMENTI COMPAGNIA")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"*/
                            row(style: "content") {
                                cell(applicazione.dataDecorrenza?.format("dd/MM/yyyy"))
                                cell(applicazione.dataEsclusione?.format("dd/MM/yyyy"))
                                cell(applicazione.motivoEsclusione?:'')
                                cell(applicazione.dataScadenza?.format("dd/MM/yyyy"))
                                cell(applicazione.marca)
                                cell(applicazione.modello)
                                cell(((applicazione.tipologiaVeicolo==TipologiaVeicolo.AUTOCARRO || applicazione.tipologiaVeicolo==TipologiaVeicolo.AUTOCARRO_NOLEGGIO) && applicazione.destinazioneUso=="Noleggio") ? "AUTOCARRI NOLEGGIO LIBERO"   : ((applicazione.tipologiaVeicolo==TipologiaVeicolo.AUTOVETTURA  || applicazione.tipologiaVeicolo==TipologiaVeicolo.AUTOVETTURA_NOLEGGIO) && applicazione.destinazioneUso=="Noleggio") ? "AUTOVETTURE NOLEGGIO LIBERO"  : applicazione.tipologiaVeicolo.toString().toUpperCase())
                                //cell(applicazione.destinazioneUso.toString())
                                cell(applicazione.targa)
                                cell((applicazione.tipologiaVeicolo==TipologiaVeicolo.AUTOVETTURA || applicazione.tipologiaVeicolo==TipologiaVeicolo.AUTOVETTURA_NOLEGGIO) ? applicazione.cv : "")
                                cell((applicazione.tipologiaVeicolo==TipologiaVeicolo.AUTOVETTURA || applicazione.tipologiaVeicolo==TipologiaVeicolo.AUTOVETTURA_NOLEGGIO) ? applicazione.kw : "")
                                cell(applicazione.cilindrata)
                                cell((applicazione.tipologiaVeicolo==TipologiaVeicolo.AUTOCARRO || applicazione.tipologiaVeicolo==TipologiaVeicolo.AUTOCARRO_NOLEGGIO ) ? applicazione.quintali :"")
                                cell("")
                                cell((applicazione.tipologiaVeicolo==TipologiaVeicolo.MOTO) ? applicazione.nposti :"")
                                cell(applicazione.alimentazione.toString())
                                cell(applicazione.vincololeasing)
                                cell((applicazione.vincololeasing !='' && applicazione.vincololeasing !=null) ? applicazione.scadenzaVincolo?.format("dd/MM/yyyy"):"")
                                cell("")
                                cell("")
                                cell("")
                                cell("")
                                cell("")
                                cell("")
                                cell(applicazione.dealer.ragioneSociale)
                                cell("${applicazione.dealer.indirizzoSedeLegale.indirizzo} ${applicazione.dealer.indirizzoSedeLegale.cap} ${applicazione.dealer.indirizzoSedeLegale.citta.toString().toUpperCase()} ${applicazione.dealer.indirizzoSedeLegale.provincia.toString().toUpperCase()}")
                                cell(applicazione.dealer.indirizzoSedeLegale.provincia.sigla)
                                cell(applicazione.dealer.partitaIva)
                                //cell(applicazione.dealer.codice)
                            }
                            applicazione.discard()

                        }
                }
            }
            def stream = new ByteArrayOutputStream()
            wb.write(stream)
            stream.close()
            fileContent = stream.toByteArray()
            fileName = "flusso_esc_${new Date().format('yyyyMMddkkmm')}.xls"
        }else{
            fileName=null
            fileContent=null
        }

        return [(fileName): fileContent]

    }
    def uploadFlussoCompagniaAtt(){
        def logg
        def fileName, fileContent
        def now = new Date().clearTime()
        def applicazioni=Applicazione.createCriteria().list(){
            ge 'dataEmissione', now
            eq 'operazione', Operazione.INSERIMENTO
            eq 'rc', SI
        }
        if(applicazioni){
            def wb = ExcelBuilder.createXls {
                style("header") {
                    border style: "thin", color: "black"
                    align vertical: "center", horizontal: "center"
                }
                sheet("Tracciati") {
                    row(style: "header") {
                        cell("DATA INCLUSIONE")
                        cell("DATA ESCLUSIONE")
                        cell("MOTIVO ESCLUSIONE")
                        cell("DATA SCADENZA POLIZZA")
                        cell("MARCA")
                        cell("MODELLO")
                        cell("TIPO")
                        //cell("USO")
                        cell("TARGA")
                        cell("CV")
                        cell("KW")
                        cell("CILINDRATA")
                        cell("Q.LI")
                        cell("Q.LI RIMORCHIO")
                        cell("N. POSTI")
                        cell("ALIMENT.")
                        cell("VINCOLO/ LEASING")
                        cell("SCAD LEASING/ ENTE VINCOL")
                        cell("2011")
                        cell("2012")
                        cell("2013")
                        cell("2014")
                        cell("2015")
                        cell("2016")
                        cell("RAGIONE SOCIALE CONCESSIONARIO")
                        cell("SEDE LEGALE CONCESSIONARIO")
                        cell("SIGLA PROVINCIA SEDE LEGALE")
                        cell("PARTITA IVA CONCESSIONARIO")
                        //cell("CODICE CONCESSIONARIO")
                    }
                    applicazioni.each{ applicazione ->
                        //def cavalliF= CavalliFiscali.executeQuery('select potenzafiscale from CavalliFiscali as a where :parametro between  a.cilindrataMin and a.cilindrataMax',[parametro:applicazione.cilindrata])
                        //logg =new Log(parametri: "i cavalli fiscali per l'applicazione ${applicazione.numero} sono ${cavalliF[0]}", operazione: "generazione file ATTIVAZIONI compagnia", pagina: "JOB GENERAZIONE DOCUMENTI COMPAGNIA")
                        //if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        row(style: "content") {
                            cell(applicazione.dataDecorrenza?.format("dd/MM/yyyy"))
                            cell(applicazione.dataEsclusione?.format("dd/MM/yyyy"))
                            cell(applicazione.motivoEsclusione?:'')
                            cell(applicazione.dataScadenza?.format("dd/MM/yyyy"))
                            cell(applicazione.marca)
                            cell(applicazione.modello)
                            cell(((applicazione.tipologiaVeicolo==TipologiaVeicolo.AUTOCARRO || applicazione.tipologiaVeicolo==TipologiaVeicolo.AUTOCARRO_NOLEGGIO) && applicazione.destinazioneUso=="Noleggio") ? "AUTOCARRI NOLEGGIO LIBERO"   : ((applicazione.tipologiaVeicolo==TipologiaVeicolo.AUTOVETTURA  || applicazione.tipologiaVeicolo==TipologiaVeicolo.AUTOVETTURA_NOLEGGIO) && applicazione.destinazioneUso=="Noleggio") ? "AUTOVETTURE NOLEGGIO LIBERO"  : applicazione.tipologiaVeicolo.toString().toUpperCase())
                            //cell(applicazione.destinazioneUso.toString())
                            cell(applicazione.targa)
                            cell((applicazione.tipologiaVeicolo==TipologiaVeicolo.AUTOVETTURA || applicazione.tipologiaVeicolo==TipologiaVeicolo.AUTOVETTURA_NOLEGGIO) ? applicazione.cv : "")
                            cell((applicazione.tipologiaVeicolo==TipologiaVeicolo.AUTOVETTURA || applicazione.tipologiaVeicolo==TipologiaVeicolo.AUTOVETTURA_NOLEGGIO) ? applicazione.kw : "")
                            cell(applicazione.cilindrata)
                            cell((applicazione.tipologiaVeicolo==TipologiaVeicolo.AUTOCARRO  || applicazione.tipologiaVeicolo==TipologiaVeicolo.AUTOCARRO_NOLEGGIO) ? applicazione.quintali :"")
                            cell("")
                            cell((applicazione.tipologiaVeicolo==TipologiaVeicolo.MOTO) ? applicazione.nposti :"")
                            cell(applicazione.alimentazione.toString())
                            cell(applicazione.vincololeasing)
                            cell((applicazione.vincololeasing !=''&& applicazione.vincololeasing !=null) ? applicazione.scadenzaVincolo?.format("dd/MM/yyyy"):"")
                            cell("")
                            cell("")
                            cell("")
                            cell("")
                            cell("")
                            cell("")
                            cell(applicazione.dealer.ragioneSociale)
                            cell("${applicazione.dealer.indirizzoSedeLegale.indirizzo} ${applicazione.dealer.indirizzoSedeLegale.cap} ${applicazione.dealer.indirizzoSedeLegale.citta.toString().toUpperCase()} ${applicazione.dealer.indirizzoSedeLegale.provincia.toString().toUpperCase()}")
                            cell(applicazione.dealer.indirizzoSedeLegale.provincia.sigla)
                            cell(applicazione.dealer.partitaIva)
                            //cell(applicazione.dealer.codice)
                        }
                        applicazione.discard()
                    }
                }
            }
            def stream = new ByteArrayOutputStream()
            wb.write(stream)
            stream.close()
            fileContent = stream.toByteArray()
            fileName = "flusso_att_${new Date().format('yyyyMMddkkmm')}.xls"
        }else{
            fileName=null
            fileContent=null
        }
        return [(fileName): fileContent]
    }
    def uploadFlussoCompagniaPreventivo(){
        def logg
        def fileName, fileContent
        def now = new Date().clearTime()
        def datanuovat = new Date().parse('yyyy-MM-dd', '2018-01-01')
        def applicazioni=Applicazione.createCriteria().list(){
            //ge 'dataEmissione', now
            ge 'dataDecorrenza', datanuovat
            eq 'operazione', Operazione.INSERIMENTO
            eq 'stato', StatoApplicazione.PREVENTIVO
            eq 'rc', SI
        }
        if(applicazioni){
            def wb = ExcelBuilder.createXls {
                style("header") {
                    border style: "thin", color: "black"
                    align vertical: "center", horizontal: "center"
                }
                sheet("Tracciati") {
                    row(style: "header") {
                        cell("DATA INCLUSIONE")
                        cell("DATA ESCLUSIONE")
                        cell("MOTIVO ESCLUSIONE")
                        cell("DATA SCADENZA POLIZZA")
                        cell("MARCA")
                        cell("MODELLO")
                        cell("TIPO")
                        //cell("USO")
                        cell("TARGA")
                        cell("CV")
                        cell("KW")
                        cell("CILINDRATA")
                        cell("Q.LI")
                        cell("Q.LI RIMORCHIO")
                        cell("N. POSTI")
                        cell("ALIMENT.")
                        cell("VINCOLO/ LEASING")
                        cell("SCAD LEASING/ ENTE VINCOL")
                        cell("2011")
                        cell("2012")
                        cell("2013")
                        cell("2014")
                        cell("2015")
                        cell("2016")
                        cell("RAGIONE SOCIALE CONCESSIONARIO")
                        cell("SEDE LEGALE CONCESSIONARIO")
                        cell("SIGLA PROVINCIA SEDE LEGALE")
                        cell("PARTITA IVA CONCESSIONARIO")
                        //cell("CODICE CONCESSIONARIO")
                    }
                    applicazioni.each{ applicazione ->
                        //def cavalliF= CavalliFiscali.executeQuery('select potenzafiscale from CavalliFiscali as a where :parametro between  a.cilindrataMin and a.cilindrataMax',[parametro:applicazione.cilindrata])
                        //logg =new Log(parametri: "i cavalli fiscali per l'applicazione ${applicazione.numero} sono ${cavalliF[0]}", operazione: "generazione file ATTIVAZIONI compagnia", pagina: "JOB GENERAZIONE DOCUMENTI COMPAGNIA")
                        //if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        row(style: "content") {
                            cell("31/12/2017")
                            cell(applicazione.dataEsclusione?.format("dd/MM/yyyy"))
                            cell(applicazione.motivoEsclusione?:'')
                            cell(applicazione.dataScadenza?.format("dd/MM/yyyy"))
                            cell(applicazione.marca)
                            cell(applicazione.modello)
                            cell(((applicazione.tipologiaVeicolo==TipologiaVeicolo.AUTOCARRO || applicazione.tipologiaVeicolo==TipologiaVeicolo.AUTOCARRO_NOLEGGIO) && applicazione.destinazioneUso=="Noleggio") ? "AUTOCARRI NOLEGGIO LIBERO"   : ((applicazione.tipologiaVeicolo==TipologiaVeicolo.AUTOVETTURA  || applicazione.tipologiaVeicolo==TipologiaVeicolo.AUTOVETTURA_NOLEGGIO) && applicazione.destinazioneUso=="Noleggio") ? "AUTOVETTURE NOLEGGIO LIBERO"  : applicazione.tipologiaVeicolo.toString().toUpperCase())
                            //cell(applicazione.destinazioneUso.toString())
                            cell(applicazione.targa)
                            cell((applicazione.tipologiaVeicolo==TipologiaVeicolo.AUTOVETTURA || applicazione.tipologiaVeicolo==TipologiaVeicolo.AUTOVETTURA_NOLEGGIO) ? applicazione.cv : "")
                            cell((applicazione.tipologiaVeicolo==TipologiaVeicolo.AUTOVETTURA || applicazione.tipologiaVeicolo==TipologiaVeicolo.AUTOVETTURA_NOLEGGIO) ? applicazione.kw : "")
                            cell(applicazione.cilindrata)
                            cell((applicazione.tipologiaVeicolo==TipologiaVeicolo.AUTOCARRO  || applicazione.tipologiaVeicolo==TipologiaVeicolo.AUTOCARRO_NOLEGGIO) ? applicazione.quintali :"")
                            cell("")
                            cell((applicazione.tipologiaVeicolo==TipologiaVeicolo.MOTO) ? applicazione.nposti :"")
                            cell(applicazione.alimentazione.toString())
                            cell(applicazione.vincololeasing)
                            cell((applicazione.vincololeasing !=''&& applicazione.vincololeasing !=null) ? applicazione.scadenzaVincolo?.format("dd/MM/yyyy"):"")
                            cell("")
                            cell("")
                            cell("")
                            cell("")
                            cell("")
                            cell("")
                            cell(applicazione.dealer.ragioneSociale)
                            cell("${applicazione.dealer.indirizzoSedeLegale.indirizzo} ${applicazione.dealer.indirizzoSedeLegale.cap} ${applicazione.dealer.indirizzoSedeLegale.citta.toString().toUpperCase()} ${applicazione.dealer.indirizzoSedeLegale.provincia.toString().toUpperCase()}")
                            cell(applicazione.dealer.indirizzoSedeLegale.provincia.sigla)
                            cell(applicazione.dealer.partitaIva)
                            //cell(applicazione.dealer.codice)
                        }
                        applicazione.discard()
                    }
                }
            }
            def stream = new ByteArrayOutputStream()
            wb.write(stream)
            stream.close()
            fileContent = stream.toByteArray()
            fileName = "flusso_att_${new Date().format('yyyyMMddkkmm')}.xls"
        }else{
            fileName=null
            fileContent=null
        }
        return [(fileName): fileContent]
    }

}
