package libromatricola

import grails.util.Environment
import org.apache.poi.xssf.usermodel.*
import org.apache.poi.hssf.usermodel.*
import org.apache.poi.hssf.util.*
import org.apache.poi.ss.usermodel.*
import utenti.*
import polizze.*
import usertype.*
import org.springframework.web.multipart.commons.CommonsMultipartFile as RequestFile
import static Rischio.*

class SviluppoService {

    def grailsApplication

    def caricaProvincie() {
        def errors = []
        def province = [
            new Provincia(nome: "Agrigento", sigla: "AG", zona: 1),
            new Provincia(nome: "Alessandria", sigla: "AL", zona: 1),
            new Provincia(nome: "Ancona", sigla: "AN", zona: 1),
            new Provincia(nome: "L'Aquila", sigla: "AQ", zona: 1),
            new Provincia(nome: "Bergamo", sigla: "BG", zona: 1),
            new Provincia(nome: "Biella", sigla: "BI", zona: 1),
            new Provincia(nome: "Belluno", sigla: "BL", zona: 1),
            new Provincia(nome: "Bolzano", sigla: "BZ", zona: 1),
            new Provincia(nome: "Cuneo", sigla: "CN", zona: 1),
            new Provincia(nome: "Cremona", sigla: "CR", zona: 1),
            new Provincia(nome: "Forlì-Cesena", sigla: "FC", zona: 1),
            new Provincia(nome: "Ferrara", sigla: "FE", zona: 1),
            new Provincia(nome: "Fermo", sigla: "FM", zona: 1),
            new Provincia(nome: "Gorizia", sigla: "GO", zona: 1),
            new Provincia(nome: "Grosseto", sigla: "GR", zona: 1),
            new Provincia(nome: "Imola", sigla: "IM", zona: 1),
            new Provincia(nome: "Livorno", sigla: "LI", zona: 1),
            new Provincia(nome: "Lucca", sigla: "LU", zona: 1),
            new Provincia(nome: "Macerata", sigla: "MC", zona: 1),
            new Provincia(nome: "Mantova", sigla: "MN", zona: 1),
            new Provincia(nome: "Modena", sigla: "MO", zona: 1),
            new Provincia(nome: "Massa-Carrara", sigla: "MS", zona: 1),
            new Provincia(nome: "Novara", sigla: "NO", zona: 1),
            new Provincia(nome: "Oristano", sigla: "OR", zona: 1),
            new Provincia(nome: "Olbia-Tempio", sigla: "OT", zona: 1),
            new Provincia(nome: "Padova", sigla: "PD", zona: 1),
            new Provincia(nome: "Perugia", sigla: "PG", zona: 1),
            new Provincia(nome: "Pisa", sigla: "PI", zona: 1),
            new Provincia(nome: "Pordenone", sigla: "PN", zona: 1),
            new Provincia(nome: "Prato", sigla: "PO", zona: 1),
            new Provincia(nome: "Parma", sigla: "PR", zona: 1),
            new Provincia(nome: "Pistoia", sigla: "PT", zona: 1),
            new Provincia(nome: "Pesaro-Urbino", sigla: "PU", zona: 1),
            new Provincia(nome: "Pavia", sigla: "PV", zona: 1),
            new Provincia(nome: "Ravenna", sigla: "RA", zona: 1),
            new Provincia(nome: "Reggio Emilia", sigla: "RE", zona: 1),
            new Provincia(nome: "Rimini", sigla: "RN", zona: 1),
            new Provincia(nome: "Rovigo", sigla: "RO", zona: 1),
            new Provincia(nome: "Siena", sigla: "SI", zona: 1),
            new Provincia(nome: "La Spezia", sigla: "SP", zona: 1),
            new Provincia(nome: "Siracusa", sigla: "SR", zona: 1),
            new Provincia(nome: "Sassari", sigla: "SS", zona: 1),
            new Provincia(nome: "Savona", sigla: "SV", zona: 1),
            new Provincia(nome: "Teramo", sigla: "TE", zona: 1),
            new Provincia(nome: "Trento", sigla: "TN", zona: 1),
            new Provincia(nome: "Torino (provincia)", sigla: "TO (provincia)", zona: 1),
            new Provincia(nome: "Terni", sigla: "TR", zona: 1),
            new Provincia(nome: "Trieste", sigla: "TS", zona: 1),
            new Provincia(nome: "Treviso", sigla: "TV", zona: 1),
            new Provincia(nome: "Udine", sigla: "UD", zona: 1),
            new Provincia(nome: "Varese", sigla: "VA", zona: 1),
            new Provincia(nome: "Verbano-Cusio-Ossola", sigla: "VB", zona: 1),
            new Provincia(nome: "Vercelli", sigla: "VC", zona: 1),
            new Provincia(nome: "Venezia", sigla: "VE", zona: 1),
            new Provincia(nome: "Vicenza", sigla: "VI", zona: 1),
            new Provincia(nome: "Verona", sigla: "VR", zona: 1),
            new Provincia(nome: "Viterbo", sigla: "VT", zona: 1),
            new Provincia(nome: "Aosta", sigla: "AO", zona: 2),
            new Provincia(nome: "Arezzo", sigla: "AP", zona: 2),
            new Provincia(nome: "Ascoli Piceno", sigla: "AR", zona: 2),
            new Provincia(nome: "Asti", sigla: "AT", zona: 2),
            new Provincia(nome: "Benevento", sigla: "BN", zona: 2),
            new Provincia(nome: "Bologna", sigla: "BO", zona: 2),
            new Provincia(nome: "Brescia", sigla: "BS", zona: 2),
            new Provincia(nome: "Barletta-Andria-Trani", sigla: "BT", zona: 2),
            new Provincia(nome: "Campobasso", sigla: "CB", zona: 2),
            new Provincia(nome: "Chieti", sigla: "CH", zona: 2),
            new Provincia(nome: "Caltanissetta", sigla: "CL", zona: 2),
            new Provincia(nome: "Como", sigla: "CO", zona: 2),
            new Provincia(nome: "Enna", sigla: "EN", zona: 2),
            new Provincia(nome: "Firenze", sigla: "FI", zona: 2),
            new Provincia(nome: "Frosinone", sigla: "FR", zona: 2),
            new Provincia(nome: "Genova", sigla: "GE", zona: 2),
            new Provincia(nome: "Lecco", sigla: "LC", zona: 2),
            new Provincia(nome: "Lodi", sigla: "LO", zona: 2),
            new Provincia(nome: "Nuoro", sigla: "NU", zona: 2),
            new Provincia(nome: "Ogliastra", sigla: "OG", zona: 2),
            new Provincia(nome: "Piacenza", sigla: "PC", zona: 2),
            new Provincia(nome: "Pescara", sigla: "PE", zona: 2),
            new Provincia(nome: "Potenza", sigla: "PZ", zona: 2),
            new Provincia(nome: "Ragusa", sigla: "RG", zona: 2),
            new Provincia(nome: "Rieti", sigla: "RI", zona: 2),
            new Provincia(nome: "Repubblica di San Marino", sigla: "SM", zona: 2),
            new Provincia(nome: "Sondrio", sigla: "SO", zona: 2),
            new Provincia(nome: "Torino (città)", sigla: "TO (comune)", zona: 2),
            new Provincia(nome: "Trapani", sigla: "TP", zona: 2),
            new Provincia(nome: "Monza-Brianza", sigla: "MB", zona: 3),
            new Provincia(nome: "Milano (provincia)", sigla: "MI (provincia)", zona: 3),
            new Provincia(nome: "Cagliari", sigla: "CA", zona: 4),
            new Provincia(nome: "Carbonia-Iglesias", sigla: "CI", zona: 4),
            new Provincia(nome: "Cosenza", sigla: "CS", zona: 4),
            new Provincia(nome: "Catanzaro", sigla: "CZ", zona: 4),
            new Provincia(nome: "Isernia", sigla: "IS", zona: 4),
            new Provincia(nome: "Crotone", sigla: "KR", zona: 4),
            new Provincia(nome: "Latina", sigla: "LT", zona: 4),
            new Provincia(nome: "Messina", sigla: "ME", zona: 4),
            new Provincia(nome: "Milano (città)", sigla: "MI (comune)", zona: 4),
            new Provincia(nome: "Matera", sigla: "MT", zona: 4),
            new Provincia(nome: "Palermo", sigla: "PA", zona: 4),
            new Provincia(nome: "Reggio Calabria", sigla: "RC", zona: 4),
            new Provincia(nome: "Roma", sigla: "RM", zona: 4),
            new Provincia(nome: "Medio Campidano", sigla: "VS", zona: 4),
            new Provincia(nome: "Vibo Valentia", sigla: "VV", zona: 4),
            new Provincia(nome: "Catania", sigla: "CT", zona: 5),
            new Provincia(nome: "Lecce", sigla: "LE", zona: 5),
            new Provincia(nome: "Avellino", sigla: "AV", zona: 6),
            new Provincia(nome: "Bari", sigla: "BA", zona: 6),
            new Provincia(nome: "Brindisi", sigla: "BR", zona: 6),
            new Provincia(nome: "Foggia", sigla: "FG", zona: 6),
            new Provincia(nome: "Salerno", sigla: "SA", zona: 6),
            new Provincia(nome: "Taranto", sigla: "TA", zona: 6),
            new Provincia(nome: "Caserta", sigla: "CE", zona: 7),
            new Provincia(nome: "Napoli", sigla: "NA", zona: 7)/*,
            new Provincia(nome: "Forze Alleate in Italia", sigla: "AFI", zona: 7),
            new Provincia(nome: "Corpo Diplomatico", sigla: "CD", zona: 7),
            new Provincia(nome: "Escursionisti Esteri", sigla: "EE", zona: 7),
            new Provincia(nome: "Forze Terrestri Alleate del Sud Europa", sigla: "FTASE", zona: 7),
            new Provincia(nome: "Sovrano Militare Ordine di Malta", sigla: "SMOM", zona: 7)*/
        ].each { provincia -> if(!provincia.save()) errors << provincia.errors }
        return errors
    }

    def caricaVeicoli() {
        def errors = []
        def veicoli = [
            new Veicolo(marca: 'Seat', modello: 'Alhambra', rischio: BASSO, codice: "64"),
            new Veicolo(marca: 'Volkswagen', modello: 'UP!', rischio: BASSO, codice: "76"),
            new Veicolo(marca: 'Seat', modello: 'Mii', rischio: BASSO, codice: "64"),
            new Veicolo(marca: 'Škoda', modello: 'Citigo', rischio: BASSO, codice: "66"),
            new Veicolo(marca: 'Seat', modello: 'Cordoba', rischio: BASSO, codice: "64"),
            new Veicolo(marca: 'Seat', modello: 'Toledo', rischio: BASSO, codice: "64"),
            new Veicolo(marca: 'Seat', modello: 'Exeo', rischio: BASSO, codice: "64"),
            new Veicolo(marca: 'Volkswagen', modello: 'Eos', rischio: BASSO, codice: "76"),
            new Veicolo(marca: 'Volkswagen', modello: 'Jetta', rischio: BASSO, codice: "76"),
            new Veicolo(marca: 'Volkswagen', modello: 'Phaeton', rischio: BASSO, codice: "76"),
            new Veicolo(marca: 'Volkswagen', modello: 'Sharan', rischio: BASSO, codice: "76"),
            new Veicolo(marca: 'Volkswagen', modello: 'Scirocco', rischio: BASSO, codice: "76"),
            new Veicolo(marca: 'Audi', modello: 'A1', rischio: MEDIO, codice: "5"),
            new Veicolo(marca: 'Audi', modello: 'A3', rischio: MEDIO, codice: "5"),
            new Veicolo(marca: 'Audi', modello: 'A4', rischio: MEDIO, codice: "5"),
            new Veicolo(marca: 'Audi', modello: 'A4 Allroad', rischio: MEDIO, codice: "5"),
            new Veicolo(marca: 'Seat', modello: 'Altea', rischio: MEDIO, codice: "64"),
            new Veicolo(marca: 'Seat', modello: 'Ibiza', rischio: MEDIO, codice: "64"),
            new Veicolo(marca: 'Škoda', modello: 'Fabia', rischio: MEDIO, codice: "66"),
            new Veicolo(marca: 'Škoda', modello: 'Octavia', rischio: MEDIO, codice: "66"),
            new Veicolo(marca: 'Škoda', modello: 'Rapid', rischio: MEDIO, codice: "66"),
            new Veicolo(marca: 'Škoda', modello: 'Roomster', rischio: MEDIO, codice: "66"),
            new Veicolo(marca: 'Škoda', modello: 'Superb', rischio: MEDIO, codice: "66"),
            new Veicolo(marca: 'Škoda', modello: 'Yeti', rischio: MEDIO, codice: "66"),
            new Veicolo(marca: 'Volkswagen', modello: 'Maggiolino', rischio: MEDIO, codice: "76"),
            new Veicolo(marca: 'Volkswagen', modello: 'Passat', rischio: MEDIO, codice: "76"),
            new Veicolo(marca: 'Volkswagen', modello: 'Polo', rischio: MEDIO, codice: "76"),
            new Veicolo(marca: 'Volkswagen', modello: 'Tiguan', rischio: MEDIO, codice: "76"),
            new Veicolo(marca: 'Volkswagen', modello: 'Touran', rischio: MEDIO, codice: "76"),
            new Veicolo(marca: 'Camper e roulottes', rischio: MEDIO, codice: "ALTRO"),
            new Veicolo(marca: 'Alfa Romeo', rischio: ALTO, codice: "3"),
            new Veicolo(marca: 'Chevrolet', rischio: ALTO, codice: "13"),
            new Veicolo(marca: 'Citroen', rischio: ALTO, codice: "15"),
            new Veicolo(marca: 'Daihatsu', rischio: ALTO, codice: "17"),
            new Veicolo(marca: 'Fiat', rischio: ALTO, codice: "22"),
            new Veicolo(marca: 'Ford', rischio: ALTO, codice: "23"),
            new Veicolo(marca: 'Honda', rischio: ALTO, codice: "28"),
            new Veicolo(marca: 'Hyunday', rischio: ALTO, codice: "30"),
            new Veicolo(marca: 'Isuzu', rischio: ALTO, codice: "21"),
            new Veicolo(marca: 'Kia', rischio: ALTO, codice: "35"),
            new Veicolo(marca: 'Lancia', rischio: ALTO, codice: "38"),
            new Veicolo(marca: 'Mini', rischio: ALTO, codice: "48"),
            new Veicolo(marca: 'Mitsubishi', rischio: ALTO, codice: "49"),
            new Veicolo(marca: 'Nissan', rischio: ALTO, codice: "52"),
            new Veicolo(marca: 'Opel', rischio: ALTO, codice: "53"),
            new Veicolo(marca: 'Peugeot', rischio: ALTO, codice: "54"),
            new Veicolo(marca: 'Renault', rischio: ALTO, codice: "58"),
            new Veicolo(marca: 'Rover', rischio: ALTO, codice: "59"),
            new Veicolo(marca: 'Ssangyong', rischio: ALTO, codice: "68"),
            new Veicolo(marca: 'Subaru', rischio: ALTO, codice: "69"),
            new Veicolo(marca: 'Tata', rischio: ALTO, codice: "71"),
            new Veicolo(marca: 'Toyota', rischio: ALTO, codice: "73"),
            new Veicolo(marca: 'Audi', modello: 'A5', rischio: ALTO, codice: "5"),
            new Veicolo(marca: 'Audi', modello: 'A6', rischio: ALTO, codice: "5"),
            new Veicolo(marca: 'Audi', modello: 'A6 Allroad', rischio: ALTO, codice: "5"),
            new Veicolo(marca: 'Audi', modello: 'A7', rischio: ALTO, codice: "5"),
            new Veicolo(marca: 'Audi', modello: 'A8', rischio: ALTO, codice: "5"),
            new Veicolo(marca: 'Audi', modello: 'Q3', rischio: ALTO, codice: "5"),
            new Veicolo(marca: 'Audi', modello: 'Q5', rischio: ALTO, codice: "5"),
            new Veicolo(marca: 'Audi', modello: 'Q7', rischio: ALTO, codice: "5"),
            new Veicolo(marca: 'Audi', modello: 'TT', rischio: ALTO, codice: "5"),
            new Veicolo(marca: 'Audi', modello: 'R8', rischio: ALTO, codice: "5"),
            new Veicolo(marca: 'Seat', modello: 'Leon', rischio: ALTO, codice: "64"),
            new Veicolo(marca: 'Volkswagen', modello: 'Golf', rischio: ALTO, codice: "76"),
            new Veicolo(marca: 'Volkswagen', modello: 'Touareg', rischio: ALTO, codice: "76"),
            new Veicolo(marca: 'Altro', rischio: ALTO, codice: "ALTRO")
        ].each { veicolo -> if(!veicolo.save()) errors << veicolo.errors }
        return errors
    }

    def caricaTariffe() {
        def errors = []
        try {
            def file = grailsApplication.mainContext.getResource('xls/Tariffe Libro Matricola 2013-12.xls')?.file
            if(file == null) throw RuntimeException('Il file "Tariffe Libro Matricola 2013-12.xls" non è stato trovato')
            def stream = new FileInputStream(file)
            def workbook = new HSSFWorkbook(stream)
            def sheet = workbook.getSheetAt(0)
            def evaluator = workbook.creationHelper.createFormulaEvaluator()
            def firstRow = new CellReference('A8').row
            def lastRow = new CellReference('A23').row

            def codiceIndex = new CellReference('A8').col
            def rcVATPIndex = new CellReference('C8').col
            def rcVANIndex = new CellReference('D8').col
            def rcMIndex = new CellReference('E8').col
            def mPIndex = new CellReference('AJ8').col
            def mKIndex = new CellReference('AK8').col
            def mMIndex = new CellReference('AL8').col
            def mMKIndex = new CellReference('AM8').col
            def pTGMIndex = new CellReference('AN8').col
            def pTGIndex = new CellReference('AO8').col
            def pI100Index = new CellReference('AP8').col
            def pI200Index = new CellReference('AQ8').col

            def firstMolt = new CellReference('H8').col
            try {
                (firstRow..lastRow).each { rowIndex ->
                    def row = sheet.getRow(rowIndex)
                    def data = [
                            codice: cellValue(row.getCell(codiceIndex as int)).trim(),
                            imponibileRCVettureAutocarriTargheProva: cellValue(row.getCell(rcVATPIndex as int), evaluator) as BigDecimal,
                            imponibileRCVettureAutocarriNoleggio: cellValue(row.getCell(rcVANIndex as int), evaluator) as BigDecimal,
                            imponibileRCMoto: cellValue(row.getCell(rcMIndex as int), evaluator) as BigDecimal,
                            moltiplicatoreCVTTargheProva: cellValue(row.getCell(mPIndex as int), evaluator) as BigDecimal,
                            moltiplicatoreKasko: cellValue(row.getCell(mKIndex as int), evaluator) as BigDecimal,
                            moltiplicatoreCVTMoto: cellValue(row.getCell(mMIndex as int), evaluator) as BigDecimal,
                            moltiplicatoreCVTKaskoMoto: cellValue(row.getCell(mMKIndex as int), evaluator) as BigDecimal,
                            imponibileTutelaGiudiziaria: cellValue(row.getCell(pTGIndex as int), evaluator) as BigDecimal,
                            imponibileTutelaGiudiziariaMoto: cellValue(row.getCell(pTGMIndex as int), evaluator) as BigDecimal,
                            imponibileInfortuni100: cellValue(row.getCell(pI100Index as int), evaluator) as BigDecimal,
                            imponibileInfortuni200: cellValue(row.getCell(pI200Index as int), evaluator) as BigDecimal,
                    ]
                    def tariffa = new Tariffa(data)
                    7.times { zona ->
                        def moltiplicatore = [
                                zona: zona + 1,
                                basso: cellValue(row.getCell(firstMolt + (zona * 4)), evaluator) as BigDecimal,
                                medio: cellValue(row.getCell(firstMolt + 1 + (zona * 4)), evaluator) as BigDecimal,
                                alto: cellValue(row.getCell(firstMolt + 2 + (zona * 4)), evaluator) as BigDecimal,
                                altro: cellValue(row.getCell(firstMolt + 3 + (zona * 4)), evaluator) as BigDecimal,
                        ]
                        tariffa.addToMoltiplicatoriCVT(moltiplicatore)
                    }
                    if(!tariffa.save()) throw new RuntimeException("Errore caricamento tariffa: $tariffa.errors")
                }
            } catch(e) { e.printStackTrace() }
            stream.close()
        } catch(e) { errors += e.message }
        return errors
    }

    def aggiornaTariffa(byte[] bytes, VersioneTariffa versione, boolean create = false) {
        def errors = []
        try {
            if(!bytes) throw new Exception("Specificare il file delle tariffe")
            def stream = new ByteArrayInputStream(bytes)
            def workbook = new HSSFWorkbook(stream)
            def sheet = workbook.getSheetAt(0)
            def evaluator = workbook.creationHelper.createFormulaEvaluator()
            def firstRow = new CellReference('A8').row
            def lastRow = new CellReference('A23').row

            def codiceIndex = new CellReference('A8').col
            def rcVATPIndex = new CellReference('C8').col
            def rcVANIndex = new CellReference('D8').col
            def rcMIndex = new CellReference('E8').col
            def mPIndex = new CellReference('AJ8').col
            def mKIndex = new CellReference('AK8').col
            def mMIndex = new CellReference('AL8').col
            def mMKIndex = new CellReference('AM8').col
            def pTGMIndex = new CellReference('AN8').col
            def pTGIndex = new CellReference('AO8').col
            def pI100Index = new CellReference('AP8').col
            def pI200Index = new CellReference('AQ8').col

            def firstMolt = new CellReference('H8').col
            try {
                (firstRow..lastRow).each { rowIndex ->
                    def row = sheet.getRow(rowIndex)
                    def data = [
                        codice: cellValue(row.getCell(codiceIndex as int)).trim(),
                        imponibileRCVettureAutocarriTargheProva: cellValue(row.getCell(rcVATPIndex as int), evaluator) as BigDecimal,
                        imponibileRCVettureAutocarriNoleggio: cellValue(row.getCell(rcVANIndex as int), evaluator) as BigDecimal,
                        imponibileRCMoto: cellValue(row.getCell(rcMIndex as int), evaluator) as BigDecimal,
                        moltiplicatoreCVTTargheProva: cellValue(row.getCell(mPIndex as int), evaluator) as BigDecimal,
                        moltiplicatoreKasko: cellValue(row.getCell(mKIndex as int), evaluator) as BigDecimal,
                        moltiplicatoreCVTMoto: cellValue(row.getCell(mMIndex as int), evaluator) as BigDecimal,
                        moltiplicatoreCVTKaskoMoto: cellValue(row.getCell(mMKIndex as int), evaluator) as BigDecimal,
                        imponibileTutelaGiudiziaria: cellValue(row.getCell(pTGIndex as int), evaluator) as BigDecimal,
                        imponibileTutelaGiudiziariaMoto: cellValue(row.getCell(pTGMIndex as int), evaluator) as BigDecimal,
                        imponibileInfortuni100: cellValue(row.getCell(pI100Index as int), evaluator) as BigDecimal,
                        imponibileInfortuni200: cellValue(row.getCell(pI200Index as int), evaluator) as BigDecimal,
                    ]
                    def tariffa = create ? new Tariffa(codice: data.codice, versione: versione) : Tariffa.findByCodiceAndVersione(data.codice, versione)
                    if(tariffa) {
                        tariffa.properties = data
                        7.times { zona ->
                            def moltiplicatore = [
                                zona: zona + 1,
                                basso: cellValue(row.getCell(firstMolt + (zona * 4)), evaluator) as BigDecimal,
                                medio: cellValue(row.getCell(firstMolt + 1 + (zona * 4)), evaluator) as BigDecimal,
                                alto: cellValue(row.getCell(firstMolt + 2 + (zona * 4)), evaluator) as BigDecimal,
                                altro: cellValue(row.getCell(firstMolt + 3 + (zona * 4)), evaluator) as BigDecimal,
                            ]
                            if(create) tariffa.addToMoltiplicatoriCVT(moltiplicatore)
                            else tariffa.moltiplicatoriCVT.find { it.zona == zona + 1 }.properties = moltiplicatore
                        }
                        if(!tariffa.save()) throw new RuntimeException("Errore caricamento tariffa: $tariffa.errors")
                        else println "Tariffa $tariffa.codice aggiornata con successo!"
                    } else errors += "Non è stata trovata nessuna tariffa con codice $data.codice"
                }
            } catch(e) {
                e.printStackTrace()
                errors += e.message
            }
            stream.close()
        } catch(e) { errors += e.message }
        return errors
    }

    def caricaDealer() {
        def errors = []
        try {
            def file = grailsApplication.mainContext.getResource('xls/Dealer Libro Matricola 2013-10.xlsx')?.file
            if(file == null) throw RuntimeException('Il file "Dealer Libro Matricola 2013-10.xlsx" non è stato trovato')
            def stream = new FileInputStream(file)
            def workbook = new XSSFWorkbook(stream)
            def sheet = workbook.getSheetAt(0)
            def firstRow = new CellReference('A3').row
            def lastRow = Environment.executeForCurrentEnvironment {
                development { firstRow + 50 }
                test { sheet.lastRowNum }
                production { sheet.lastRowNum }
            }
            def indexes = [
                    ragioneSociale: new CellReference('C3').col,
                    provincia: new CellReference('Q3').col,
                    indirizzo: new CellReference('R3').col,
                    citta: new CellReference('S3').col,
                    cap: new CellReference('T3').col,
                    telefono: new CellReference('U3').col,
                    partitaIva: new CellReference('AH3').col,
                    emailVendita: new CellReference('AR3').col,
                    emailOfficina: new CellReference('AV3').col,
                    codiceTariffa: new CellReference('F3').col,
                    sovrappremio: new CellReference('G3').col
            ]
            def tariffe = Tariffa.list().collectEntries { tariffa -> [tariffa.codice, tariffa] }
            (firstRow..lastRow).eachWithIndex { rowIndex, index ->
                def row = sheet.getRow(rowIndex)
                def dealer = new Dealer()
                dealer.codice = "${index + 1}".leftPad(5, '0')
                dealer.numeroLibroMatricola = "${index + 1}".leftPad(4, '0')
                dealer.ragioneSociale = cellValue(row.getCell(indexes.ragioneSociale))?.trim()
                dealer.telefono = cellValue(row.getCell(indexes.telefono))?.trim()
                dealer.partitaIva = cellValue(row.getCell(indexes.partitaIva))?.trim() ?: '0'.leftPad(11, '0')
                dealer.email = cellValue(row.getCell(indexes.emailVendita))?.trim() ?: cellValue(row.getCell(indexes.emailOfficina))?.trim() ?: null
                dealer.username = String.random(10)
                dealer.password = grailsApplication.config.libromatricola.passwordDealer
                dealer.indirizzoSedeLegale = new Indirizzo(
                        indirizzo: cellValue(row.getCell(indexes.indirizzo))?.trim(),
                        citta: cellValue(row.getCell(indexes.citta))?.trim(),
                        cap: cellValue(row.getCell(indexes.cap))?.trim(),
                        provincia: Provincia.findBySiglaLike("${cellValue(row.getCell(indexes.provincia))}%")
                )
                def codiceTariffa = cellValue(row.getCell(indexes.codiceTariffa))?.trim() ?: ''
                def sovrappremio = ((cellValue(row.getCell(indexes.sovrappremio)) ?: 0.0) * 100).round()
                //dealer.tariffa = Tariffa.findByCodice("$codiceTariffa$sovrappremio") ?: Tariffa.findByCodice('A10')
                dealer.tariffa = tariffe["$codiceTariffa$sovrappremio"] ?: tariffe["A10"]
                if(!dealer.save()) throw new RuntimeException("Errore caricamento dealer: $dealer.errors")
            }
            stream.close()
        } catch(e) { errors += e.message }
        return errors
    }

    def caricaNuovaVersioneTariffa(RequestFile file, String versione) {
        def versioneTariffa = new VersioneTariffa(file: file.bytes, fileName: file.originalFilename, versione: versione, dataCaricamento: new Date())
        if(versioneTariffa.save(flush: true)) return aggiornaTariffa(file.bytes, versioneTariffa, true)
        else return versioneTariffa.errors
    }

    def modificaVersioneTariffa(VersioneTariffa versioneTariffa, RequestFile file, String versione) {
        versioneTariffa.file = file.bytes
        versioneTariffa.versione = versione
        if(file.bytes) versioneTariffa.dataCaricamento = new Date()
        if(versioneTariffa.save(flush: true)) return file.bytes ? aggiornaTariffa(file.bytes, versioneTariffa) : false
        else return versioneTariffa.errors
    }

    private cellValue(cell) {
        switch(cell.cellType) {
            case Cell.CELL_TYPE_BOOLEAN: return cell.booleanCellValue
            case Cell.CELL_TYPE_ERROR: return cell.errorCellValue
            case Cell.CELL_TYPE_NUMERIC: return cell.numericCellValue
            case Cell.CELL_TYPE_STRING: return cell.stringCellValue
            default: return null
        }
    }

    private cellValue(cell, evaluator) {
        def evaluated = evaluator.evaluate(cell)
        switch(evaluated.cellType) {
            case Cell.CELL_TYPE_BOOLEAN: return evaluated.booleanValue
            case Cell.CELL_TYPE_NUMERIC: return evaluated.numberValue
            case Cell.CELL_TYPE_STRING: return evaluated.stringValue
            default: return null
        }
    }

}
