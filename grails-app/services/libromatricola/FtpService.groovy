package libromatricola

import com.jcraft.jsch.ChannelSftp
import com.jcraft.jsch.JSch
import com.jcraft.jsch.SftpATTRS
import com.jcraft.jsch.SftpException
import grails.util.Environment
import org.apache.commons.net.ftp.*
import utenti.Log
import static org.apache.camel.Exchange.FILE_NAME

class FtpService {

    static transactional = false

    def grailsApplication
    def mailService

    def uploadFile(local, remote, dir = null) {
        def logg

        def config = grailsApplication.config.sftp
        log.info "ftpService uploadFile to $remote"
        logg =new Log(parametri: "ftpService uploadFile $remote", operazione: "upload file", pagina: "FTP Service")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        //ftpUploadFile(config, local, remote, dir)
        ftpsuploadFile(local, remote, dir,null,false)
    }

    def makeDir(dir) {
        def config = grailsApplication.config.ftp
        log.info "ftpService makeDir '$dir'"
        //ftpMakeDir(config, dir)
    }

    /*private ftpUploadFile(config, local, remote, dir = null) {
        new FTPClient().with {
            try {
                connect(config.host)
                login(config.username, config.password)
                if(FTPReply.isPositiveCompletion(replyCode)) {
                    changeWorkingDirectory(config.dir)
                    if(dir) changeWorkingDirectory(dir)
                    enterLocalPassiveMode()
                    def input = local instanceof File || local instanceof String ? new FileInputStream(local) : local instanceof byte[] ? new ByteArrayInputStream(local) : local instanceof InputStream ? local : null
                    if(input) {
                        if(remote.endsWith(".pdf")) setFileType(FTP.BINARY_FILE_TYPE)
                        storeFile(remote, input)
                    }
                } else {
                    disconnect()
                    sendErrorMail(replyString)
                }
            } catch(e) {
                sendErrorMail(e.message)
            } finally {
                if(isConnected()) try {
                    logout()
                    disconnect()
                } catch(e) { sendErrorMail(e.message) }
            }
        }
    }*/

    /**
     * 24 giugno 2014:
     * Migrazione a nuovo SFTP
     */
    private ftpUploadFile(config, local, remote, dir = null) {
        def url = config.url
        if(dir) url += "/$dir"
        def password = config.password
        def options = config.options.join("&")
        def uri = "$url?password=$password&$options"
        def input = null
        switch(local) {
            case File:
                input = local.text
                break
            case String:
                input = local
                break
            case byte[]:
                input = new String(local)
                break
            default: input = null
        }
        if(input) try {
            sendMessageAndHeaders(uri, input, [(FILE_NAME): remote])
        } catch(e) {
            sendErrorMail(e.message)
        } else println "No input to send"
    }

    private ftpMakeDir(config, dir) {
        new FTPClient().with {
            try {
                connect(config.host)
                login(config.username, config.password)
                if(FTPReply.isPositiveCompletion(replyCode)) {
                    changeWorkingDirectory(config.dir)
                    makeDirectory(dir)
                } else {
                    disconnect()
                    sendErrorMail(replyString)
                }
            } catch(e) {
                sendErrorMail(e.message)
            } finally {
                if(isConnected()) try {
                    logout()
                    disconnect()
                } catch(e) { sendErrorMail(e.message) }
            }
        }
    }

    private sendErrorMail(error) {
        log.error error
        def mail = grailsApplication.config.ftp.failureMail
        mailService.sendMail {
            to mail
            subject 'Errore FTP Libro matricola'
            text "Si è verificato un errore di connessione al server ftp: $error"
        }
    }
    def ftpsuploadFile(file, String fileName, dir=null, utente=null, boolean annulla=false) {
        def logg
        def input
        try {
            if(file instanceof byte[]) input = new ByteArrayInputStream(file)
            else if(file instanceof InputStream) input = file
            else if(file instanceof String) input = new ByteArrayInputStream(file.bytes)
            def sftpConfig = grailsApplication.config.sftpannulla
            def client = new JSch()
            def session = client.getSession(sftpConfig.username, sftpConfig.host, sftpConfig.port)
            session.setConfig("StrictHostKeyChecking", "no")
            session.password = sftpConfig.password
            session.connect()
            logg =new Log(parametri: "JSch Session connected to ${session.userName}@${session.host}:${session.port}", operazione: "collegamento server ftp", pagina: "Annullamenti",utente: utente )
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            def channel = session.openChannel("sftp") as ChannelSftp
            channel.connect()
            logg =new Log(parametri: "JSch Channel connected", operazione: "collegamento server ftp", pagina: "Annullamenti",utente: utente)
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            try {
                channel.cd(sftpConfig.cartella)
                logg =new Log(parametri: "Current sftp directory: ${channel.pwd()}", operazione: "modifica cartella", pagina: "Annullamenti",utente: utente)
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                def curDir=channel.pwd()
                if(dir!=null){
                    def pathDir = "${curDir}/${dir}"
                    SftpATTRS existDir=null
                    try {
                        existDir = channel.stat(pathDir)
                    } catch (SftpException e) {
                        if (e.getMessage() != null&& e.getMessage().indexOf("No such file") == -1) {
                            logg =new Log(parametri: "Errore in ottenere stat od directory : ${pathDir} -- ${ e.getMessage()}", operazione: "creazione cartella contrassegno", pagina: "Annullamenti",utente: utente)
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            println "Error getting stat of  directory:"+ pathDir + "/" + e.getMessage()
                            throw e
                        }
                    }
                    if (existDir == null) {
                        // try to create dir
                        logg =new Log(parametri: "Trying to create ${curDir}/\\ ${dir}", operazione: "creazione cartella contrassegno", pagina: "Annullamenti",utente: utente)
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"

                        channel.mkdir("${curDir}/${dir}")

                    } else {
                        if (!existDir.isDir()) {
                            throw new IOException(curDir + "/" + dir+ " is not a directory:" + existDir)
                            logg =new Log(parametri: "${curDir}/\\ ${dir} is not a directory", operazione: "creazione cartella contrassegno", pagina: "Annullamenti",utente: utente)
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        }
                    }
                    if(Environment.current != Environment.PRODUCTION) {
                        channel.cd("/home/flussi-nais/Libro Matricola Volkswagen/test/${dir}")
                    }else{
                        channel.cd("/home/flussi-nais/Libro Matricola Volkswagen/${dir}")
                    }
                    logg =new Log(parametri: "Current sftp directory: ${channel.pwd()}", operazione: "modifica cartella", pagina: "Annullamenti",utente: utente)
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"

                    logg =new Log(parametri: "try uploading ${fileName}...", operazione: "caricamento file", pagina: "Annullamenti",utente: utente)
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"

                    channel.put(input, fileName)

                    logg =new Log(parametri: "${fileName} uploaded!", operazione: "caricamento file", pagina: "Annullamenti",utente: utente)
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }else {
                    if(annulla){/**carico gli annullamenti generati*/
                        if(Environment.current != Environment.PRODUCTION) {
                            channel.cd("/home/flussi-nais/Libro Matricola Volkswagen/Annullamenti/test")
                        }else{
                            channel.cd("/home/flussi-nais/Libro Matricola Volkswagen/Annullamenti")
                        }
                        logg =new Log(parametri: "Current sftp directory: ${channel.pwd()}", operazione: "caricamento annullamenti", pagina: "Annullamenti",utente: utente)
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"

                        /***verificare se esiste un file già con lo steso nome**/
                        def list = channel.ls("*.txt")
                        def i=0
                        for(def entry : list) {
                            fileName=fileName.substring(0, fileName.indexOf("_rcigio.txt")-1)+"${i}_rcigio.txt"
                            while(fileName.equals(entry.filename)){
                                i++
                                fileName=fileName.substring(0, fileName.indexOf("_rcigio.txt")-1)+"${i}_rcigio.txt"
                                logg =new Log(parametri: "esiste già un file con lo stesso nome: ${entry.filename}, rinomino il file-->${fileName}", operazione: "caricamento annullamenti", pagina: "Annullamenti",utente: utente)
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            }
                        }
                        logg =new Log(parametri: "alla fine il file e' stato nominato come-->${fileName}", operazione: "caricamento annullamenti", pagina: "Annullamenti",utente: utente)
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }else{
                        if(Environment.current != Environment.PRODUCTION) {
                            channel.cd("/home/flussi-nais/Libro Matricola Volkswagen/test")
                        }else{
                            channel.cd("/home/flussi-nais/Libro Matricola Volkswagen")
                        }
                    }

                    logg =new Log(parametri: "try uploading ${fileName}...", operazione: "caricamento annullamenti", pagina: "Annullamenti",utente: utente)
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"

                    channel.put(input, fileName)

                    logg =new Log(parametri: "${fileName} uploaded!", operazione: "caricamento file", pagina: "Annullamenti",utente: utente)
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }
            }
            catch (e) {
                logg =new Log(parametri: "errore try catch ->${e.toString()}", operazione: "modifica cartella", pagina: "Annullamenti",utente: utente)
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            }
            channel.disconnect()
            session.disconnect()
            return true
        } catch(e) {
            logg =new Log(parametri: "Error uploading file ${fileName} to sftp: ${e.toString()}", operazione: "FTP UPLOAD", pagina: "Annullamenti",utente: utente)
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            return false
        }
    }

}
