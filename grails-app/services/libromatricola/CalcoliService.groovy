package libromatricola

import org.apache.poi.ss.formula.eval.ValueEval
import org.apache.poi.ss.formula.functions.Days360
import polizze.*
import groovy.time.*
import utenti.Log

import static TipologiaVeicolo.*

class CalcoliService {
    
    static transactional = false
	
    def calcola(tipoVeicolo, targaProva, prodotti, tariffa, provincia, veicolo, valore, decorrenza, scadenza,cv, destUso, tariffeACRC) {
        def logg
        def risultati = [:]
        def durataPercentuale = calcolaPercentuale(decorrenza, scadenza)
        def difgg=diffDateInDays(decorrenza, scadenza)

        println "gg difgg $difgg"
        prodotti.each { prodotto ->
            if(prodotto == 'rc') {
                def zRC="Z1"
                def tassazione=1.2650

                def z1=["AO","VC","NO","BI","VB","AT","PN","AL","CN","COR","BL","BG","BS","UD","CR","SO","MN","PV","BZ","LO","LC","TN","PC","RO","VA","MB","COR", "SI","CB","FE",
                        "VR","VT","IM","SV","GO","PR","VI","TR","MI","MT","MO","GR","TO (provincia)"]
                def z2=["VS","NU","IS","RE","PU","TV","CI","PD","AR","PZ","OG","VE","TO (comune)","OT","FC","AQ","PG","TS","RA","CS","RG","EN","MC","TP","FM","RI","AP","GE","SR","CA","CH",
                        "SS","FR","TE","LI","AN","BO","RN","LE","CL","PA","AG","CZ"]
                def z3=["ME","BN","CT","PI","FI","SP","KR","BA","LT","BT","RM","PE","FG","LU","AV","BR","SA","VV","PT","PO","CE","MS","RC","TA","NA"]

                def t1950=["AO","BZ","TN"]
                def t2300=["AV","SCV","GO","OR","PN","SO","TS","UD"]
                def t2550=["TV"]
                def t2600=["AQ"]
                def t2650=["AG","AL","AN","AR","AP","AT","BA","BT","BL","BN","BG","BI","BO","BS","BR","CA","CL","CB","CI","CE","CT","CZ","CH","CO","CS","CR","KR","CN","EN","FE","FI","FM","FG","FC","FR","GE","GR","IM","IS","SP","LT","LE","LC","LI","LO","LU","MC","MN","MS","MT","VS","ME","MI","MO","MB","NA","NO","NU","OG","OT","PD","PA","PR","PV","PG","PU","PE","PC","PI","PT","PZ","PO","RG","RA","RC","RE","RI","RN","RM","RO","SA","SS","SV","SI","SR","TA","TE","TR","TO","TP","VA","VE","VB","VC","VR","VV","VI","VT"]
                if( provincia.sigla in z1) {
                    zRC="Z1"
                }else if (provincia.sigla in z2){
                    zRC="Z2"
                }else if(provincia.sigla in z3){
                    zRC="Z3"
                }
                if(provincia.sigla in t1950){tassazione=1.1950}
                else if(provincia.sigla in t2300){tassazione=1.2300}
                else if(provincia.sigla in t2550){tassazione=1.2550}
                else if(provincia.sigla in t2600){tassazione=1.2600}
                else if(provincia.sigla in t2650){tassazione=1.2650}
                else {tassazione=1.2650}
                println "zRC --> ${zRC}"
                println "tassazione calcoli?--> ${tassazione}"
                //RC
                def imponibile,pcl,margineMach1
                /**agosto 2017**/
                def datanuovat = new Date().parse('yyyy/MM/dd', '2017/10/01')
                if(decorrenza>=datanuovat){
                    def imponibile2017= imponibileRC2017(zRC,tipoVeicolo, targaProva, cv, destUso,tariffeACRC)
                    println "imponibile rc 2017 ${imponibile2017}"
                    logg =new Log(parametri: "imponibile rc 2017 ${imponibile2017}", operazione: "calcola rc 2017 ", pagina: "CREAZIONE APPLICAZIONE")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    logg =new Log(parametri: "durataPercentuale ${durataPercentuale}", operazione: "calcola rc 2017", pagina: "CREAZIONE APPLICAZIONE")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"

                    imponibile = (imponibileRC2017(zRC, tipoVeicolo, targaProva, cv, destUso,tariffeACRC) / 360 * difgg).round(2)
                    pcl = (imponibile * tassazione).round(2)
                    margineMach1 = 0.1
                    if(new Date()[Calendar.YEAR] > 2015) margineMach1 = 0.05 //portata al 5% come da mail del 18-12-2015 di Susanna Facciotto (oggetto: LIBRO MATRICOLA 2016)
                    risultati.'rc' = [
                            pcl: pcl,
                            imponibile: imponibile,
                            imposte: (pcl - imponibile).round(2),
                            //pdu: (imponibile * 0.90).round(2),
                            pdu: (imponibile -(imponibile * 0.10)).round(2),
                            provvigioniVW: (imponibile * 0.10).round(2),
                            provvigioniMach1: (imponibile * 0.010).round(2)
                    ]
                    println "risultati tariffa 2017 pcl->${pcl} imponibile-> ${imponibile}  pdu ${(imponibile * 0.90).round(2)} provvigioniVW-> ${(imponibile * 0.10).round(2)} provvigioniMach1-> ${(imponibile * 0.90 * margineMach1).round(2)}"
                    logg =new Log(parametri: "risultati tariffa 2017 pcl->${pcl} imponibile-> ${imponibile}  pdu ${(imponibile * 0.90).round(2)} provvigioniVW-> ${(imponibile * 0.10).round(2)} provvigioniMach1-> ${(imponibile * 0.90 * margineMach1).round(2)}", operazione: "calcola rc 2017", pagina: "CREAZIONE APPLICAZIONE")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }else{
                    /****VECCHIO CALCOLO RC****/
                    /**/
                    imponibile = (imponibileRC(tariffa, tipoVeicolo, targaProva) * durataPercentuale).round(2)
                    pcl = (imponibile * 1.265).round(2)
                    margineMach1 = 0.1
                    if(new Date()[Calendar.YEAR] > 2015) margineMach1 = 0.05 //portata al 5% come da mail del 18-12-2015 di Susanna Facciotto (oggetto: LIBRO MATRICOLA 2016)
                    risultati.'rc' = [
                            pcl: pcl,
                            imponibile: imponibile,
                            imposte: (pcl - imponibile).round(2),
                            pdu: (imponibile * 0.95).round(2),
                            provvigioniVW: (imponibile * 0.05).round(2),
                            provvigioniMach1: (imponibile * 0.95 * margineMach1).round(2)
                    ]
                    println "risultati tariffa vecchia pcl->${pcl} imponibile-> ${imponibile}  pdu ${(imponibile * 0.90).round(2)} provvigioniVW-> ${(imponibile * 0.10).round(2)} provvigioniMach1-> ${(imponibile * 0.90 * margineMach1).round(2)}"
                    logg =new Log(parametri: "risultati tariffa vecchia pcl->${pcl} imponibile-> ${imponibile}  pdu ${(imponibile * 0.90).round(2)} provvigioniVW-> ${(imponibile * 0.10).round(2)} provvigioniMach1-> ${(imponibile * 0.90 * margineMach1).round(2)}", operazione: "calcola rc 2017", pagina: "CREAZIONE APPLICAZIONE")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }


            } else if(prodotto == 'cvt') {
                //CVT
                def imponibile = (imponibileCVT(tariffa, tipoVeicolo, targaProva, valore, provincia, veicolo) * durataPercentuale).round(2)

                def pcl = (imponibile * 1.135).round(2)
                risultati."$prodotto" = [
                    pcl: pcl,
                    imponibile: imponibile,
                    imposte: (pcl - imponibile).round(2),
                    pdu: (imponibile * 0.85).round(2),
                    provvigioniVW: (imponibile * 0.15).round(2),
                    provvigioniMach1: (imponibile * 0.85 * 0.2).round(2)
                ]
                logg =new Log(parametri: "risultati pcl->${pcl} imponibile-> ${imponibile} imposte--> ${(pcl - imponibile).round(2)} pdu ${(imponibile * 0.85).round(2)} provvigioniVW-> ${(imponibile * 0.15).round(2)} provvigioniMach1-> ${(imponibile * 0.85 * 0.2).round(2)}", operazione: "calcola cvt", pagina: "CREAZIONE APPLICAZIONE")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            } else if(prodotto == 'infortuni 100.000') {
                //Inf100
                def imponibile = (tariffa.imponibileInfortuni100 * durataPercentuale).round(2)
                def pcl = (imponibile * 1.025).round(2)
                risultati."$prodotto" = [
                    pcl: pcl,
                    imponibile: imponibile,
                    imposte: (pcl - imponibile).round(2),
                    pdu: imponibile,
                    provvigioniVW: 0.0,
                    provvigioniMach1: (imponibile * 0.22).round(2)
                ]
                logg =new Log(parametri: "risultati pcl->${pcl} imponibile-> ${imponibile} imposte--> ${(pcl - imponibile).round(2)} pdu ${(imponibile)} provvigioniVW-> ${0.0} provvigioniMach1-> ${(imponibile * 0.22).round(2)}", operazione: "calcola infortuni 100.000", pagina: "CREAZIONE APPLICAZIONE")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            } else if(prodotto == 'infortuni 200.000') {
                //Inf200
                def imponibile = (tariffa.imponibileInfortuni200 * durataPercentuale).round(2)
                def pcl = (imponibile * 1.025).round(2)
                risultati."$prodotto" = [
                    pcl: pcl,
                    imponibile: imponibile,
                    imposte: (pcl - imponibile).round(2),
                    pdu: imponibile,
                    provvigioniVW: 0.0,
                    provvigioniMach1: (imponibile * 0.22).round(2)
                ]
                logg =new Log(parametri: "risultati pcl->${pcl} imponibile-> ${imponibile} imposte--> ${(pcl - imponibile).round(2)} pdu ${(imponibile)} provvigioniVW-> ${0.0} provvigioniMach1-> ${(imponibile * 0.22).round(2)}", operazione: "calcola infortuni 200.000", pagina: "CREAZIONE APPLICAZIONE")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            } else if(prodotto == 'kasko' && prodotti.contains('cvt')) {
                //Kasko
                //println "tariffa-> $tariffa tipoveicolo $tipoVeicolo valore $valore"
                def imponibile = (imponibileKasko(tariffa, tipoVeicolo, valore) * durataPercentuale).round(2)
                def pcl = (imponibile * 1.135).round(2)
                risultati."$prodotto" = [
                    pcl: pcl,
                    imponibile: imponibile,
                    imposte: (pcl - imponibile).round(2),
                    pdu: (imponibile * 0.85).round(2),
                    provvigioniVW: (imponibile * 0.15).round(2),
                    provvigioniMach1: (imponibile * 0.85 * 0.2).round(2)
                ]
                logg =new Log(parametri: "risultati pcl->${pcl} imponibile-> ${imponibile} imposte--> ${(pcl - imponibile).round(2)} pdu ${(imponibile * 0.85).round(2)} provvigioniVW-> ${(imponibile * 0.15).round(2)} provvigioniMach1-> ${ (imponibile * 0.85 * 0.2).round(2)}", operazione: "kasko cvt", pagina: "CREAZIONE APPLICAZIONE")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            } else if(prodotto == 'tutela giudiziaria') {
                //Tutela giudiziaria
                def imponibile = (imponibileTutelaGiudiziaria(tariffa, tipoVeicolo) * durataPercentuale).round(2)
                def pcl = (imponibile * 1.135).round(2)
                risultati."$prodotto" = [
                    pcl: pcl,
                    imponibile: imponibile,
                    imposte: (pcl - imponibile).round(2),
                    pdu: imponibile,
                    provvigioniVW: 0.0,
                    provvigioniMach1: (imponibile * 0.17).round(2)
                ]
                logg =new Log(parametri: "risultati pcl->${pcl} imponibile-> ${imponibile} imposte--> ${(pcl - imponibile).round(2)} pdu ${(imponibile )} provvigioniVW-> ${0.0} provvigioniMach1-> ${(imponibile * 0.17).round(2)}", operazione: "tutela giudiziaria", pagina: "CREAZIONE APPLICAZIONE")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            }
        }
        logg =new Log(parametri: "risultati ->${risultati} ", operazione: "calcoli", pagina: "CREAZIONE APPLICAZIONE")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        //log.info "Info calcoli: ${(durataPercentuale * 100).round(2)}% -> $risultati"
        return risultati
    }
    
    def imponibileRC(tariffa, tipoVeicolo, targaProva) {
        switch(tipoVeicolo) {
            case AUTOVETTURA:
            case AUTOCARRO: return tariffa.imponibileRCVettureAutocarriTargheProva
            case AUTOVETTURA_NOLEGGIO:
            case AUTOCARRO_NOLEGGIO: return tariffa.imponibileRCVettureAutocarriNoleggio
            case MOTO: return tariffa.imponibileRCMoto
            case TARGAPROVA: return tariffa.imponibileRCVettureAutocarriTargheProva
            default:
                if(targaProva) return tariffa.imponibileRCVettureAutocarriTargheProva
                else return 0
        }
    }
    def imponibileRC2017(zRC, tipoVeicolo, targaProva,cv, destUso,tariffeACRC) {
        def logg
        logg =new Log(parametri: "parametri calcolo imponibile rc2017 destinazione uso--> ${destUso}  tariffeACRC--> ${tariffeACRC}  zRC--> ${zRC}  tipoVeicolo--> ${tipoVeicolo}   targaProva--> ${targaProva}  cv--> ${cv} ", operazione: "calcolo tariffa rca 2017", pagina: "CREAZIONE APPLICAZIONE")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        println "destinazione uso--> ${destUso}"
        println "tariffeACRC--> ${tariffeACRC}"
        println "zRC--> ${zRC}"
        println "tipoVeicolo--> ${tipoVeicolo}"
        println "targaProva--> ${targaProva}"
        println "cv--> ${cv}"

        switch(tipoVeicolo) {
            case AUTOVETTURA:
                if(cv<=12 && zRC.toString().toUpperCase().trim()=="Z1" && destUso.toString().toUpperCase().trim() != "NOLEGGIO")return 348.00
                else if((cv>12 && cv<=20) && zRC.toString().toUpperCase().trim()=="Z1" && destUso.toString().toUpperCase().trim() != "NOLEGGIO")return 425.00
                else if (cv>20 && zRC.toString().toUpperCase().trim()=="Z1" && destUso.toString().toUpperCase().trim() != "NOLEGGIO") return 543.00
                else if(cv<=12 && zRC.toString().toUpperCase().trim()=="Z2" && destUso.toString().toUpperCase().trim() != "NOLEGGIO")return 425.00
                else if((cv>12 && cv<=20) && zRC.toString().toUpperCase().trim()=="Z2" && destUso.toString().toUpperCase().trim() != "NOLEGGIO")return 519.00
                else if (cv>20 && zRC.toString().toUpperCase().trim()=="Z2" && destUso.toString().toUpperCase().trim() != "NOLEGGIO") return 663.00
                else if(cv<=12 && zRC.toString().toUpperCase().trim()=="Z3" && destUso.toString().toUpperCase().trim() != "NOLEGGIO")return 564.00
                else if((cv>12 && cv<=20) && zRC.toString().toUpperCase().trim()=="Z3" && destUso.toString().toUpperCase().trim() != "NOLEGGIO")return 689.00
                else if (cv>20 && zRC.toString().toUpperCase().trim()=="Z3" && destUso.toString().toUpperCase().trim() != "NOLEGGIO") return 881.00
                else if(cv<=12 && zRC.toString().toUpperCase().trim()=="Z1" && destUso.toString().toUpperCase().trim().contains("NOLEGGIO"))return 418.00
                else if((cv>12 && cv<=20) && zRC.toString().toUpperCase().trim()=="Z1" && destUso.toString().toUpperCase().trim().contains("NOLEGGIO"))return 510.00
                else if (cv>20 && zRC.toString().toUpperCase().trim()=="Z1" && destUso.toString().toUpperCase().trim().contains("NOLEGGIO")) return 652.00
                else if(cv<=12 && zRC.toString().toUpperCase().trim()=="Z2" && destUso.toString().toUpperCase().trim().contains("NOLEGGIO"))return 510.00
                else if((cv>12 && cv<=20) && zRC.toString().toUpperCase().trim()=="Z2" && destUso.toString().toUpperCase().trim().contains("NOLEGGIO"))return 623.00
                else if (cv>20 && zRC.toString().toUpperCase().trim()=="Z2" && destUso.toString().toUpperCase().trim().contains("NOLEGGIO")) return 796.00
                else if(cv<=12 && zRC.toString().toUpperCase().trim()=="Z3" && destUso.toString().toUpperCase().trim().contains("NOLEGGIO"))return 677.00
                else if((cv>12 && cv<=20) && zRC.toString().toUpperCase().trim()=="Z3" && destUso.toString().toUpperCase().trim().contains("NOLEGGIO"))return 827.00
                else if (cv>20 && zRC.toString().toUpperCase().trim()=="Z3" && destUso.toString().toUpperCase().trim().contains("NOLEGGIO")) return 1057.00
            case AUTOCARRO:
                if(tariffeACRC.toString().toUpperCase().trim()=="T1" && destUso.toString().toUpperCase().trim() != "NOLEGGIO" && zRC.toString().toUpperCase().trim()=="Z1")return 418.00
                else if(tariffeACRC.toString().toUpperCase().trim()=="T2" && destUso.toString().toUpperCase().trim() != "NOLEGGIO" && zRC.toString().toUpperCase().trim()=="Z1")return 542.00
                else if(tariffeACRC.toString().toUpperCase().trim()=="T3" && destUso.toString().toUpperCase().trim() != "NOLEGGIO" && zRC.toString().toUpperCase().trim()=="Z1")return 642.00
                else if(tariffeACRC.toString().toUpperCase().trim()=="T1" && destUso.toString().toUpperCase().trim() != "NOLEGGIO" && zRC.toString().toUpperCase().trim()=="Z2")return 484.00
                else if(tariffeACRC.toString().toUpperCase().trim()=="T2" && destUso.toString().toUpperCase().trim() != "NOLEGGIO" && zRC.toString().toUpperCase().trim()=="Z2")return 619.00
                else if(tariffeACRC.toString().toUpperCase().trim()=="T3" && destUso.toString().toUpperCase().trim() != "NOLEGGIO" && zRC.toString().toUpperCase().trim()=="Z2")return 728.00
                else if(tariffeACRC.toString().toUpperCase().trim()=="T1" && destUso.toString().toUpperCase().trim() != "NOLEGGIO" && zRC.toString().toUpperCase().trim()=="Z3")return 630.00
                else if(tariffeACRC.toString().toUpperCase().trim()=="T2" && destUso.toString().toUpperCase().trim() != "NOLEGGIO" && zRC.toString().toUpperCase().trim()=="Z3")return 809.00
                else if(tariffeACRC.toString().toUpperCase().trim()=="T3" && destUso.toString().toUpperCase().trim() != "NOLEGGIO" && zRC.toString().toUpperCase().trim()=="Z3")return 984.00
                else if(tariffeACRC.toString().toUpperCase().trim()=="T1" && destUso.toString().toUpperCase().trim().contains("NOLEGGIO") && zRC.toString().toUpperCase().trim()=="Z1")return 501.60
                else if(tariffeACRC.toString().toUpperCase().trim()=="T2" && destUso.toString().toUpperCase().trim().contains("NOLEGGIO") && zRC.toString().toUpperCase().trim()=="Z1")return 650.40
                else if(tariffeACRC.toString().toUpperCase().trim()=="T3" && destUso.toString().toUpperCase().trim().contains("NOLEGGIO") && zRC.toString().toUpperCase().trim()=="Z1") return 770.40
                else if(tariffeACRC.toString().toUpperCase().trim()=="T1" && destUso.toString().toUpperCase().trim().contains("NOLEGGIO") && zRC.toString().toUpperCase().trim()=="Z2")return 580.80
                else if(tariffeACRC.toString().toUpperCase().trim()=="T2" && destUso.toString().toUpperCase().trim().contains("NOLEGGIO") && zRC.toString().toUpperCase().trim()=="Z2")return 742.80
                else if(tariffeACRC.toString().toUpperCase().trim()=="T3" && destUso.toString().toUpperCase().trim().contains("NOLEGGIO")&& zRC.toString().toUpperCase().trim()=="Z2" ) return 873.60
                else if(tariffeACRC.toString().toUpperCase().trim()=="T1" && destUso.toString().toUpperCase().trim().contains("NOLEGGIO") && zRC.toString().toUpperCase().trim()=="Z3")return 756.00
                else if(tariffeACRC.toString().toUpperCase().trim()=="T2" && destUso.toString().toUpperCase().trim().contains("NOLEGGIO") && zRC.toString().toUpperCase().trim()=="Z3")return 970.80
                else if(tariffeACRC.toString().toUpperCase().trim()=="T3" && destUso.toString().toUpperCase().trim().contains("NOLEGGIO")&& zRC.toString().toUpperCase().trim()=="Z3" ) return 1180.80
            //case AUTOVETTURA_NOLEGGIO:if(cv<12 && tariffaRC=="T1")return 418.00 else if((cv>=12 && cv<20) && tariffaRC=="T1")return 510.00 else if (cv>=20 && tariffaRC=="T1") return 652.00 else if(cv<12 && tariffaRC=="T2")return 510.00 else if((cv>=12 && cv<20) && tariffaRC=="T2")return 623.00 else if (cv>=20 && tariffaRC=="T2") return 796.00 else if(cv<12 && tariffaRC=="T3")return 677.00 else if((cv>=12 && cv<20) && tariffaRC=="T3")return 827.00 else if (cv>=20 && tariffaRC=="T3") return 1057.00
            //case AUTOCARRO_NOLEGGIO: if(tariffaRC=="T1")return 501.60 else if(tariffaRC=="T2")return 580.80 else if(tariffaRC=="T3")return 756.00
            case MOTO:
                return 310.00
            default:
                if(targaProva) return 670.00
                else return 0
        }

    }

    def imponibileCVT(tariffa, tipoVeicolo, targaProva, valore = 0, provincia = null, veicolo = null) {

        if(tipoVeicolo in [AUTOVETTURA, AUTOVETTURA_NOLEGGIO, AUTOCARRO,AUTOCARRO_NOLEGGIO] && !targaProva) {
            def zona = provincia.zona
            def rischio = veicolo.rischio.name().toLowerCase()
            return valore * MoltiplicatoreCVT.findByZonaAndTariffa(zona, tariffa)."$rischio" / 1000
        } else if(targaProva) {
            return valore * tariffa.moltiplicatoreCVTTargheProva / 1000
        } else if(tipoVeicolo == MOTO) {
            return valore * tariffa.moltiplicatoreCVTMoto / 1000
        } else return 0
    }
    
    def imponibileKasko(tariffa, tipoVeicolo, valore) {
        if(tipoVeicolo in [AUTOVETTURA, AUTOVETTURA_NOLEGGIO, AUTOCARRO, AUTOCARRO_NOLEGGIO, TARGAPROVA]) {
            println "sono qui kasko --> $tariffa, $tipoVeicolo, $valore"
            return valore * tariffa.moltiplicatoreKasko / 1000
        } else if(tipoVeicolo == MOTO) {
            return valore * tariffa.moltiplicatoreCVTKaskoMoto / 1000
        } else return 0
    }

    def imponibileTutelaGiudiziaria(tariffa, tipoVeicolo) {
        if(tipoVeicolo == MOTO) return tariffa.imponibileTutelaGiudiziariaMoto
        else return tariffa.imponibileTutelaGiudiziaria
    }

    def calcolaPercentuale(Date decorrenza, Date scadenza) {
        return use(TimeCategory) { ((scadenza - decorrenza.clearTime()).days + 1) / scadenza[Calendar.DAY_OF_YEAR] }
    }

    private int diffDateInDays(Date start, Date end) {
        def logg
        if(start > end) {
            def temp = end
            end = start
            start = temp
        }
       /* println "end[Calendar.MONTH]-->>>>>${end[Calendar.MONTH]}\n\n"
        println "start[Calendar.MONTH]-->>>>>${start[Calendar.MONTH]}\n\n"
        println "end[Calendar.DAY_OF_MONTH]-->>>>>${end[Calendar.DAY_OF_MONTH]}\n\n"
        println "start[Calendar.DAY_OF_MONTH]-->>>>>${start[Calendar.DAY_OF_MONTH]}\n\n"*/

        def diff = [
                end[Calendar.YEAR] - start[Calendar.YEAR],
                end[Calendar.MONTH] - start[Calendar.MONTH],
                Math.abs(end[Calendar.DAY_OF_MONTH] - start[Calendar.DAY_OF_MONTH]),
                end[Calendar.HOUR] - start[Calendar.HOUR],
                end[Calendar.MINUTE] - start[Calendar.MINUTE],
                end[Calendar.SECOND] - start[Calendar.SECOND]
        ]
       // println "diff-->>>>>${diff}\n\n"
        logg =new Log(parametri: "diff-->>>>>${diff}", operazione: "calcolo diff giorni", pagina: "Elenco applicazioni")
        if(!logg.save()) println "Errori salvataggio log reponse: ${logg.errors}"
        def delta = [ 12, 31, 24, 60, 60 ]
        delta[ 1 ] = end
        for ( def i = diff.size() - 1; i > -1; i-- ) {
            if ( diff[ i ] < 0 ) {
                if ( i > -1 ) {
                    def j = i - 1
                    diff[ i ] += delta[ j ]
                    diff[ j ]--
                } else {
                    logg =new Log(parametri: "differenza negativa di anni:  ${diff[ 0 ]}", operazione: "calcolo diff giorni", pagina: "Elenco applicazioni")
                    if(!logg.save()) println "Errori salvataggio log reponse: ${logg.errors}"
                    println "Negative year difference:  ${diff[ 0 ]}"
                }
            }
        }
        println "diff dopo ciclo for-->>>>>${diff}\n\n"
        logg =new Log(parametri: "diff dopo ciclo for-->>>>>${diff}", operazione: "calcolo diff giorni", pagina: "Elenco applicazioni")
        if(!logg.save()) println "Errori salvataggio log reponse: ${logg.errors}"

        return diff[ 0 ] * 360 + diff[ 1 ] * 30 + diff[ 2 ]
    }






    /**
     * @return il numero di giorni dell'anno corrente
     */
    private int getGiorniNellAnno() {
        def calendar = Calendar.instance
        return calendar.getActualMaximum(Calendar.DAY_OF_YEAR)
    }
    
}

