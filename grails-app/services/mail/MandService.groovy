package mail

import com.itextpdf.text.pdf.BaseFont
import com.itextpdf.text.pdf.PdfContentByte
import com.itextpdf.text.pdf.PdfReader
import com.itextpdf.text.pdf.PdfStamper
import org.codehaus.groovy.grails.web.util.WebUtils
/*import org.grails.mandrill.MandrillAttachment
import org.grails.mandrill.MandrillMessage
import org.grails.mandrill.MandrillRecipient
import org.grails.mandrill.MergeVar*/
import utenti.Log

import javax.servlet.http.HttpSession
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

class MandService {
    static transactional = false
    def mandrillService
    def grailsApplication
    def performanceLogger
    def ftpService
    def mailService
    static fontsB = [
        "normal": BaseFont.HELVETICA,
        "bold": BaseFont.HELVETICA_BOLD
    ]


    def invioMail(def filesNameAcc, def filesContAcc,  def filesNameEsc, def filesContEsc){
        def logg
        def fileEncode
        def  recpts = [], attachs = [], contents = []
        def respTotale = [:]
        if(filesContAcc){
            fileEncode = new sun.misc.BASE64Encoder().encode(filesContAcc)
        //    attachs.add(new MandrillAttachment(type: "application/pdf", name: "${filesNameAcc}", content: fileEncode))
        }
        if(filesContEsc){
            fileEncode = new sun.misc.BASE64Encoder().encode(filesContEsc)
          //  attachs.add(new MandrillAttachment(type: "application/pdf", name: "${filesNameEsc}", content: fileEncode))
        }

        /*def vars = [new MergeVar(name: "COMUNICAZIONE", content: "Questa è una prova")]
        recpts.add(new MandrillRecipient(name: "mittente", email: "priscila@presstoday.com"))
        def mandrillMessage = new MandrillMessage(
                text: "this is a text message",
                subject: "questa è una prova",
                from_email: "priscila@presstoday.com",
                to: recpts,
                attachments: attachs,
                global_merge_vars: vars
        )
        contents.add([name: "", content: ""])
        def respMail = mandrillService.sendTemplate(mandrillMessage, "creazione-sinistro-iassicur", contents)
        def inviato = respMail.count { r -> r.success == false } == 0
        respTotale.invio = inviato
        def responseR = respMail.collect { r ->
            [
                    status: r?.status,
                    message: r?.message,
                    emailDest: r?.email,
                    id: r?.id,
                    rejectReason: r?.rejectReason,
                    successDest: r?.success
            ].collect { k, v -> "$k: $v" }.join(", ")
        }.join("\n")
        logg=new Log(parametri: "valore risposta email ${responseR}", operazione: "invioMail", pagina: "EsportazioneCompagniaJob").save(flush: true)
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"*/
        def cheMail=mailService.sendMail {
            multipart true
            to "priscila@presstoday.com"
            subject "test"
            text "questa è una prova"
            attach "${filesNameAcc}", "application/pdf", filesContAcc as Byte []
            attach "${filesNameEsc}", "application/pdf", filesContEsc as Byte []
        }
        def risposta=cheMail.asBoolean()
        logg =new Log(parametri: "risposta invio della mail inviata  ${cheMail.asBoolean()}", operazione: "inviaMail", pagina: "DETTAGLIO SINISTRO")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        return risposta
    }


    def createCanvasAndStreams(document, page) {
        def output = new ByteArrayOutputStream()
        def writer = new PdfStamper(document, output)
        PdfContentByte canvas = writer.getOverContent(page)
        return [canvas, output, writer]
    }

    def writeText(canvas, x, y, String text, fontSize, color = "nero", font){
        def carattere = BaseFont.createFont(fontsB[font], BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        canvas.beginText()
        canvas.moveText(x as float,y as float)
        canvas.setFontAndSize(carattere, fontSize as float)
        if(color == "rosso") canvas.setRGBColorFill(192, 0, 0)
        else if (color == "bianco") canvas.setRGBColorFill(255, 255, 255)
        else canvas.setRGBColorFill(0, 0, 0)
        canvas.showText(text)
        canvas.endText()
    }

    private HttpSession getSession() { WebUtils.retrieveGrailsWebRequest().getSession() }
}
