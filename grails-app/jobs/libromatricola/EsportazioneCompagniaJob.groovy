package libromatricola

import grails.util.Environment
import groovy.time.TimeCategory
import groovy.time.TimeCategory as TC
import polizze.Applicazione
import polizze.Flusso
import utenti.Log

class EsportazioneCompagniaJob {

    def ftpService
    def mailService
    def applicazioniService

    static triggers = {
        if(Environment.current == Environment.PRODUCTION) {
           cron name: 'esportazioneTracciatiCompagnia', cronExpression: '0 05 17 * * ?'
        }
    }

    def execute() {
        log.info 'EsportazioneCompagniaJob triggered'
        uploadFlussoAttivazioni()
    }

    def uploadFlussoAttivazioni() {
        def logg
        def filesNameAcc, filesNameEsc
        def filesContAcc, filesContEsc
        def dest="priscila@presstoday.com"
        def dest2="priscila@presstoday.com"
        boolean risposta = false
        def giornoOdierno=new Date().format("EEE")

        if(Environment.current == Environment.DEVELOPMENT) {
            dest="priscila@presstoday.com"
        }else if(Environment.current == Environment.TEST) {
            dest="s.carino@mach-1.it"
            dest2="c.sivelli@mach-1.it"
        }
        else{
            dest="libromatricola@mach-1.it"
            dest2="analisi@mach-1.it"
        }
       applicazioniService.uploadFlussoCompagniaAtt().each{fileName, fileContent ->

           filesNameAcc=fileName
           filesContAcc=fileContent
       }
        logg =new Log(parametri: "oggi è il giorno  ${giornoOdierno} ", operazione: "esportazione compagnia", pagina: "JOB esportazione file per compagnia")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        if(giornoOdierno.contains("lun")){
            logg =new Log(parametri: "oggi è il giorno  ${giornoOdierno} ", operazione: "esportazione compagnia", pagina: "JOB esportazione file per compagnia")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            applicazioniService.uploadFlussoCompagniaEsc().each{fileName, fileContent ->
                filesNameEsc = fileName
                filesContEsc= fileContent
            }
        }else{
            logg =new Log(parametri: "oggi non viene effetuata la estrazione delle esclusioni", operazione: "esportazione compagnia", pagina: "JOB esportazione file per compagnia")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        }

        if(filesContEsc && filesContAcc){
            //println "filesContAcc ${filesContAcc.getClass()}"

            def dir = "Estrazioni ${new Date().format("yyyyMMdd")}"
            logg =new Log(parametri: "ho trovato delle applicazioni escluse e applicazioni attive", operazione: "inviaMail", pagina: "Esportazione compagnia job")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            ftpService.ftpsuploadFile(filesContAcc, filesNameAcc, dir)
            ftpService.ftpsuploadFile(filesContEsc, filesNameEsc, dir)
            risposta=mailService.sendMail {
                multipart true
                to "${dest}"
                if(Environment.current == Environment.PRODUCTION) {cc "${dest2}"}
                subject "Invio estrazioni"
                text "In allegato i file contenenti le estrazioni delle applicazioni attivate ed escluse rispettivamente"
                attach "${filesNameEsc}", "application/pdf", filesContEsc as Byte []
                attach "${filesNameAcc}", "application/pdf", filesContAcc as Byte []
            }
        }else if(filesContAcc  && !filesContEsc){
            //println "filesContAcc ${filesContAcc.getClass()}"

            def dir = "Estrazioni ${new Date().format("yyyyMMdd")}"
            logg =new Log(parametri: "ho trovato solo delle applicazioni attivate ", operazione: "inviaMail", pagina: "Esportazione compagnia job")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            ftpService.ftpsuploadFile(filesContAcc, filesNameAcc, dir)
            risposta=mailService.sendMail {
                multipart true
                to "${dest}"
                if(Environment.current == Environment.PRODUCTION) {cc "${dest2}"}
                subject "Invio estrazioni"
                text "In allegato il file contenente le estrazioni delle applicazioni attivate, non sono state generate applicazioni escluse"
                attach "${filesNameAcc}", "application/pdf", filesContAcc as Byte []
            }
        }else if(!filesContAcc && filesContEsc){
            //println "filesContEsc ${filesContEsc.getClass()}"

            def dir = "Estrazioni ${new Date().format("yyyyMMdd")}"
            logg =new Log(parametri: "ho trovato solo delle applicazioni escluse", operazione: "inviaMail", pagina: "Esportazione compagnia job")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            ftpService.ftpsuploadFile(filesContEsc, filesNameEsc, dir)
            risposta=mailService.sendMail {
                multipart true
                to "${dest}"
                if(Environment.current == Environment.PRODUCTION) {cc "${dest2}"}
                subject "Invio estrazioni"
                text "In allegato il file contenente le estrazioni delle applicazioni escluse, non sono state attivate delle applicazioni"
                attach "${filesNameEsc}", "application/pdf", filesContEsc as Byte []
            }
        }else{
            logg =new Log(parametri: "non ci sono estrazioni", operazione: "inviaMail", pagina: "Esportazione compagnia job")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        }
        //def risposta=mandService.invioMail(filesNameAcc, filesContAcc, filesNameEsc, filesContEsc)

        logg =new Log(parametri: "risposta invio della mail inviata  ${risposta}", operazione: "inviaMail", pagina: "Esportazione compagnia job")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        //if(risposta.contains("queued")||risposta.contains("sent") ||risposta.contains("scheduled") ){
        if(risposta){
            logg=new Log(parametri: "l'invio mail a: ${dest} e' stato effetuato", operazione: "invioMail", pagina: "EsportazioneCompagniaJob").save(flush: true)
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        }else{
            new Log(parametri: "l'invio mail a: ${dest} e' fallito", operazione: "invioMail", pagina: "EsportazioneCompagniaJob").save(flush: true)
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        }
    }


}
