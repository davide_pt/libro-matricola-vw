package libromatricola

import polizze.*
import static polizze.StatoApplicazione.*

class ScadenzaJob {

    def mailService
    def messageSource

    static triggers = {
        cron name: 'scadenzaPolizze', cronExpression: '0 0 0 * * ?'
    }

    def execute() {
        log.info 'ScadenzaJob triggered'
        mettiPolizzeInScadenza()
    }

    def mettiPolizzeInScadenza() {
        def now = new Date().clearTime(), errors = []
        def applicazioniInScadenza = Applicazione.findAllByStatoAndDataScadenzaLessThanEquals(POLIZZA, now)
        applicazioniInScadenza.each { applicazione ->
            applicazione.stato = SCADUTA
            if(!applicazione.save(flush: true)) errors << [numero: applicazione.numero, errors: applicazione.errors]
        }
        if(errors) mailService.sendMail {
            multipart true
            to "priscila@presstoday.com"
            subject "Errore messa in scadenza polizze Libro Matricola (app.mach-1.it/LibroMatricola)"
            text "Si sono verificati errori durante la messa in scadenza delle polizze Libro Matricola.\nVedere allegati."
            errors.each { error -> attach "${error.numero}.txt", "text/plain", error.errors.allErrors.collect { er -> messageSource.getMessage(er, null) }.join("\n") }
        }
    }

}
