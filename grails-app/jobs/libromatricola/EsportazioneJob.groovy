package libromatricola

import grails.util.Environment
import polizze.*
import groovy.time.TimeCategory as TC
import utenti.Log

class EsportazioneJob {

    def ftpService
    def itextService
    def mailService
    def applicazioniService

    static triggers = {
        if(Environment.current == Environment.PRODUCTION) {
            cron name: 'esportazioneTracciati', cronExpression: '0 15 6 * * ?'
        }
    }

    def execute() {
        log.info 'EsportazioneJob triggered'
        uploadFlusso()
        uploadFlussoANIA()
        uploadContrassegni()
        uploadFlussoEsclusioni()
    }

    def uploadFlusso() {
        def today = new Date()
        def tracciatiDaEsportare = TracciatiDaEsportare.list()
        def tracciati = tracciatiDaEsportare*.tracciato.join('\r\n')
        if(tracciati) {
            def file = tracciati.bytes, fileName = "clper441_${today.format('yyyyMMddkkmm')}_rcigio.txt"
            ftpService.uploadFile(file, fileName)
            tracciatiDaEsportare.each { tracciato -> tracciato.dataEsportazione = today }*.save()
            def flusso = new Flusso(file: file, fileName: fileName)
            if(!flusso.save()) {
                mailService.sendMail {
                    multipart true
                    to 'priscila@presstoday.com'
                    subject 'Errore salvataggio flusso'
                    text "Si sono verificati degli errori durante il salvataggio del flusso '$fileName': $flusso.errors"
                    attach fileName, 'text/plain', file
                }
            }
        } else log.info "Non ci sono tracciati da esportare"
    }
    def uploadFlussoEsclusioni() {
        applicazioniService.uploadFlussoEsclusioni()
        /*def today = new Date()
        //def tracciatiDaEsportare = Tracciatiannulla.list()
        def tracciatiDaEsportare = TracciatoIass.createCriteria().list(){
            isNull("dataEsportazione")
            eq "tipo",TipoTracciato.ANNULLAMENTO
            groupProperty("dataAnnullamento")
        }
        def tracciati = tracciatiDaEsportare*.tracciato.join('\r\n')
        if(tracciati) {
            def file = tracciati.bytes, fileName = "clper441_${today.format('yyyyMMddkkmm')}_rcigio.txt"
            ftpService.ftpsuploadFile(file, fileName)
            tracciatiDaEsportare.each { tracciato -> tracciato.dataEsportazione = today }*.save()
            def flusso = new Flusso(file: file, fileName: fileName)
            if(!flusso.save()) {
                mailService.sendMail {
                    multipart true
                    to 'priscila@presstoday.com'
                    subject 'Errore salvataggio flusso'
                    text "Si sono verificati degli errori durante il salvataggio del flusso '$fileName': $flusso.errors"
                    attach fileName, 'text/plain', file
                }
            }
        } else log.info "Non ci sono tracciati da esportare"*/
    }

    def uploadFlussoANIA() {
        def today = use(TC) { new Date() + 10.minutes }
        def tracciatiAniaDaEsportare = TracciatiAniaDaEsportare.list()
        def tracciati = tracciatiAniaDaEsportare*.tracciato.join('\r\n')
        if(tracciati) {
            def file = tracciati.bytes, fileName = "clper441_${today.format('yyyyMMddkkmm')}_rcigio.txt"
            ftpService.uploadFile(file, fileName)
            tracciatiAniaDaEsportare.each { tracciato -> tracciato.dataEsportazione = today }*.save()
            def flusso = new Flusso(file: file, fileName: fileName)
            if(!flusso.save()) {
                mailService.sendMail {
                    multipart true
                    to 'priscila@presstoday.com'
                    subject 'Errore salvataggio flusso ANIA'
                    text "Si sono verificati degli errori durante il salvataggio del flusso ANIA '$fileName': $flusso.errors"
                    attach fileName, 'text/plain', file
                }
            }
        } else log.info "Non ci sono tracciati ANIA da esportare"
    }

    /*def uploadContrassegni() {
        def today = new Date()
        def applicazioni = ApplicazioniPagate.withCriteria { projections { property 'id' } }, errors = []
        if(applicazioni) {
            try {
                def dir = "Contrassegni ${today.format()}"
                ftpService.makeDir(dir)
                log.info "Sono stati trovati ${applicazioni.size()} contrassegni da esportare"
                applicazioni.each { id ->
                    def applicazione = Applicazione.get(id)
                    def contrassegno = itextService.generaContrassegno(applicazione)
                    applicazione.contrassegnoGenerato = true
                    if(applicazione.save()) {
                        def file = contrassegno.toByteArray(), fileName = "Contrassegno_${applicazione.numero}.pdf"
                        ftpService.uploadFile(file, fileName, dir)
                    } else errors << [numero: applicazione.numero, errors: applicazione.errors]
                }
                if(errors) mailService.sendMail {
                    to 'priscila@presstoday.com'
                    subject 'Errore salvataggio applicazioni durante esportazione contrassegni'
                    html "Si sono verificati i seguenti errori durante l'esportazione dei contrassegni:<br>${errors.collect { "$it.numero: $it.errors" }.join('<br>')}"
                } else log.info "Contrassegni uploadati correttamente"
            } catch(e) { log.error e.message }
        } else log.info "Non ci sono contrassegni da esportare"
    }*/

    def uploadContrassegni() {/**** nuova implementazione job caricamento contrassegni nov-2016*/
        def logg
        def today = new Date()
        def applicazioni = ApplicazioniNuoviContrassegni.withCriteria { projections { property 'id' } }, errors = []
        //def applicazioni = Polizze.withCriteria { projections { property 'id' } }, errors = []
        if(applicazioni) {
            try {
                def dir = "Contrassegni ${today.format("yyyyMMdd")}"
                log.info "Sono stati trovati ${applicazioni.size()} contrassegni da esportare"
                println "Sono stati trovati ${applicazioni.size()} contrassegni da esportare"
                logg =new Log(parametri: "Sono stati trovati ${applicazioni.size()} contrassegni da esportare", operazione: "caricamento constrassegni", pagina: "esportazione JOB")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                applicazioni.each { id ->
                    def applicazione = Applicazione.get(id)
                    def contrassegno = itextService.generaContrassegno(applicazione)
                    def file = contrassegno.toByteArray(), fileName = "Contrassegno_${applicazione.numero}.pdf"
                    applicazione.contrassegnoGenerato = true
                    if(applicazione.save()) {
                        ftpService.ftpsuploadFile(file, fileName, dir, null, false)
                    } else errors << [numero: applicazione.numero, errors: applicazione.errors]

                }
                if (errors) mailService.sendMail {
                    to 'priscila@presstoday.com'
                    subject 'Errore salvataggio applicazioni durante esportazione contrassegni'
                    html "Si sono verificati i seguenti errori durante l'esportazione dei contrassegni:<br>${errors.collect { "$it.numero: $it.errors" }.join('<br>')}"
                } else log.info "Contrassegni uploadati correttamente"

            } catch(e) {
                log.error e.message
                logg =new Log(parametri: "catch error  ${e.toString()} ", operazione: "caricamento constrassegni", pagina: "Esportazione Job")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            }
        } else {
            log.info "Non ci sono contrassegni da esportare"
            logg =new Log(parametri: "Non ci sono contrassegni da esportare", operazione: "caricamento constrassegni", pagina: "Esportazione Job")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        }
    }

}
