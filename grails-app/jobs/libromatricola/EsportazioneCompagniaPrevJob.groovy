package libromatricola

import grails.util.Environment
import utenti.Log

class EsportazioneCompagniaPrevJob {

    def ftpService
    def mailService
    def applicazioniService

    static triggers = {
        if(Environment.current == Environment.PRODUCTION) {
           //cron name: 'EsportazioneCompagniaPrev', cronExpression: '0 00 9 * * ? 2017'
        }
    }

    def execute() {
        log.info 'EsportazioneCompagniaPrevJob triggered'
        uploadFlussoAttivazioni()
    }

    def uploadFlussoAttivazioni() {
        def logg
        def filesNameAcc, filesNameEsc
        def filesContAcc, filesContEsc
        def dest="priscila@presstoday.com"
        def dest2="priscila@presstoday.com"
        boolean risposta = false
        def giornoOdierno=new Date().format("EEE")

        if(Environment.current == Environment.DEVELOPMENT) {
            dest="priscila@presstoday.com"
        }else if(Environment.current == Environment.TEST) {
            dest="s.carino@mach-1.it"
            dest2="c.sivelli@mach-1.it"
        }
        else{
            dest="libromatricola@mach-1.it"
            dest2="analisi@mach-1.it"
        }
       applicazioniService.uploadFlussoCompagniaPreventivo().each{fileName, fileContent ->

           filesNameAcc=fileName
           filesContAcc=fileContent
       }
        logg =new Log(parametri: "oggi è il giorno  ${giornoOdierno} ", operazione: "esportazione compagnia prev", pagina: "JOB esportazione file per compagnia")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"


        if(filesContAcc){
            //println "filesContAcc ${filesContAcc.getClass()}"

            def dir = "Estrazioni ${new Date().format("yyyyMMdd")}"
            logg =new Log(parametri: "ho trovato solo delle applicazioni attivate ", operazione: "inviaMail prev", pagina: "Esportazione compagnia prev job")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            ftpService.ftpsuploadFile(filesContAcc, filesNameAcc, dir)
            risposta=mailService.sendMail {
                multipart true
                to "${dest}"
                if(Environment.current == Environment.PRODUCTION) {cc "${dest2}"}
                subject "Invio estrazioni"
                text "In allegato il file contenente le estrazioni delle applicazioni attivate"
                attach "${filesNameAcc}", "application/pdf", filesContAcc as Byte []
            }
        }else{
            logg =new Log(parametri: "non ci sono estrazioni preventivi", operazione: "inviaMail", pagina: "Esportazione compagnia prev job")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        }
        //def risposta=mandService.invioMail(filesNameAcc, filesContAcc, filesNameEsc, filesContEsc)

        logg =new Log(parametri: "risposta invio della mail inviata  ${risposta}", operazione: "inviaMail", pagina: "Esportazione compagnia prev job")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        //if(risposta.contains("queued")||risposta.contains("sent") ||risposta.contains("scheduled") ){
        if(risposta){
            logg=new Log(parametri: "l'invio mail a: ${dest} e' stato effetuato", operazione: "invioMail", pagina: "EsportazioneCompagnia prev Job").save(flush: true)
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        }else{
            new Log(parametri: "l'invio mail a: ${dest} e' fallito", operazione: "invioMail", pagina: "EsportazioneCompagnia prev Job").save(flush: true)
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        }
    }


}
