package utenti

class Utente {

    String username
    String password
    boolean deleted
    boolean bloccato

    static constraints = {
        username blank: false, unique: true, maxSize: 40
        password blank: false, maxSize: 40
    }

    static mapping = {
        table 'utenti'
        tablePerHierarchy false
        deleted defaultValue: '0'
        bloccato defaultValue: '0'
    }

    static hibernateFilters = {
        notDeleted condition: 'deleted = 0', default: true
    }

    def beforeInsert() {
        encodePassword()
        return true
    }

    def beforeUpdate() {
        if(isDirty('password')) encodePassword()
        return true
    }

    def beforeDelete() {
        Utente.withNewSession {
            def utente = merge()
            utente.deleted = true
            utente.username = "DELETED $username"
            utente.save(flush: true)
        }
        return false
    }

    String toString() { return username }

    protected void encodePassword() { password = password.encodeAsMD5() }

}
