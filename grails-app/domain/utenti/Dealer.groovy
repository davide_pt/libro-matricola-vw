package utenti

import usertype.*
import polizze.*

class Dealer extends Utente {
	
    String codice
    String ragioneSociale
    Indirizzo indirizzoSedeLegale = new Indirizzo()
    String partitaIva
    String telefono, fax, email
    String numeroLibroMatricola
    int numerazioneApplicazioni
    Tariffa tariffaEmissioni, tariffaRinnovi
    boolean anagraficaConfermata
    
    static hasMany = [applicazioni: Applicazione]
    
    static constraints = {
        codice blank: false, unique: true, maxSize: 5
        ragioneSociale blank: false, maxSize: 40
        indirizzoSedeLegale nullable: false, validator: { indirizzo, dealer, errors ->
            if(!indirizzo.indirizzo) errors.rejectValue('indirizzoSedeLegale.indirizzo', 'dealer.indirizzoSedeLegale.indirizzo.blank', "Specificare l'indirizzo")
            if(!indirizzo.citta) errors.rejectValue('indirizzoSedeLegale.citta', 'dealer.indirizzoSedeLegale.citta.blank', "Specificare la città")
            if(!indirizzo.cap) errors.rejectValue('indirizzoSedeLegale.cap', 'dealer.indirizzoSedeLegale.cap.blank', "Specificare il cap")
            if(!indirizzo.provincia) errors.rejectValue('indirizzoSedeLegale.provincia', 'dealer.indirizzoSedeLegale.provincia.blank', "Specificare la provincia")
        }
        partitaIva blank: false, maxSize: 11
        telefono nullable: true
        fax nullable: true, maxSize: 15
        email nullable: true, email: true, maxSize: 50
        numeroLibroMatricola blank: false, unique: true, matches: '[0-9]{4}'
        tariffaEmissioni nullable: true
        tariffaRinnovi nullable: true
    }
    
    static mapping = {
        table 'utenti_dealer'
        numeroLibroMatricola length: 4
        indirizzoSedeLegale type: IndirizzoUserType, {
            column name: 'indirizzo_sedelegale'
            column name: 'citta_sedelegale'
            column name: 'cap_sedelegale'
            column name: 'provincia_sedelegale'
        }
        anagraficaConfermata defaultValue: '0'
        sort 'ragioneSociale'
    }
    
    def generaNumeroApplicazione() {
        numerazioneApplicazioni += 1
        save()
        "GVW$numeroLibroMatricola" + "$numerazioneApplicazioni".leftPad(4, '0')
    }
    
    String toString() { return ragioneSociale }
    
}

