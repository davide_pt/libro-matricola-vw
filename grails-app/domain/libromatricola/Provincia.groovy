package libromatricola

class Provincia {
	
    String nome, sigla
    int zona,zonaRC
    
    static constraints = {
        nome blank: false, unique: true, maxSize: 45
        sigla blank: false, unique: true, maxSize: 15
        zona range: 1..7
        zonaRC range: 1..7
    }
    
    static mapping = {
        table 'provincie'
        id generator: 'assigned', name: 'sigla'
        sort 'nome'
    }
    
    String getId() { return sigla }
    
    String toString() { return nome }
    
}

