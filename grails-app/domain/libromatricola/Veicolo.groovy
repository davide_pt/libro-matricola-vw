package libromatricola

enum Rischio { 
    ALTO, MEDIO, BASSO, ALTRO
    String toString() { name().toLowerCase().capitalize() }
    def getName() { name() }
}

enum TipologiaVeicolo { 
    AUTOVETTURA('Autovetture'), AUTOCARRO('Autocarri'),  AUTOVETTURA_NOLEGGIO('Autovetture da noleggio'), AUTOCARRO_NOLEGGIO('Autocarri da noleggio'), MOTO('Motocicli'),TARGAPROVA('Targa prova')
    final def value
    TipologiaVeicolo(value) { this.value = value }
    String toString() { value }
    static def list() { [AUTOVETTURA, AUTOCARRO, AUTOVETTURA_NOLEGGIO, AUTOCARRO_NOLEGGIO, MOTO, TARGAPROVA] }
}

class Veicolo {
    
    String marca, modello, codice
    Rischio rischio
    
    static constraints = {
        marca blank: false, size: 2..50
        modello nullable: true, blank: false, size: 2..50
        codice blank: false, maxSize: 5
        rischio blank: false
    }
    
    static mapping = {
        table 'veicoli'
        sort 'marca'
    }
    
    String toString() { return "$marca - ${modello ?: Veicolo.countByMarca(marca) > 1 ? 'Altri modelli' : 'Qualsiasi modello'}" }
	
}

