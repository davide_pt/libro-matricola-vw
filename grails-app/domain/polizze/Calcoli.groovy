package polizze

class Calcoli {
	
    BigDecimal pcl, imponibile, imposte, pdu, provvigioniVW, provvigioniMach1
    
    static belongsTo = Applicazione
    
    static constraints = {
        pcl min: 0.0
        imponibile min: 0.0
        imposte min: 0.0
        pdu min: 0.0
        provvigioniVW min: 0.0
        provvigioniMach1 min: 0.0
    }
    
    static mapping = {
        table 'calcoli'
    }
    
    String toString() { "$pcl $imponibile $imposte $pdu $provvigioniVW $provvigioniMach1" }
    
}

