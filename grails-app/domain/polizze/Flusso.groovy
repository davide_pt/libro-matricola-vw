package polizze

class Flusso {
	
    Date dateCreated
    byte[] file
    String fileName
    BigDecimal size
    
    static constraints = {
        file nullable: false, size: 1..(1024 * 1024 * 10)
        fileName blank: false, matches: 'clper441_2[0-9]{11}_rcigio.txt'
    }
    
    static mapping = {
        table 'flussi'
        fileName sqtType: 'varchar(32)'
        size formula: "char_length(file) / 1000"
    }
    
    String toString() { return fileName }
}

