package polizze

class Tariffa {
	
    String codice
    BigDecimal imponibileRCVettureAutocarriTargheProva
    BigDecimal imponibileRCVettureAutocarriNoleggio
    BigDecimal imponibileRCMoto
    BigDecimal moltiplicatoreCVTTargheProva
    BigDecimal moltiplicatoreKasko
    BigDecimal moltiplicatoreCVTMoto
    BigDecimal moltiplicatoreCVTKaskoMoto
    BigDecimal imponibileTutelaGiudiziaria
    BigDecimal imponibileTutelaGiudiziariaMoto
    BigDecimal imponibileInfortuni100
    BigDecimal imponibileInfortuni200

    /**gennaio 2017 nuovi campi per nuova tariffa**/
    BigDecimal imponibileRCVetture12CV
    BigDecimal imponibileRCVettureNoleggio12CV
    BigDecimal imponibileRCAutocarri12CV
    BigDecimal imponibileRCAutocarriNoleggio12CV
    BigDecimal imponibileRCVetture1220CV
    BigDecimal imponibileRCVettureNoleggio1220CV
    BigDecimal imponibileRCAutocarri1220CV
    BigDecimal imponibileRCAutocarriNoleggio1220CV
    BigDecimal imponibileRCVetture20CV
    BigDecimal imponibileRCVettureNoleggio20CV
    BigDecimal imponibileRCAutocarri20CV
    BigDecimal imponibileRCAutocarriNoleggio20CV
    BigDecimal imponibileRCTargheprova

    static hasMany = [moltiplicatoriCVT: MoltiplicatoreCVT]
    static belongsTo = [versione: VersioneTariffa]
    
    static constraints = {
        codice blank: false, matches: '[A-D][0-9]{2}'
        imponibileRCVettureAutocarriTargheProva min: 0.0, scale: 14
        imponibileRCVettureAutocarriNoleggio min: 0.0, scale: 14
        imponibileRCMoto min: 0.0, scale: 14
        moltiplicatoreCVTTargheProva min: 0.0, scale: 14
        moltiplicatoreKasko min: 0.0, scale: 14
        moltiplicatoreCVTMoto min: 0.0, scale: 14
        moltiplicatoreCVTKaskoMoto min: 0.0, scale: 14
        imponibileTutelaGiudiziaria min: 0.0, scale: 14
        imponibileTutelaGiudiziariaMoto min: 0.0, scale: 14
        imponibileInfortuni100 min: 0.0, scale: 14
        imponibileInfortuni200 min: 0.0, scale: 14
        /**gennaio 2017 nuovi campi per nuova tariffa**/
        imponibileRCVetture12CV  min: 0.0, scale: 14
        imponibileRCVettureNoleggio12CV  min: 0.0, scale: 14
        imponibileRCAutocarri12CV  min: 0.0, scale: 14
        imponibileRCAutocarriNoleggio12CV  min: 0.0, scale: 14
        imponibileRCTargheprova  min: 0.0, scale: 14
        imponibileRCVetture1220CV min: 0.0, scale: 14
        imponibileRCVettureNoleggio1220CV min: 0.0, scale: 14
        imponibileRCAutocarri1220CV min: 0.0, scale: 14
        imponibileRCAutocarriNoleggio1220CV min: 0.0, scale: 14
        imponibileRCVetture20CV min: 0.0, scale: 14
        imponibileRCVettureNoleggio20CV min: 0.0, scale: 14
        imponibileRCAutocarri20CV min: 0.0, scale: 14
        imponibileRCAutocarriNoleggio20CV min: 0.0, scale: 14
    }
    
    static mapping = {
        table 'tariffe'
        codice length: 3
        moltiplicatoriCVT cascade: 'all'
        sort 'codice'
    }
    
    String toString() { return codice }
}

