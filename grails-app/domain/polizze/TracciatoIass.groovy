package polizze


class TracciatoIass {

    Operazione operazione
    String garanzia
    String tracciato
    TipoTracciato tipo = TipoTracciato.ANNULLAMENTO
    Date dataEsportazione
    Date dataAnnullamento

    static belongsTo = [applicazione: Applicazione]
    
    static constraints = {
        garanzia blank: false
        tracciato size: 698..698, unique: true
        dataEsportazione nullable: true
        dataAnnullamento nullable: false
    }
    
    static mapping = {
        table 'tracciatiannulla'
        tracciato type: 'text'
    }
    
    static hibernateFilters = {
        notExportedAnnulla condition: "data_esportazione is null and operazione = 'ANNULLAMENTO'", aliasDomain: 'Tracciatiannulla'
    }
    
    String toString() { return tracciato }
}

