package polizze

enum TipoTracciato { NORMALE, ANIA, ANNULLAMENTO, ANNULLAMENTOANIA }

class Tracciato {

    Operazione operazione
    String garanzia
    String tracciato
    TipoTracciato tipo = TipoTracciato.NORMALE
    Date dataEsportazione
    
    static belongsTo = [applicazione: Applicazione]
    
    static constraints = {
        garanzia blank: false
        tracciato size: 518..698, unique: true
        dataEsportazione nullable: true
    }
    
    static mapping = {
        table 'tracciati'
        tracciato type: 'text'
    }
    
    static hibernateFilters = {
        notExported condition: "data_esportazione is null and tipo = 'NORMALE'", aliasDomain: 'TracciatiDaEsportare'
        notExportedANIA condition: "data_esportazione is null and tipo = 'ANIA'", aliasDomain: 'TracciatiAniaDaEsportare'
    }
    
    String toString() { return tracciato }
}

