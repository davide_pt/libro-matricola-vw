package polizze

class MoltiplicatoreCVT {
	
    int zona
    BigDecimal basso, medio, alto, altro
    
    static belongsTo = [tariffa: Tariffa]
    
    static constraints = {
        zona range: 1..7
        basso min: 0.0, scale: 14
        medio min: 0.0, scale: 14
        alto min: 0.0, scale: 14
        altro min: 0.0, scale: 14
    }
    
    static mapping = {
        table 'moltiplicatori_cvt'
    }
}

