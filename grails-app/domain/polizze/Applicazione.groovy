package polizze

import libromatricola.*
import utenti.*
import java.text.SimpleDateFormat

enum StatoApplicazione { PREVENTIVO, POLIZZA, ESCLUSA, ELIMINATA, SCADUTA }

enum Operazione { INSERIMENTO, ANNULLAMENTO }
enum Alimentazione {DIESEL ('Diesel'), BENZINA ('Benzina'), GPL ('GPL'), METANO('Metano'), IBRIDA ('Ibrida'), ELETTRICA ('Elettrica'), ALTRO('Altro')
    final def value
    Alimentazione(value) { this.value = value }
    String toString() { value }
    static def list() { [DIESEL, BENZINA, GPL, METANO, IBRIDA, ELETTRICA,ALTRO ] }
}

enum ProdottoCheck {
    SI, NO, A, B
    String toString() { name().toLowerCase().capitalize() }
    def getName() { name() }
    static def cvt() { [SI, NO] }
    static def kasko() { [SI, NO] }
    static def rc() { [SI, NO] }
    static def tutela() { [SI, NO] }
    static def infortuni() { [A, B, NO] }
}
enum TariffaRCCheck {
    T1, T2, T3, NO
    String toString() { name().toLowerCase().capitalize() }
    def getName() { name() }
    static def tariffaAutocarri() { [T1, T2, T3, NO] }
}
class Applicazione {

    Date dateCreated, lastUpdated
    Dealer dealer
    Tariffa tariffa
    Provincia provincia
    TipologiaVeicolo tipologiaVeicolo
    Rischio rischio
    String marca, modello, targa, telaio, codiceVeicolo
    String vincolo, motivoEsclusione
    Date dataImmatricolazione
    BigDecimal valoreAssicurato
    int quintali
    StatoApplicazione stato = StatoApplicazione.PREVENTIVO
    Operazione operazione = Operazione.INSERIMENTO
    Applicazione applicazioneEsclusa, applicazioneRinnovata
    Date dataDecorrenza, dataScadenza, dataEsclusione, dataEmissione, dataPagamento
    int annoValidita
    String numero
    boolean deleted
    Date dataInvioDocumenti
    boolean contrassegnoGenerato
    boolean documentiGenerati
    boolean pagata
    boolean targaProva
    ProdottoCheck cvt, kasko, rc, tutela, infortuni
    TariffaRCCheck tariffaAutocarri
    BigDecimal pcl, imponibile, imposte, pdu, provvigioniVW, provvigioniMach1, premiorimborso
    Map calcoli
    String note
    /****novembre***/
    Date scadenzaVincolo
    String destinazioneUso
    Alimentazione alimentazione
    String nposti
    String vincololeasing
    BigDecimal cv, kw,cilindrata


    static hasMany = [calcoli: Calcoli, tracciati: Tracciato, tracciAssicur:TracciatoIass]

    static constraints = {
        marca maxSize: 40, validator: { marca, applicazione -> if(!applicazione.targaProva && !marca) return 'applicazione.marca.blank' }
        //marca maxSize: 40, validator: { marca, applicazione -> if((applicazione.tipologiaVeicolo == TipologiaVeicolo.AUTOVETTURA || applicazione.tipologiaVeicolo == TipologiaVeicolo.AUTOCARRO ||  applicazione.tipologiaVeicolo == TipologiaVeicolo.MOTO ) && !marca) return 'applicazione.marca.blank' }   new Date().parse('yyyy/MM/dd', '2017/10/01')
        modello maxSize: 40, validator: { modello, applicazione -> if(!applicazione.targaProva && !modello) return 'applicazione.modello.blank' }
        cilindrata blank: true, validator: { cilindrata, applicazione -> if(!applicazione.targaProva && !(cilindrata>0.0 ) && applicazione.operazione!=Operazione.ANNULLAMENTO && applicazione.stato!=StatoApplicazione.ELIMINATA && applicazione.tipologiaVeicolo != TipologiaVeicolo.MOTO && applicazione.tipologiaVeicolo != TipologiaVeicolo.AUTOVETTURA && applicazione.dateCreated>=new Date().parse('yyyy-MM-dd', '2017-10-06')) return 'applicazione.cilindrata.min.notmet' }
        alimentazione blank: true,  validator: { alimentazione, applicazione -> if(!applicazione.targaProva && !alimentazione) return 'applicazione.alimentazione.blank' }
        //modello maxSize: 40, validator: { modello, applicazione -> if((applicazione.tipologiaVeicolo == TipologiaVeicolo.AUTOVETTURA || applicazione.tipologiaVeicolo == TipologiaVeicolo.AUTOCARRO  || applicazione.tipologiaVeicolo == TipologiaVeicolo.MOTO ) && !modello) return 'applicazione.modello.blank' }
        targa blank: true, maxSize: 15, validator: { targa, applicazione -> if(applicazione.stato == StatoApplicazione.POLIZZA && !targa) return 'applicazione.targa.blank' }
        telaio blank: true, maxSize: 17
        codiceVeicolo maxSize: 5
        vincolo blank: true, maxSize: 50
        dataImmatricolazione nullable: true, validator: { immatricolazione, applicazione ->
            if(applicazione.stato == StatoApplicazione.POLIZZA && !immatricolazione) return 'applicazione.dataImmatricolazione.nullable'
            if(immatricolazione && immatricolazione.clearTime() > new Date().clearTime()) return 'applicazione.dataImmatricolazione.max.exceeded'
        }
        valoreAssicurato min: 0.00, max: 500000.00, validator : {valoreAssicurato, applicazione ->
            if(!applicazione.targaProva && !(valoreAssicurato>0.0) && (applicazione.cvt==ProdottoCheck.SI )) return 'applicazione.valoreAssicurato.min.notmet'
            else if(!applicazione.targaProva && !(valoreAssicurato<500000.0) && (applicazione.cvt == ProdottoCheck.SI )) return 'applicazione.valoreAssicurato.max.exceeded'
        }
        kw validator : {kw, applicazione ->if(!applicazione.targaProva && !(kw>0.0) && applicazione.operazione!=Operazione.ANNULLAMENTO && applicazione.stato!=StatoApplicazione.ELIMINATA && applicazione.tipologiaVeicolo != TipologiaVeicolo.MOTO && applicazione.tipologiaVeicolo != TipologiaVeicolo.AUTOVETTURA && applicazione.dateCreated>=new Date().parse('yyyy-MM-dd', '2017-10-06')) return 'applicazione.kw.min.notmet'}
        cv validator : {cv, applicazione ->if(!applicazione.targaProva && !(cv>0.0) && applicazione.operazione!=Operazione.ANNULLAMENTO && applicazione.stato!=StatoApplicazione.ELIMINATA && applicazione.tipologiaVeicolo != TipologiaVeicolo.MOTO && applicazione.tipologiaVeicolo != TipologiaVeicolo.AUTOVETTURA && applicazione.dateCreated>=new Date().parse('yyyy-MM-dd', '2017-10-06')) return 'applicazione.cv.min.notmet'}

        quintali validator: { quintali, applicazione ->
            if(quintali < 0) return 'applicazione.quintali.min.notmet'
            else if(quintali > 60) return 'applicazione.quintali.max.exceeded'
        }
        dataDecorrenza nullable: true, validator: { decorrenza, applicazione ->
            def persistent = applicazione.getPersistentValue('dataDecorrenza')
            if(applicazione.stato == StatoApplicazione.POLIZZA && !decorrenza) return 'applicazione.dataDecorrenza.nullable'
            else if(applicazione.stato == StatoApplicazione.POLIZZA && !persistent && decorrenza.clearTime() < new Date().clearTime()) return 'applicazione.dataDecorrenza.min.notmet'
        }
        dataScadenza nullable: true, validator: { scadenza, applicazione ->
            def decorrenza = applicazione.dataDecorrenza
            if(applicazione.stato == StatoApplicazione.POLIZZA && !scadenza) return 'applicazione.dataScadenza.nullable'
            if(scadenza && decorrenza && scadenza <= decorrenza) return 'applicazione.dataScadenza.min.notmet'
            if(scadenza && decorrenza && scadenza > "31-12-${decorrenza.format("yyyy")}".parseDate()) return 'applicazione.dataScadenza.max.exceeded'
        }
        dataEsclusione nullable: true, validator: { esclusione, applicazione ->
            if(applicazione.stato == StatoApplicazione.POLIZZA && applicazione.operazione == Operazione.ANNULLAMENTO && !esclusione) return 'applicazione.dataEsclusione.nullable'
            else if(applicazione.stato == StatoApplicazione.POLIZZA && applicazione.operazione == Operazione.ANNULLAMENTO && esclusione) {
                if(esclusione < applicazione.dataDecorrenza) return 'applicazione.dataEsclusione.min.notmet'
                else if(esclusione > applicazione.dataScadenza) return 'applicazione.dataEsclusione.max.exceeded'
            } else if(applicazione.stato == StatoApplicazione.ESCLUSA && !esclusione) return 'applicazione.dataEsclusione.nullable'
        }
        dataEmissione nullable: true, validator: { emissione, applicazione -> if(applicazione.stato == StatoApplicazione.POLIZZA && !emissione) return 'applicazione.dataEmissione.nullable' }
        dataPagamento nullable: true, validator: { pagamento, applicazione -> if(applicazione.pagata && !pagamento) return 'applicazione.dataPagamento.nullable' }
        numero nullable: true, unique: true
        dataInvioDocumenti nullable: true
        cvt blank: false, inList: ProdottoCheck.cvt(), maxSize: 2
        kasko blank: false, inList: ProdottoCheck.kasko(), maxSize: 2
        rc blank: false, inList: ProdottoCheck.rc(), maxSize: 2
        tutela blank: false, inList: ProdottoCheck.tutela(), maxSize: 2
        infortuni blank: false, inList: ProdottoCheck.infortuni(), maxSize: 2
        tariffaAutocarri blank: false, inList: TariffaRCCheck.tariffaAutocarri(), maxSize: 2
        pcl min: 0.0
        imponibile min: 0.0
        imposte min: 0.0
        pdu min: 0.0
        provvigioniVW min: 0.0
        provvigioniMach1 min: 0.0
        premiorimborso min: 0.0
        calcoli nullable: false
        note blank: true
        destinazioneUso nullable: true, validator: { destinazioneUso, applicazione -> if((applicazione.tipologiaVeicolo == TipologiaVeicolo.AUTOVETTURA || applicazione.tipologiaVeicolo == TipologiaVeicolo.AUTOCARRO  || applicazione.tipologiaVeicolo == TipologiaVeicolo.MOTO || applicazione.tipologiaVeicolo == TipologiaVeicolo.TARGAPROVA) && !destinazioneUso) return 'applicazione.destinazioneUso.nullable' }
        nposti nullable: true, validator: { nposti, applicazione -> if((applicazione.tipologiaVeicolo == TipologiaVeicolo.MOTO) && !nposti) return 'applicazione.nposti.nullable' }
        vincololeasing nullable: true
        scadenzaVincolo nullable: true/*,  validator: { scadenzaVincolo, applicazione ->
            if(scadenzaVincolo < applicazione.dataDecorrenza) return 'applicazione.scadenzaVincolo.min.notmet'
            else if(scadenzaVincolo > applicazione.dataScadenza) return 'applicazione.scadenzaVincolo.max.exceeded'
        }*/
        motivoEsclusione nullable: true
    }

    static mapping = {
        table 'applicazioni'
        deleted defaultValue: '0'
        contrassegnoGenerato defaultValue: '0'
        documentiGenerati defaultValue: '0'
        pagata defaultValue: '0'
        targaProva defaultValue: '0'
        sort dateCreated: 'desc'
        calcoli cascade: 'save-update'
        tracciati cascade: 'save-update'
        tracciAssicur cascade: 'save-update'
        note type: 'text'
        annoValidita formula: "if(data_scadenza is not null, year(data_scadenza), if(data_decorrenza is not null, year(data_decorrenza), year(date_created)))"
        alimentazione defaultValue: Alimentazione.BENZINA
    }

    static hibernateFilters = {
        notDeleted condition: 'deleted = 0', default: true
        preventivi condition: "stato = 'PREVENTIVO'", aliasDomain: 'Preventivi'
        polizze condition: "stato = 'POLIZZA'", aliasDomain: 'Polizze'
        escluse condition: "stato = 'ESCLUSA'", aliasDomain: 'Escluse'
        documentsNotSent condition: 'data_invio_documenti is null', aliasDomain: 'ApplicazioniDaNotificare'
        contrassegnoNotGenerated condition: "contrassegno_generato = 0 and stato = 'POLIZZA'", aliasDomain: 'ApplicazioniNuoviContrassegni'
        documentiNotGenerated condition: "documenti_generati = 0 and stato = 'POLIZZA'", aliasDomain: 'ApplicazioniNuoviDocumenti'
        pagate condition: "contrassegno_generato = 0 and stato = 'POLIZZA' and pagata = 1 and rc = 'SI'", aliasDomain: "ApplicazioniPagate"
    }

    void setDealer(Dealer dealer) {
        if(dealer) {
            this.dealer = dealer
            /*this.tariffa = dealer.tariffa*/
            this.provincia = dealer.indirizzoSedeLegale.provincia
        }
    }

    void setCalcoli(Map calcoli) {
        if(calcoli) {
            this.calcoli = calcoli
            def values = calcoli.values()
            pcl = values.sum { it.pcl }
            imponibile = values.sum { it.imponibile }
            imposte = values.sum { it.imposte }
            pdu = values.sum { it.pdu }
            provvigioniVW = values.sum { it.provvigioniVW }
            provvigioniMach1 = values.sum { it.provvigioniMach1 }
        }
    }

    def beforeInsert() {
        if(targa) targa = targa.toUpperCase()
        if(telaio) telaio = telaio.toUpperCase()
        return true
    }

    def afterInsert() {
        if(stato == StatoApplicazione.PREVENTIVO) numero = "PRE" + "$id".leftPad(8, '0')
    }

    String toString() { return numero }
    /*void setAlimentazione(Alimentazione alimentazione) {
        if(alimentazione) {
            this.alimentazione = alimentazione
            if(alimentazione == null) alimentazione = Alimentazione.BENZINA
        }
    }*/

}

