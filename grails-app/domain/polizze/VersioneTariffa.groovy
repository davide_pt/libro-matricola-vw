package polizze

class VersioneTariffa {

    byte[] file
    String fileName
    String versione
    Date dataCaricamento

    static hasMany = [tariffe: Tariffa]

    static constraints = {
        file maxSize: 1024 * 1024
        fileName size: 5..100
        versione size: 1..25
    }

    static mapping = {
        table "versioni_tariffa"
    }

    String toString() { return versione }
}
