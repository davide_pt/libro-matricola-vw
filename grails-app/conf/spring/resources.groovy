// Place your Spring DSL code here
beans = {
    localeResolver(org.springframework.web.servlet.i18n.SessionLocaleResolver) {
        defaultLocale = new Locale("it","IT")
        java.util.Locale.setDefault(defaultLocale)
    }
    customPropertyEditorRegistrar(editor.CustomEditorRegistrar)
}
