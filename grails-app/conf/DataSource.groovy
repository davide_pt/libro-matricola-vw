dataSource {
    pooled = true
    driverClassName = "com.mysql.jdbc.Driver"
    dialect = org.hibernate.dialect.MySQL5InnoDBDialect
    configClass = org.grails.plugin.hibernate.filter.HibernateFilterDomainConfiguration
    username = "root"
    password = "poi"
    dbCreate = "update"
    url = "jdbc:mysql://ironman/libromatricola_development"
    formatSql = true
    properties {
        maxActive = -1
        minEvictableIdleTimeMillis = 1800000
        timeBetweenEvictionRunsMillis = 1800000
        numTestsPerEvictionRun = 3
        testOnBorrow = true
        testWhileIdle = true
        testOnReturn = true
        validationQuery = "SELECT 1"
    }
}
mandrill {
    apiKey= "UpUWFHEJxNRZyyKUHwUeMg"
}
hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
}
// environment specific settings
environments {
    test {
        dataSource {
            url = "jdbc:mysql://ironman/libromatricola_test"
            password= "Pre55T0day!"
        }
    }
    production {
        dataSource {
            url = "jdbc:mysql://ironman/libromatricola_production"
            password= "Pre55T0day!"
        }
    }
}
