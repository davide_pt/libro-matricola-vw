import polizze.Tariffa
import polizze.VersioneTariffa

import javax.servlet.http.*
import java.math.*
import java.text.*
import java.util.*
import org.apache.commons.lang3.*
import utenti.*

class BootStrap {

    def grailsApplication

    def initMetaClass() {
        String.metaClass.static.random = { int length = 0, List from = ['a'..'z','A'..'Z',0..9] ->
            def pool = from.flatten()
            def rand = new Random(System.currentTimeMillis())
            def str = (0..<length).collect { pool[rand.nextInt(pool.size())] }
            return str.join()
        }

        String.metaClass.decapitalize = {
            if(delegate.size() > 0) {
                def first = delegate[0].toLowerCase()
                return first + (delegate.size() > 2 ? delegate[1..-1] : "")
            } else return delegate
        }

        String.metaClass.icontains = { match -> return delegate.toLowerCase().contains(match.toLowerCase()) }

        String.metaClass.leftPad = { int length, padChar = ' ' ->
            if(delegate.size() > length) delegate = delegate.substring(0, length)
            return StringUtils.leftPad(delegate, length, padChar)
        }

        String.metaClass.rightPad = { int length, padChar = ' ' ->
            if(delegate.size() > length) delegate = delegate.substring(0, length)
            return StringUtils.rightPad(delegate, length, padChar)
        }

        String.metaClass.center = { int length, padChar = ' ' ->
            if(delegate.size() > length) delegate = delegate.substring(0, length)
            return StringUtils.center(delegate, length, padChar)
        }

        String.metaClass.parseDate = { String format = 'dd-MM-yyyy' -> new SimpleDateFormat(format).parse(delegate) }

        Number.metaClass.round = { int precision = 0 ->
            if(precision < 0) precision = 0
            precision = 10 ** precision
            return Math.round(delegate * precision) / precision
        }

        BigDecimal.metaClass.format = { String format = '#,##0.00' -> new DecimalFormat(format).format(delegate) }

        Date.metaClass.format { String format = 'dd-MM-yyyy' -> new SimpleDateFormat(format).format(delegate).toString() }

        HttpSession.metaClass.setUtente = { Utente utente -> delegate.utente_id = utente.ident() }

        HttpSession.metaClass.getUtente = { -> Utente.get(delegate.utente_id) }
    }

    def initUsers() {
        if(Admin.count() == 0) {
            [
                new Admin(username: 'dev', password: 'Pre55T0day', email: 'davide@presstoday.com', isDeveloper: true),
                new Admin(username: 'Mach1', password: 'CarTuri', email: 'backoffice@mach-1.it')
            ].each { utente -> if(!utente.save()) println utente.errors }
        }
    }

    def updateVersioniTariffa() {
        if(VersioneTariffa.count() < 1) {
            def fileName = "VW tariffa 2014 v8 Libro matricola Concessionari.xls"
            def file = grailsApplication.mainContext.getResource("xls/$fileName")?.file?.bytes
            if(file) {
                def versioneTariffa = new VersioneTariffa(file: file, fileName: fileName, versione: "V8", dataCaricamento: "26-03-2014".parseDate())
                if(versioneTariffa.save(flush: true)) {
                    def tariffe = Tariffa.executeUpdate("update Tariffa t set versione = :versione", [versione: versioneTariffa])
                    println "Aggiornate coorettamente $tariffe tariffe"
                } else throw new RuntimeException("Versione $versioneTariffa.versione delle tariffe non caricata: $versioneTariffa.errors")
            } else throw new RuntimeException("Errore grave: $fileName non trovato!!!")

        }
    }

    def init = { servletContext ->
        initMetaClass()
        //initUsers()
        //updateVersioniTariffa()
    }
    def destroy = {
    }
}
