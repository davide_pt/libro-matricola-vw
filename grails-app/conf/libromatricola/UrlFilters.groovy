package libromatricola

class UrlFilters {

    def filters = {
        loginCheck(action: 'login|logout', invert: true) {
            before = {
                if(session.utente == null) {
                    redirect(controller: "utenti", action: "login")
                    return false
                }
            }
        }
        developerCheck(controller: 'sviluppo') {
            before = {
                if(session.utente instanceof utenti.Admin && session.utente.isDeveloper) return true
                else {
                    redirect(controller: "libroMatricola", action: "index")
                    return false
                }
            }
        }
        adminCheck(controller: 'utenti') {
            before = {
                if(actionName in ['login', 'logout'] || session.utente instanceof utenti.Admin) return true
                else {
                    redirect(controller: 'applicazioni', action: 'elenco')
                    return false
                }
            }
        }
    }
}
