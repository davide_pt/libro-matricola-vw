// locations to search for config files that get merged into the main config;
// config files can be ConfigSlurper scripts, Java properties files, or classes
// in the classpath in ConfigSlurper format

// grails.config.locations = [ "classpath:${appName}-config.properties",
//                             "classpath:${appName}-config.groovy",
//                             "file:${userHome}/.grails/${appName}-config.properties",
//                             "file:${userHome}/.grails/${appName}-config.groovy"]

// if (System.properties["${appName}.config.location"]) {
//    grails.config.locations << "file:" + System.properties["${appName}.config.location"]
// }

grails.project.groupId = appName // change this to alter the default package name and Maven publishing destination
grails.mime.file.extensions = true // enables the parsing of file extensions from URLs into the request format
grails.mime.use.accept.header = false
grails.mime.types = [
    all:           '*/*',
    atom:          'application/atom+xml',
    css:           'text/css',
    csv:           'text/csv',
    form:          'application/x-www-form-urlencoded',
    html:          ['text/html','application/xhtml+xml'],
    js:            'text/javascript',
    json:          ['application/json', 'text/json'],
    multipartForm: 'multipart/form-data',
    rss:           'application/rss+xml',
    text:          'text/plain',
    xml:           ['text/xml', 'application/xml'],
    pdf:           'application/pdf',
    rtf:           'application/rtf',
    excel:         'application/vnd.ms-excel',
    ods:           'application/vnd.oasis.opendocument.spreadsheet'
]

// URL Mapping Cache Max Size, defaults to 5000
//grails.urlmapping.cache.maxsize = 1000

// What URL patterns should be processed by the resources plugin
grails.resources.adhoc.patterns = ['/images/*', '/css/*', '/js/*', '/plugins/*']
grails.resources.uri.prefix = 'resources'

// The default codec used to encode data with ${}
grails.views.default.codec = "none" // none, html, base64
grails.views.gsp.encoding = "UTF-8"
grails.converters.encoding = "UTF-8"
// enable Sitemesh preprocessing of GSP pages
grails.views.gsp.sitemesh.preprocess = true
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []
// whether to disable processing of multi part requests
grails.web.disable.multipart=false

// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']

// configure auto-caching of queries by default (if false you can cache individual queries with 'cache: true')
grails.hibernate.cache.queries = false

environments {
    development {
        grails.logging.jul.usebridge = true
        grails.resources.mappers.hashandcache.enabled = false
    }

    production {
        grails.logging.jul.usebridge = false

        ftp.dir = 'Libro Matricola'
        //sftp.url = "sftp://flussi-nais@5.249.141.51:22/Libro Matricola Volkswagen"
        sftp.cartella = 'Libro Matricola Volkswagen'
        sftpannulla.cartella = 'Libro Matricola Volkswagen'
        grails.mail.default.from = 'libromatricola@mach-1.it'
        // TODO: grails.serverURL = "http://www.changeme.com"
    }
}

// log4j configuration
log4j = {
    // Example of changing the log pattern for the default console appender:
    //
    //appenders {
    //    console name:'stdout', layout:pattern(conversionPattern: '%c{2} %m%n')
    //}

    error  'org.codehaus.groovy.grails.web.servlet',        // controllers
           'org.codehaus.groovy.grails.web.pages',          // GSP
           'org.codehaus.groovy.grails.web.sitemesh',       // layouts
           'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
           'org.codehaus.groovy.grails.web.mapping',        // URL mapping
           'org.codehaus.groovy.grails.commons',            // core / classloading
           'org.codehaus.groovy.grails.plugins',            // plugins
           'org.codehaus.groovy.grails.orm.hibernate',      // hibernate integration
           'org.springframework',
           'org.hibernate',
           'net.sf.ehcache.hibernate'
    info 'grails.app.jobs', 'grails.app.services.libromatricola.FtpService', 'grails.app.services.libromatricola.CalcoliService'
    environments {
        production {
            error 'grails.app.jobs', 'grails.app.services.libromatricola.FtpService'
        }
    }
}

grails.sitemesh.default.layout = 'main'

grails.gorm.default.mapping = {
    version false
}

grails {
    mail {
        host = "smtp.mailgun.org"
        port = 587
        username = "davide@mg.presstoday.com"
        password = "Pre55T0day"
    }
}
grails.mail.default.from = 'libromatricola@mach-1.it'

ftp {
    host = '62.152.97.98'
    username = 'Mach1\\ftp-Nais'
    password = 'XXXXrrr655'
    dir = 'Libro Matricola/test'
    failureMail = 'priscila@presstoday.com'
}

/*vecchia implementazione
sftp {
    username = "flussi-nais"
    password = "XYZrrr655"
    url = "sftp://flussi-nais@5.249.141.51:22/Libro Matricola Volkswagen/test"
    options = ["passiveMode=true"]
}*/
sftp {
    username = "flussi-nais"
    password = "XYZrrr655"
    //url = "sftp://flussi-nais@5.249.141.51:22/Libro Matricola Volkswagen/test"
    host ="5.249.141.51"
    port=22
    cartella = "Libro Matricola Volkswagen/test"
    options = ["passiveMode=true"]
}
libromatricola {
    documentiPolizze {
        noMail = 'priscila@presstoday.com'
        testMail = 'priscila@presstoday.com'
    }
    passwordDealer = 'libromatricola'
    testInvitiMail = 's.facciotto@mach-1.it'
}

quartz {
    autoStartup = true
}

sftpannulla {
    host = '5.249.141.51'
    port = 22
    username = 'flussi-nais'
    password = 'XYZrrr655'
   // url = "sftp://flussi-nais@5.249.141.51:22/Libro Matricola Volkswagen/Annullamenti/test"
    // cartella = 'Libro Matricola Volkswagen/Annullamenti/test'
    cartella = 'Libro Matricola Volkswagen/test'
   // options = ["passiveMode=true"]
}
// Added by the Grails Mandrill plugin:
mandrill {
	apiKey = ""
	// insert proxy values if needed
	//proxy {
	//    host = ""
	// The port Value has to be an integer ;)
	//    port = ""
	//}
}

