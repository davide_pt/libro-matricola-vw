class UrlMappings {

	static mappings = {
		"/$controller/$action?/$id?"{ constraints { } }
        "/"(controller: "libroMatricola", action: "index")
        "/login"(controller: 'utenti', action: 'login')
        "/logout"(controller: 'utenti', action: 'logout')
		"/grails"(controller: 'libroMatricola', action: 'grails')
		"500"(view:'/error')
	}
}
