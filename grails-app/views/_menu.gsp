<%@ page import="utenti.*" %>
<g:set var="utente" value="${session.utente}"/>
<ul class="menu">
    <li id="applicazioni"><g:link controller="applicazioni" class="${controllerName == 'applicazioni' ? 'active' : ''}">Applicazioni</g:link></li>
    <g:if test="${utente instanceof Admin}">
        <li id="utenti"><g:link controller="utenti" class="${(controllerName == 'utenti') ? 'active' : ''}">Dealer</g:link></li>
        <li id="tariffe"><g:link controller="tariffe"  class="${(controllerName == 'tariffe')? 'active' : ''}">Tariffe</g:link></li>
        <li id="flussi"><g:link controller="flussi" action="elenco" class="${(controllerName == 'flussi' ) ? 'active' : ''}">Flussi</g:link></li>
        <li id="caricaAnnulla"><g:link controller="annullamenti"  action="annullamenti" class="${(controllerName=='annullamenti') ? 'active' : ''}">Annullamenti da excel</g:link></li>
    </g:if>
    <g:if test="${utente instanceof Admin && utente.isDeveloper}"><li id="sviluppo"><g:link controller="sviluppo" class="${controllerName == 'sviluppo' ? 'active' : ''}">Sviluppo</g:link></li></g:if>
    <li id="logout"><g:link controller="utenti" action="logout">Logout</g:link></li>
</ul>
<r:script>
    $(".menu").buttonset();
    $(".ui-button", $("#logout").prev()).addClass("ui-corner-right");
    $(".ui-button", "#logout").addClass("ui-corner-left");
</r:script>