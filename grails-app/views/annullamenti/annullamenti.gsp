
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Flussi</title>
    <script type="text/javascript">
        window.onload = function() {
            var txtflussi = document.getElementById('listflussi');
            if(txtflussi){
                var arrayflussi = txtflussi.value.toString().split(',').join("\r\n");
                if(document.getElementById('linkflussi') != null){
                    document.getElementById('linkflussi').onclick = function(code) {
                        this.href = 'data:text/plain;charset=utf-8,'
                                + encodeURIComponent(arrayflussi);
                    };
                }
            }
            var txterror = document.getElementById('errorflussi');
            if(txterror){
                var arrayflussiError = txterror.value.toString().split(',').join("\r\n");
                if(document.getElementById('linkError') != null){
                    document.getElementById('linkError').onclick = function(code) {
                        this.href = 'data:text/plain;charset=utf-8,'
                                + encodeURIComponent(arrayflussiError);
                    };
                }
            }
        }
    </script>
    <style>
    .grid-table {
        width: 80%;
        margin: 0 auto;
        table-layout: fixed;
    }
    .short-column { width: 110px; }
    </style>
</head>
<body>
<g:if test="${flash.message}">
    <div class="">
        <input type="hidden" id="listflussi" value="${flash.message}"/>
        <p>Premere il link in basso per  il riassunto dei flussi  caricati:</p>
        <a href="" class="link" id="linkflussi" download="flussicarica.txt">Lista flussi</a><br/>
        %{--<g:each var="messageP" in="${flash.praticheNuove}">
            <p>${messageP}</p>
        </g:each>--}%
    </div>
</g:if>
<g:if test="${flash.error}">
    <div class="">
        <input type="hidden" id="errorflussi" value="${flash.error}"/>
        <p>Premere il link in basso per  il riassunto degli errori generati:</p>
        <a href="" class="link" id="linkError" download="flussiError.txt">Lista errori</a><br/>
        %{--<g:each var="messageP" in="${flash.praticheNuove}">
            <p>${messageP}</p>
        </g:each>--}%
    </div>
</g:if>
<form action="generaFlussoiassicurAnnullaDaExcel" method="post" enctype="multipart/form-data" id="genera-flusso-iassicur-form">
    <h2>Caricamento Annullamenti da Excel</h2>
    <f:field type="file" name="elenco"/>
    <f:submit value="Genera flusso IAssicur da Excel"/>
</form>
%{--<form action="uploadContrassegni" style="margin-top: 15px;" method="post" enctype="multipart/form-data" id="genera-flusso-iassicur-form">

    <f:submit value="Genera Constrassegni"/>
</form>--}%
</body>
</html>