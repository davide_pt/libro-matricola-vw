
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title>Dealer</title>
        <r:external file="/less/utenti.less"/>
    </head>
    <body>
        <div class="clearfix">
            <ui:button action="crea" text="Crea un nuovo dealer"/>
            <f:field type="search" field-id="search" placeholder="cerca"/>
        </div>
        <flash:output/>
        <grid:table data="dealers" paginate="true">
            <grid:column td-class="action-column">
                <ui:link action="modifica" params="${params + [id: value.id]}" class="edit button" title="Modifica $value"/>
                <g:if test="${value.bloccato}"><ui:link action="blocca" params="${params + [id: value.id, bloccato: false]}" class="locked button" title="Sblocca $value"/></g:if>
                <g:else><ui:link action="blocca" params="${params + [id: value.id, bloccato: true]}" class="unlocked button" title="Blocca $value"/></g:else>
            </grid:column>
            <grid:column name="codice" td-class="centered-column short-column" sortable="true"/>
            <grid:column name="tariffaEmissioni" td-class="centered-column short-column" sortable="true"/>
            <grid:column name="tariffaRinnovi" td-class="centered-column short-column" sortable="true"/>
            <grid:column name="numeroLibroMatricola" header="Libro matricola" td-class="centered-column" sortable="true"/>
            <grid:column name="ragioneSociale" sortable="true" td-class="ragsoc-column"/>
            <grid:column name="partitaIva" class="piva-column"/>
            <grid:column name="indirizzoSedeLegale" header="Indirizzo" class="indirizzo-column"/>
            <grid:column name="telefono" class="telefono-column"/>
            <grid:column name="email" td-class="email-column"><g:if test="${value}"><ui:email email="${value}"/></g:if></grid:column>
            <grid:column td-class="centered-column cancella-column"><ui:link action="cancella" params="${params + [id: value.id]}" class="delete button" title="Cancella $value"/></grid:column>
        </grid:table>
    </body>
</html>