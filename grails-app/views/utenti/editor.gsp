
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title><g:if test="${dealer.id}">Modifica</g:if><g:else>Crea nuovo</g:else> dealer</title>
        <r:external file="/less/utenti.less"/>
    </head>
    <body>
        <form action="${createLink(action: 'salva', params: params.findAll { k, v -> k in ['max', 'offset', 'sort', 'order', 'q', 'id'] })}" method="post" id="dealer-form" autocomplete="off">
            <flash:output/>
            <div id="legend">
                <label>(<span>*</span>) campo obbligatorio</label>
            </div>
            <div id="left-form">
                <f:field name="ragioneSociale" type="text" bean="dealer" required="true"/>
                <f:field name="partitaIva" type="text" bean="dealer" required="true"/>
                <f:field name="indirizzoSedeLegale.indirizzo" type="text" bean="dealer" required="true"/>
                <f:field name="indirizzoSedeLegale.citta" bean="dealer" type="text" required="true"/>
                <f:field name="indirizzoSedeLegale.cap" bean="dealer" type="integer" required="true"/>
                <f:field name="indirizzoSedeLegale.provincia.id" bean="dealer" label="Provincia" type="select" from="provincie" required="true"/>
                <f:field name="username" type="text" bean="dealer" required="true"/>
                <f:field name="password" type="password" required="true"/>
            </div>
            <div id="right-form">
                <f:field name="telefono" bean="dealer" type="text"/>
                <f:field name="fax" bean="dealer" type="text"/>
                <f:field name="email" bean="dealer" type="text"/>
                <f:field name="codice" bean="dealer" required="true" type="text"/>
                <f:field name="numeroLibroMatricola" bean="dealer" required="true" type="text"/>
                <f:field name="tariffaEmissioni.id" bean="dealer" label="Tariffa nuove emissioni" type="select" from="tariffe" required="true"/>
                <f:field name="tariffaRinnovi.id" bean="dealer" label="Tariffa rinnovi" type="select" from="tariffe" required="true"/>
            </div>
            <div id="form-footer">
                <ui:button action="elenco" params="${params}" text="Torna all'elenco"/>
                <f:submit value="Salva"/>
            </div>
        </form>
    </body>
</html>