
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title>Sviluppo</title>
        <r:external file="/less/sviluppo.less"/>
        <r:script>
            $("#applicazione").autocomplete({
                minLength: 5,
                source: "${createLink(action: "cercaApplicazione")}"
            });
            $("#rigenera-tracciati-applicazione").submit(function() {
                var data = $(this).serialize();
                $.post("${createLink(action: "rigeneraTracciatiApplicazione")}", data, function(response) { alert(response); });
                return false;
            });
            $("#versione-id").change(function() { $.post("datiVersioneTariffa", {id: this.value}, function(versioneTariffa) {
                $("#modifica-versione").val(versioneTariffa.versione);
                $("#versione-fields").show();
            }).fail(function() { $("#versione-fields").hide(); }); });
            $("#cancella-versione-button").click(function() { alert("Confermi la cancellazione della versione?"); });
        </r:script>
    </head>
    <body>
        <flash:output/>
        <ui:button action="esportazioneJob" text="Trigger EsportazioneJob"/>
        <ui:button action="scadenzaJob" text="Trigger ScadenzaJob"/>
        <form action="rigeneraFlussoAniaDaElenco" method="post" enctype="multipart/form-data" id="rigenera-flusso-ania-form">
            <h2>Rigenerazione flussi ANIA</h2>
            <f:field type="file" name="elenco"/>
            <f:submit value="Rigenera flusso ANIA da elenco"/>
        </form>
        <form action="rigeneraTracciatiApplicazione" method="post" id="rigenera-tracciati-applicazione">
            <h2>Rigenerazione tracciati</h2>
            <f:field type="text" name="id" label="Applicazione" id="applicazione" placeholder="Digita almeno 5 caratteri"/>
            <f:submit value="Rigenera tracciati"/>
        </form>
        <form action="caricaNuovaVersioneTariffa" method="post" enctype="multipart/form-data" id="nuova-versione-tariffa">
            <h2>Nuova versione tariffe</h2>
            <f:field type="file" name="file" required="true"/>
            <f:field type="text" name="versione" required="true"/>
            <f:submit value="Carica nuova versione"/>
        </form>
        <form action="modificaVersioneTariffa" method="post" enctype="multipart/form-data" id="modifica-versione-tariffa">
            <h2>Modifica versione tariffe</h2>
            <f:field type="select" name="id" id="versione-id" from="versioniTariffa" label="Versione" field-class="versione-field"/>
            <div id="versione-fields">
                <f:field type="file" name="file" required="true"/>
                <f:field type="text" name="versione" id="modifica-versione" required="true"/>
                <f:submit value="Modifica versione tariffa"/>
            </div>
        </form>
        <form action="cancellaVersioneTariffa" method="post" id="cancella-versione-tariffa">
            <h2>Cancella versione tariffe</h2>
            <f:field type="select" name="id" id="versione-cancellare-id" from="versioniTariffa" label="Versione" field-class="versione-field"/>
            <f:button type="submit" value="Cancella versione tariffa" icons="${[primary: "ui-icon-trash"]}" id="cancella-versione-button"/>
        </form>
    </body>
</html>