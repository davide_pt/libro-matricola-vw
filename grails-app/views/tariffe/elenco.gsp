<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title>Tariffe</title>
        <r:external file="/less/tariffe.less"/>
    </head>
    <body>
        <flash:output/>
        <table class="grid-table">
            <thead>
                <tr>
                   %{-- <th rowspan="4"></th>--}%
                    <th rowspan="4">Versione</th>
                    <th rowspan="4">Codice</th>
                   %{-- <th rowspan="2" colspan="3">RC</th>--}%
                    <th colspan="30">CVT</th>
                    <th rowspan="2" colspan="2">Kasko</th>
                    <th rowspan="2" colspan="2">Tutela giudiziaria</th>
                    <th rowspan="2" colspan="2">Infortuni</th>
                </tr>
                <tr>
                    <th colspan="28">Autovetture e autocarri</th>
                    <th rowspan="3">Targhe prova</th>
                    <th rowspan="3">Motocicli</th>
                </tr>
                <tr>
                   %{-- <th rowspan="2" class="col-95">Autovetture,<br>autocarri e<br>targhe prova</th>
                    <th rowspan="2">Autovetture<br>e autocarri<br>da noleggio</th>
                    <th rowspan="2">Motocicli</th>--}%
                    <th colspan="4">Zona 1</th>
                    <th colspan="4">Zona 2</th>
                    <th colspan="4">Zona 3</th>
                    <th colspan="4">Zona 4</th>
                    <th colspan="4">Zona 5</th>
                    <th colspan="4">Zona 6</th>
                    <th colspan="4">Zona 7</th>
                    <th rowspan="2" class="col-95">Autovetture,<br>autocarri e<br>targhe prova</th>
                    <th rowspan="2">Motocicli</th>
                    <th rowspan="2" class="col-95">Autovetture,<br>autocarri e<br>targhe prova</th>
                    <th rowspan="2">Motocicli</th>
                    <th rowspan="2">100.000</th>
                    <th rowspan="2">200.000</th>
                </tr>
                <tr>
                    <ui:times n="7">
                        <th>Basso</th>
                        <th>Medio</th>
                        <th>Alto</th>
                        <th>Altro</th>
                    </ui:times>
                </tr>
            </thead>
            <tbody>
                <g:set var="versionIndex" value="${0}"/>
                <g:each var="tariffa" in="${tariffe}" status="index">
                    <tr>
                        %{--<td><ui:link action="modifica" params="${[id: tariffa.id]}" class="edit button" title="Modifica tariffa ${tariffa.codice}"/></td>--}%
                        <g:if test="${index == versionIndex}">
                            <g:set var="versionIndex" value="${versionIndex + tariffa.versione.tariffe.size()}"/>
                            <td rowspan="${tariffa.versione.tariffe.size()}">
                                <g:set var="versione" value="${tariffa.versione}"/>
                                <g:set var="versioneId" value="${versione.id}"/>
                                <a data-versione="${versioneId}" class="versione-trigger">${versione.versione}</a>
                                <div id="versione-${versioneId}" title="Versione #${versioneId}" class="versione-dialog">
                                    <input class="fake">
                                    <f:field type="output" name="versione" bean="versione"/>
                                    <f:field type="output" name="dataCaricamento" bean="versione"/>
                                    <f:field label="Excel">
                                        <a href="${createLink(controller: "versioniTariffa", action: "scaricaExcel", id: versioneId)}" class="download-versione" title="Scarica excel">${versione.fileName}</a>
                                    </f:field>
                                </div>
                            </td>
                        </g:if>
                        <td><b>${tariffa.codice}</b></td>
                        %{--<td>${tariffa.imponibileRCVettureAutocarriTargheProva.format()}</td>
                        <td>${tariffa.imponibileRCVettureAutocarriNoleggio.format()}</td>
                        <td>${tariffa.imponibileRCMoto.format()}</td>--}%
                        <g:each var="zona" in="${1..7}">
                            <g:set var="moltiplicatore" value="${tariffa.moltiplicatoriCVT?.find { it.zona == zona }}"/>
                            <g:each var="rischio" in="${["basso", "medio", "alto", "altro"]}">
                                <td>${moltiplicatore?."$rischio"?.format()}</td>
                            </g:each>
                        </g:each>
                        <td>${tariffa.moltiplicatoreCVTTargheProva.format()}</td>
                        <td>${tariffa.moltiplicatoreCVTMoto.format()}</td>
                        <td>${tariffa.moltiplicatoreKasko.format()}</td>
                        <td>${tariffa.moltiplicatoreCVTKaskoMoto.format()}</td>
                        <td>${tariffa.imponibileTutelaGiudiziaria.format()}</td>
                        <td>${tariffa.imponibileTutelaGiudiziariaMoto.format()}</td>
                        <td>${tariffa.imponibileInfortuni100.format()}</td>
                        <td>${tariffa.imponibileInfortuni200.format()}</td>
                    </tr>
                </g:each>
            </tbody>
        </table>
        <r:script>
            $(".versione-trigger").click(function() {
                var versione = $(this).data("versione");
                $("#versione-" + versione).dialog("open");
            });
            $(".versione-dialog").dialog({
                autoOpen: false,
                modal: true,
                width: 600,
                resizable: false,
                buttons: {"Ok": function() { $(this).dialog("close"); }}
            });
        </r:script>
    </body>
</html>