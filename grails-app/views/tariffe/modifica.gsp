<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title>Modifica tariffa ${tariffa.codice}</title>
        <style>
            html body { min-width: 1280px; }
            fieldset {
                border: solid 1px #DDD;
                border-radius: 10px;
            }
            fieldset:not(:last-child) { margin-bottom: 10px; }
            fieldset .field-label { width: 370px; }
            #cvt table.field-content { vertical-align: top; }
        </style>
    </head>
    <body>
        <form method="post" autocomplete="off">
            <flash:output/>
            <f:field name="codice" bean="tariffa" type="output"/>
            <fieldset id="rc">
                <legend>RC</legend>
                <f:field name="imponibileRCVettureAutocarriTargheProva" bean="tariffa" type="decimal" required="true" label="Imponibile autovetture, autocarri e targhe prova"/>
                <f:field name="imponibileRCVettureAutocarriNoleggio" bean="tariffa" type="decimal" required="true" label="Imponibile autovetture e autocarri da noleggio"/>
                <f:field name="imponibileRCMoto" bean="tariffa" type="decimal" required="true" label="Imponibile motocicli"/>
            </fieldset>
            <fieldset id="cvt">
                <legend>CVT</legend>
                <f:field type="custom" label="Moltiplicatori autoveicoli e autocarri" required="true">
                    <table class="field-content">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Basso</th>
                            <th>Medio</th>
                            <th>Alto</th>
                            <th>Altro</th>
                        </tr>
                        </thead>
                        <tbody>
                        <g:each var="zona" in="${1..7}" status="i">
                            <g:set var="moltiplicatore" value="${tariffa.moltiplicatoriCVT.find { it.zona == zona }}"/>
                            <tr>
                                <th>Zona ${zona}<input type="hidden" name="moltiplicatori[${i}].id" value="${moltiplicatore?.id}"></th>
                                <g:each var="rischio" in="${["basso", "medio", "alto", "altro"]}">
                                    <g:if test="${moltiplicatore?."$rischio" < 0}">
                                        <td class="field error" title="<ui:list data="${["Non può contenere un valore negativo"]}" class="error-list" li-class="error"/>">
                                            <f:decimal name="moltiplicatori[$i].$rischio" value="${moltiplicatore?."$rischio"?.format()}" class="field-content ui-widget-content ui-corner-all"/>
                                        </td>
                                    </g:if>
                                    <g:else>
                                        <td class="field"><f:decimal name="moltiplicatori[$i].$rischio" value="${moltiplicatore?."$rischio"?.format()}" class="field-content ui-widget-content ui-corner-all"/></td>
                                    </g:else>
                                </g:each>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </f:field>
                <f:field name="moltiplicatoreCVTTargheProva" bean="tariffa" type="decimal" required="true" label="Moltiplicatore targhe prova"/>
                <f:field name="moltiplicatoreCVTMoto" bean="tariffa" type="decimal" required="true" label="Moltiplicatore motocicli"/>
            </fieldset>
            <fieldset id="kasko">
                <legend>Kasko</legend>
                <f:field name="moltiplicatoreKasko" bean="tariffa" type="decimal" required="true" label="Moltiplicatore autoveicolo, autocarri e targhe prova"/>
                <f:field name="moltiplicatoreCVTKaskoMoto" bean="tariffa" type="decimal" required="true" label="Moltiplicatore motocicli"/>
            </fieldset>
            <fieldset id="tutela">
                <legend>Tutela giudiziaria</legend>
                <f:field name="imponibileTutelaGiudiziaria" bean="tariffa" type="decimal" required="true" label="Imponibile autoveicolo, autocarri e targhe prova"/>
                <f:field name="imponibileTutelaGiudiziariaMoto" bean="tariffa" type="decimal" required="true" label="Imponibile motocicli"/>
            </fieldset>
            <fieldset id="infortuni">
                <legend>Infortuni</legend>
                <f:field name="imponibileInfortuni100" bean="tariffa" type="decimal" required="true" label="Imponibile 100.000"/>
                <f:field name="imponibileInfortuni200" bean="tariffa" type="decimal" required="true" label="Imponibile 200.000"/>
            </fieldset>
            <ui:button action="elenco" text="Torna all'elenco" id="back-button"/>
            <f:submit name="submit" id="salva" value="Salva"/>
        </form>
    </body>
</html>