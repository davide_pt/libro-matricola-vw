
<table class="grid-table" id="tracciato-scomposto">
    <thead>
        <tr>
            <th class="campo-column">Campo</th>
            <th class="valore-column">Valore</th>
            <th>Lunghezza</th>
        </tr>
    </thead>
    <tbody>
        <g:each var="entry" in="${scomposto}">
            <tr>
                <td>${entry.key}</td>
                <td>${entry.value}</td>
                <td>${entry.value.size()}</td>
            </tr>
        </g:each>
    </tbody>
</table>