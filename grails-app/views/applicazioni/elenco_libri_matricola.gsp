
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title>Libri Matricola</title>
        <r:external file="/less/applicazioni.less"/>
        <r:script>
            $("tr:not(.empty-table-row)", "#libri-matricola tbody").click(function() {
                var id = $(".dealer-column input[type=hidden][name=id]", this).val();
                document.location.href = "${createLink(controller: 'applicazioni', action: 'elenco')}/" + id;
            });
            $(".stampa-certificato").click(function(event) {
                event.preventDefault();
                event.stopPropagation();
            });
            $("#filter-toggle").click(function() { $("#form-estrazione").dialog("open"); });
            $(".garanzia-check").click(function(event) { if(!this.checked && $(".garanzia-check:checked").length < 1) event.preventDefault(); });
            $("#form-estrazione").dialog({
                autoOpen: false,
                closeOnEscape: false,
                modal: true,
                resizable: false,
                width: 700,
                create: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog).hide(); },
                open: function(event, ui) {
                    $("#stato", ui.dialog).select2("val", "null");
                    $(".garanzia-check", ui.dialog).prop("checked", true);
                    //$("#pagamentoFrom, #pagamentoTo, #premio-from, #premio-to", ui.dialog).val("");
                    $("#decorrenzaFrom, #decorrenzaTo, #scadenzaFrom, #scadenzaTo, #esclusioneFrom, #esclusioneTo, #premio-from, #premio-to", ui.dialog).val("");
                },
                buttons: [
                    {text: "Annulla", icons: {primary: "ui-icon-close"}, click: function() { $(this).dialog("close"); }},
                    {text: "Estrai", icons: {secondary: "ui-icon-check"}, click: function() { $(this).submit().dialog("close"); }}
                ]
            });
            $("#form-certificato").dialog({
                autoOpen: false,
                closeOnEscape: false,
                modal: true,
                resizable: false,
                width: 460,
                create: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog).hide(); },
                open: function(event, ui) { $("#split", ui.dialog).prop("checked", true); },
                buttons: [
                    {text: "Annulla", icons: {primary: "ui-icon-close"}, click: function() { $(this).dialog("close"); }},
                    {text: "Stampa", icons: {primary: "ui-icon-print"}, click: function() { $(this).submit().dialog("close"); }}
                ]
            });
            $("#form-preventivi").dialog({
                autoOpen: false,
                closeOnEscape: false,
                modal: true,
                resizable: false,
                width: 460,
                create: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog).hide(); },
                buttons: [
                    {text: "Annulla", icons: {primary: "ui-icon-close"}, click: function() { $(this).dialog("close"); }},
                    {text: "Stampa", icons: {primary: "ui-icon-print"}, click: function() { $(this).submit().dialog("close"); }}
                ]
            });
            var stampaCertificato = function(id) {
                $.post("${createLink(action: "pagamenti")}", {id: id}, function(pagamenti) {
                    var form = $("#form-certificato");
                    $(".data-pagamento", form).datepicker("option", {minDate: pagamenti.min, maxDate: pagamenti.max});
                    $("#from", form).datepicker("setDate", pagamenti.min);
                    $("#to", form).datepicker("setDate", pagamenti.max);
                    form.attr("action", "certificato/" + id).dialog("open");
                });
            };
            $(".stampa-preventivi").click(function(event) {
                event.preventDefault()
                event.stopPropagation();
            });
            var stampaPreventivi = function(id) {
                $.post("${createLink(action: "datePreventivi")}", {id: id}, function(date) {
                    var form = $("#form-preventivi");
                    $(".data-pagamento", form).datepicker("option", {minDate: date.min, maxDate: date.max});
                    $("#preventiviFrom").datepicker("setDate", date.min);
                    $("#preventiviTo").datepicker("setDate", date.max);
                    form.attr("action", "preventivi/" + id).dialog("open");
                });
            };
            $(".pagaTutti").click(function(event) {
                event.preventDefault()
                event.stopPropagation();
            });
            var pagaTutti = function(id) {
                $.post("${createLink(action: "pagaTutte")}", {id: id}, function(response) {
                    var risposta=response.errore;
                    if(risposta.length !=0 ){
                        $( ".labelErrore" ).remove();
                        $(".messaggio").after("<div class=\"labelErrore flash ui-corner-all error\"><span class='testoAlert'></span></div>");
                        $( ".testoAlert").append(risposta).append("<br>");
                    }else{
                        $( ".labelErrore" ).remove();
                        $(".messaggio").after("<div class=\"labelErrore flash ui-corner-all message\"><span class='testoAlert'></span></div>");
                        $( ".testoAlert").append("tutte le applicazione sono state pagate").append("<br>");
                    }
                });
            };
        </r:script>
    </head>
    <body>
        <div class="clearfix">
            <ui:button action="crea" text="Nuova applicazione"/>
            <ui:button text="Estrazione" id="filter-toggle"/>
            <f:field type="search" field-id="search" placeholder="cerca"/>
        </div>
        <div class="messaggio "></div>
        <grid:table data="libriMatricola" paginate="true" id="libri-matricola">
            <grid:column class="action-column" td-class="count-column giustified-column">
                <ui:link href="#" class="doc contrassegno button stampa-certificato" onclick="stampaCertificato(${value.dealer.id})" title="Certifiato emissioni ${value.dealer}" disabled="${value.polizze == 0 || value.pagato == 0}"/>
                <ui:link class="doc preventivo button stampa-preventivi" onclick="stampaPreventivi(${value.dealer.id})" title="Preventivi ${value.dealer}" disabled="${value.preventivi == 0}"/>
                <ui:link class="euro button pagaTutti" onclick="pagaTutti(${value.dealer.id})" title="Pagamento tutte app. per il dealer  ${value.dealer}"/>
            </grid:column>
            <grid:column name="dealer.numeroLibroMatricola" header="Libro matricola" sortable="true" class="numero-column centered-column"/>
            <grid:column name="dealer" sortable="true" td-class="dealer-column"><input type="hidden" name="id" value="${value.id}">${value}</grid:column>
            <grid:column name="preventivi" td-class="centered-column" class="count-column" emptyMessage="0"/>
            <grid:column name="polizze" td-class="centered-column" class="count-column" emptyMessage="0"/>
            <grid:column name="totale" td-class="centered-column" class="totale-column" emptyMessage="0,00"/>
            <grid:column name="pagato" td-class="centered-column" class="totale-column" emptyMessage="0,00"/>
        </grid:table>
        <form id="form-estrazione" action="estrazione" method="post" title="Estrazione excel">
            <input class="fake">
            <f:field type="select" name="stato" from="${filter.statiApplicazione}" first="Qualsiasi" enhanced="true"/>
            <div class="range">
                <f:field type="date" name="decorrenzaFrom" minDate="${filter.decorrenzaMin}" maxDate="${filter.decorrenzaMax}" label="Data decorrenza polizza da"/>
                <f:field type="date" name="decorrenzaTo" minDate="${filter.decorrenzaMin}" maxDate="${filter.decorrenzaMax}" label="a"/>
            </div>
            <div class="range">
                <f:field type="date" name="esclusioneFrom" minDate="${filter.esclusioneMin}" maxDate="${filter.esclusioneMax}" label="Data esclusione polizza da"/>
                <f:field type="date" name="esclusioneTo" minDate="${filter.esclusioneMin}" maxDate="${filter.esclusioneMax}" label="a"/>
            </div>
            <div class="range">
                <f:field type="date" name="scadenzaFrom" minDate="${filter.scadenzaMin}" maxDate="${filter.scadenzaMax}" label="Data scadenza polizza da"/>
                <f:field type="date" name="scadenzaTo" minDate="${filter.scadenzaMin}" maxDate="${filter.scadenzaMax}" label="a"/>
            </div>
            <f:field type="custom" label="Garanzie">
                <div class="field-content garanzie">
                    <label class="garanzia"><input type="checkbox" name="garanzie" value="cvt" checked class="garanzia-check"> CVT</label>
                    <label class="garanzia"><input type="checkbox" name="garanzie" value="k" checked class="garanzia-check"> Kasko</label>
                    <label class="garanzia"><input type="checkbox" name="garanzie" value="rc" checked class="garanzia-check"> RC</label>
                    <label class="garanzia"><input type="checkbox" name="garanzie" value="tg" checked class="garanzia-check"> Tutela giudiziaria</label>
                    <label class="garanzia"><input type="checkbox" name="garanzie" value="i100" checked class="garanzia-check"> Infortuni 100.000</label>
                    <label class="garanzia"><input type="checkbox" name="garanzie" value="i200" checked class="garanzia-check"> Infortuni 200.000</label>
                </div>
            </f:field>
            <f:field type="checkbox" name="riattivazioni"/>
            %{--<div class="range">
                <f:field type="currency" name="premioFrom" label="Premio cliente da" id="premio-from" allowBlank="true"/>
                <f:field type="currency" name="premioTo" label="a" id="premio-to" allowBlank="true"/>
            </div>--}%
        </form>
        <form id="form-certificato" method="post" title="Certificato emissioni">
            Applicazioni pagate
            <input class="fake">
            <f:field type="date" name="from" label="dal" minDate="01-12-2013" value="01-01-2014" maxDate="30-12-2014" class="data-pagamento" changeMonth="true"/>
            <f:field type="date" name="to" label="al" minDate="01-01-2014" value="31-12-2014" maxDate="31-12-2014" class="data-pagamento" changeMonth="true"/>
            <f:field type="checkbox" name="split" label="Totali divisi per pagina" checked="checked"/>
        </form>
        <form id="form-preventivi" method="post" title="Preventivi">
            Preventivi validi
            <input class="fake">
            <g:set var="today" value="${new Date()}"/>
            <f:field type="date" name="preventiviFrom" label="dal" value="${today.format()}" class="data-pagamento" changeMonth="true"/>
            <f:field type="date" name="preventiviTo" label="al" value="${today.format()}" class="data-pagamento" changeMonth="true"/>
        </form>

    </body>
</html>