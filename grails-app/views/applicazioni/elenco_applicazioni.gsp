
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="utenti.*" %>
<%@ page import="polizze.*" %>
<g:set var="utente" value="${session.utente}"/>
<g:set var="paramsElenco" value="${params.findAll { k, v -> k in ['max', 'offset', 'sort', 'order', 'q', 'id'] }}"/>
<g:set var="today" value="${new Date().clearTime()}"/>
<html>
<head>
    <title>Applicazioni</title>
    <r:external file="/less/applicazioni.less"/>
    <r:script>
            $(".pagata-check").change(function() {
                var id = this.value, row = $(this).parents("tr");
                $.post("${createLink(action: 'paga')}", {id: id}, function(result) {
                    if(result.error) alert(result.error);
                    else $(".pagata-il-column", row).text(result.pagataIl);
                });
            });
            $("#filter-toggle").click(function() { $("#form-estrazione").dialog("open"); });
            $(".garanzia-check").click(function(event) { if(!this.checked && $(".garanzia-check:checked").length < 1) event.preventDefault(); });
            $(".escludi-applicazione-button").click(function(event) {
                event.preventDefault();
                var a = $(this);
                var app = a.data("app");
                var daten=document.getElementById(app).value;
                var pagato=document.getElementById('pagata' + app).checked;
                document.getElementById("dEsclu").value = daten;
                document.getElementById("pagata").value = pagato;
                /*if(daten !=''){

                    $.post("${createLink(action: "appenaEmessa")}", {id: app}, function(result) {*/
                        var form = $("#form-esclusione").find("#applicazione").val(app).end();
                        /*if(result.check) form.submit();
                        else {*/
                            var title = "Escludi applicazione " + a.data("numero");
                            form.dialog("option", "title", title).dialog("open");
                        /*}
                    });
                }else{
                    $('#finestra').dialog("open");
                }*/
            });
            $("#form-estrazione").dialog({
                autoOpen: false,
                closeOnEscape: false,
                modal: true,
                resizable: false,
                width: 700,
                create: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog).hide(); },
                open: function(event, ui) {
                    $("#stato", ui.dialog).select2("val", "null");
                    $(".garanzia-check", ui.dialog).prop("checked", true);
                   /* $("#pagamentoFrom, #pagamentoTo, #premio-from, #premio-to", ui.dialog).val("");*/
                    $("#decorrenzaFrom, #decorrenzaTo, #scadenzaFrom, #scadenzaTo, #esclusioneFrom, #esclusioneTo, #premio-from, #premio-to", ui.dialog).val("");
                },
                buttons: [
                    {text: "Annulla", icons: {primary: "ui-icon-close"}, click: function() { $(this).dialog("close"); }},
                    {text: "Estrai", icons: {secondary: "ui-icon-check"}, click: function() { $(this).submit().dialog("close"); }}
                ]
            });
            $("#form-esclusione").dialog({
                autoOpen: false,
                closeOnEscape: false,
                modal: true,
                resizable: false,
                width: 500,
                create: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog).hide(); },
                buttons: [
                    {text: "Annulla", icons: {primary: "ui-icon-close"}, click: function() { $(this).dialog("close"); }},
                    {text: "Escludi", icons: {secondary: "ui-icon-check"}, click: function() { $(this).submit().dialog("close"); }}
                ]
            });
            $('#finestra').dialog({
                modal: true,
                autoOpen: false,
                buttons: {
                    "Ok": function() {
                        $( this ).dialog( "close" );
                        }
                    }
            });
    </r:script>
</head>
<body>
<div class="clearfix">
    <g:if test="${utente instanceof Admin && dealer}"><ui:button action="elenco" text="Torna all'elenco"/></g:if>
    <ui:button action="crea" params="${[id: params.id]}" text="Nuova applicazione"/>
    <ui:button text="Estrazione" id="filter-toggle"/>
    <f:field type="search" field-id="search" placeholder="cerca"/>
</div>
<flash:output/>
<grid:table data="applicazioni" paginate="true" id="applicazioni">
    <g:if test="${utente instanceof Admin && dealer}"><grid:caption>Applicazioni libro matricola <strong>${dealer.numeroLibroMatricola}</strong>: <strong>${dealer}</strong></grid:caption></g:if>
    <grid:column class="action-column${utente instanceof Admin ? ' admin' : ''}">
        <g:if test="${value.stato == StatoApplicazione.POLIZZA && value.rc == ProdottoCheck.SI}"><ui:link action="contrassegno" params="${[id: value.id]}" class="doc button" title="Stampa contrassegno ${value.numero}" target="_blank"/></g:if>
        <g:else><span class="doc button ui-state-disabled"></span></g:else>
        <ui:link action="mostra" params="${paramsElenco + [app: value.id]}" class="view button" title="Dettagli ${value.numero}"/>
        <ui:link action="modifica" params="${paramsElenco + [app: value.id]}" class="edit button" title="Modifica applicazione"/>
        <g:if test="${utente instanceof Admin}">
            <g:if test="${value.stato == StatoApplicazione.PREVENTIVO}"><ui:link action="emetti" params="${paramsElenco + [app: value.id]}" class="emetti button" title="Emetti l'applicazione"/></g:if>
            <g:else><span class="ui-state-disabled"><span class="emetti button ui-state-disabled" title="L'applicazione è gia stata emessa"></span></span></g:else>
            <g:if test="${value.stato in [StatoApplicazione.POLIZZA, StatoApplicazione.SCADUTA]/* && Applicazione.countByApplicazioneRinnovataAndStato(value, StatoApplicazione.POLIZZA) == 0*/}"><ui:link action="rinnova" params="${paramsElenco + [app: value.id]}" class="rinnova button" title="Rinnova applicazione"/></g:if>
            <g:else><span class="ui-state-disabled"><span class="rinnova button" title="L'applicazione non è rinnovabile"></span></span></g:else>
        </g:if>
    </grid:column>
    <grid:column header="€" class="centered-column pagata-column" th-title="Pagata">
        <g:if test="${value.stato != polizze.StatoApplicazione.PREVENTIVO}"><input type="checkbox" value="${value.id}" ${value.pagata ? 'checked' : ''} class="pagata-check" id="pagata${value.id}"></g:if>
        <g:else><input type="checkbox" disabled class="ui-state-disabled"></g:else>
    </grid:column>
    <grid:column name="numero" sortable="true" class="centered-column numero-column"/>
    <grid:column name="stato" sortable="true" class="stato-column">${value == StatoApplicazione.PREVENTIVO ? "<span class='preventivo'>$value</span>" : value == StatoApplicazione.POLIZZA ? "<span class='polizza'>$value</span>" : "<span class='esclusa'>$value</span>"}</grid:column>
    <grid:column name="dataPagamento" header="Pagata il" class="centered-column pagata-il-column" emptyMessage="Non ancora pagata"/>
    <grid:column name="annoValidita" sortable="true" header="Anno" class="centered-column"/>
    <grid:column header="Validità" class="centered-column validita-column">
        <g:if test="${value.stato == StatoApplicazione.POLIZZA && today >= value.dataDecorrenza}">
            <%
                def decorrenza = value.dataDecorrenza, scadenza = value.dataScadenza
                def durata = scadenza - decorrenza
                def progresso = (((today - decorrenza) / durata) * 100).round(2)
            %>
            <div id="validita-${value.id}" class="validita-applicazione" title="<b>decorrenza:</b> ${decorrenza.format()}<br><b>scadenza:</b> ${scadenza.format()}<br><b>${today - decorrenza}</b> giorni dalla decorrenza<br><b>${scadenza - today}</b> giorni alla scadenza"></div>
            <r:script>$("#validita-${value.id}").progressbar({value: ${progresso}});</r:script>
        </g:if><g:elseif test="${value.stato == StatoApplicazione.POLIZZA && today < value.dataDecorrenza}">Decorrerà dal ${value.dataDecorrenza.format()}</g:elseif>
        <g:elseif test="${value.stato == StatoApplicazione.PREVENTIVO}">Non ancora emessa</g:elseif>
        <g:elseif test="${value.stato == StatoApplicazione.SCADUTA}">
            <div id="validita-${value.id}" class="validita-applicazione scaduta" title="Applicazione scaduta il <b>${value.dataScadenza.format()}</b>"></div>
            <r:script>$("#validita-${value.id}").progressbar({value: false});</r:script>
        </g:elseif><g:else>Esclusa il ${value.dataEsclusione.format()}</g:else>
    </grid:column>
    <grid:column header="Tipo" sortable="true" class="tipologia-column">
        <%
            if(value.targaProva) out << "(P) "
            out << value.tipologiaVeicolo
        %>
    </grid:column>
    <grid:column name="marca" sortable="true" class="veicolo-column"/>
    <grid:column name="modello" sortable="true" class="veicolo-column"/>
    <grid:column name="targa" sortable="true" class="centered-column targa-column"/>
    <grid:column name="valoreAssicurato" header="Valore" td-class="right-align-column" class="valore-column">${value} €</grid:column>
    <grid:column header="CVT" td-class="centered-column" class="prodotto-column"><span class="${value.cvt == ProdottoCheck.NO ? 'without-product' : 'with-product'}">${value.calcoli["cvt"]?.pcl?.format() ?: 0.00} €</span></grid:column>
    <grid:column header="Kasko" td-class="centered-column" class="prodotto-column"><span class="${value.kasko == ProdottoCheck.NO ? 'without-product' : 'with-product'}">${value.calcoli["kasko"]?.pcl?.format() ?: 0.00} €</span></grid:column>
    <grid:column header="RC" td-class="centered-column" class="prodotto-column"><span class="${value.rc == ProdottoCheck.NO ? 'without-product' : 'with-product'}">${value.calcoli["rc"]?.pcl?.format() ?: 0.00} €</span></grid:column>
    <grid:column header="Tutela" td-class="centered-column" class="prodotto-column"><span class="${value.tutela == ProdottoCheck.NO ? 'without-product' : 'with-product'}">${value.calcoli["tutela giudiziaria"]?.pcl?.format() ?: 0.00} €</span></grid:column>
    <grid:column header="Infortuni" td-class="centered-column" class="prodotto-column"><span class="${value.infortuni == ProdottoCheck.NO ? 'without-product' : 'with-product'}">${value.calcoli["infortuni 100.000"]?.pcl?.format() ?: value.calcoli["infortuni 200.000"]?.pcl?.format() ?: 0.00} €</span></grid:column>
    <grid:column name="pcl" header="Premio cliente" td-class="right-align-column" class="premio-column">${value} €</grid:column>
    <grid:column name="premiorimborso" header="Premio rimborso" td-class="right-align-column" class="premio-column">${value} €</grid:column>
    <grid:column name="note" td-class="left-align-column">${value.rightPad(10).replaceAll(/\n/, "<br>")}</grid:column>
    <grid:column header="Esclusa il" class="centered-column validita-il-column">
        <g:if test="${value.stato == polizze.StatoApplicazione.PREVENTIVO || value.stato == polizze.StatoApplicazione.POLIZZA }">
            <f:field type="date"  bean="applicazione" required="false" class="dataesclusion${value.id}" id="${value.id}"/>
        </g:if>
        <g:elseif test="${value.stato == polizze.StatoApplicazione.SCADUTA }"><span title="L'applicazione ${value.numero} è scaduta">Scaduta</span></g:elseif>
        %{--<g:elseif test="${polizze.Applicazione.countByApplicazioneEsclusa(value) > 0}"><span title="L'applicazione ${value.numero} è gia stata riattivata"><span class="refresh button ui-state-disabled"></span></span></g:elseif>--}%
        <g:else><span>${value.dataEsclusione?.format("dd-MM-yyyy")}</span> </g:else>
    </grid:column>
    <grid:column td-class="delete-column centered-column">
        <g:if test="${value.stato == polizze.StatoApplicazione.PREVENTIVO}"><ui:link action="cancella" params="${paramsElenco + [app: value.id, motivoEsclusione:"preventivo"] }" class="delete button" title="Cancella preventivo" onclick="return confirm('Confermi la cancellazione del preventivo ${value.numero}');"/></g:if>
        <g:elseif test="${value.stato == polizze.StatoApplicazione.POLIZZA}">
        <%--<ui:link action="cancella" params="${paramsElenco + [app: value.id]}" class="delete button" title="Escludi applicazione" onclick="return confirm('Confermi la cancellazione della polizza ${value.numero}');"/>--%>
            <a href="" class="delete button escludi-applicazione-button" title="Escludi applicazione" data-app="${value.id}" data-numero="${value.numero}"></a>
        </g:elseif>
        %{--<g:elseif test="${polizze.Applicazione.countByApplicazioneEsclusa(value) > 0}"><span title="L'applicazione ${value.numero} è gia stata riattivata"><span class="refresh button ui-state-disabled"></span></span></g:elseif>--}%
        <g:else><ui:link action="riattiva" params="${paramsElenco + [app: value.id]}" class="refresh button" title="Riattiva applicazione"/></g:else>
    </grid:column>
</grid:table>
<form id="form-estrazione" action="${createLink(action: 'estrazione', id: dealer.id)}" method="post" title="Estrazione excel">
    <input class="fake">
    <input type="hidden" name="dealer.id" value="${dealer.id}">
    <f:field type="select" name="stato" from="${filter.statiApplicazione}" first="Qualsiasi" enhanced="true"/>
    <g:if test="${filter.decorrenzaMin && filter.decorrenzaMax}">
       <div class="range">
            <f:field type="date" name="decorrenzaFrom" minDate="${filter.decorrenzaMin}" maxDate="${filter.decorrenzaMax}" label="Data decorrenza polizza da"/>
            <f:field type="date" name="decorrenzaTo" minDate="${filter.decorrenzaMin}" maxDate="${filter.decorrenzaMax}" label="a"/>
        </div>
    </g:if>
    <g:if test="${filter.esclusioneMin && filter.esclusioneMax}">
        <div class="range">
            <f:field type="date" name="esclusioneFrom" minDate="${filter.esclusioneMin}" maxDate="${filter.esclusioneMax}" label="Data esclusione polizza da"/>
            <f:field type="date" name="esclusioneTo" minDate="${filter.esclusioneMin}" maxDate="${filter.esclusioneMax}" label="a"/>
        </div>
    </g:if>
    <g:if test="${filter.scadenzaMin && filter.scadenzaMax}">
        <div class="range">
            <f:field type="date" name="scadenzaFrom" minDate="${filter.scadenzaMin}" maxDate="${filter.scadenzaMax}" label="Data scadenza polizza da"/>
            <f:field type="date" name="scadenzaTo" minDate="${filter.scadenzaMin}" maxDate="${filter.scadenzaMax}" label="a"/>
        </div>
    </g:if>

    <f:field type="custom" label="Garanzie">
        <div class="field-content garanzie">
            <label class="garanzia"><input type="checkbox" name="garanzie" value="cvt" checked class="garanzia-check"> CVT</label>
            <label class="garanzia"><input type="checkbox" name="garanzie" value="k" checked class="garanzia-check"> Kasko</label>
            <label class="garanzia"><input type="checkbox" name="garanzie" value="rc" checked class="garanzia-check"> RC</label>
            <label class="garanzia"><input type="checkbox" name="garanzie" value="tg" checked class="garanzia-check"> Tutela giudiziaria</label>
            <label class="garanzia"><input type="checkbox" name="garanzie" value="i100" checked class="garanzia-check"> Infortuni 100.000</label>
            <label class="garanzia"><input type="checkbox" name="garanzie" value="i200" checked class="garanzia-check"> Infortuni 200.000</label>
        </div>
    </f:field>
    %{--<div class="range">
        <f:field type="currency" name="premioFrom" label="Premio cliente da" id="premio-from" allowBlank="true"/>
        <f:field type="currency" name="premioTo" label="a" id="premio-to" allowBlank="true"/>
    </div>--}%
</form>
<form id="form-esclusione" action="${createLink(action: "cancella")}" method="post">
    <g:each var="param" in="${paramsElenco}"><input type="hidden" name="${param.key}" value="${param.value}"></g:each>
    <input type="hidden" name="app" id="applicazione">
    <input type="hidden" name="dEsclu" id="dEsclu">
    <input type="hidden" name="pagata" id="pagata">
    <f:field type="custom" label="Codice esclusione">
        <div class="field-content codici">
            <label class="codice"><input type="radio" name="codice" value="002"> 002</label>
            <label class="codice"><input type="radio" name="codice" value="006" checked> 006</label>
        </div>
    </f:field>
    <f:field type="custom" label="Motivo esclusione">
        <div class="field-content esclusione">
            <label class="esclusione"><input type="text" name="motivoEsclusione" /> </label>
        </div>
    </f:field>
    <f:field type="custom" label="oppure">
        <div class="field-content codici">
            <label class="codice"><input type="radio" name="codice" value="-1"> cancella definitivamente</label>
        </div>
    </f:field>
</form>
<div id="finestra" title="Errore">
    <p>Manca la data di esclusione</p>
</div>
</body>
</html>