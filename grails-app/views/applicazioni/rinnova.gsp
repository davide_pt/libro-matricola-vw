
<%@ page contentType="text/html;charset=UTF-8" %>
<g:set var="paramsElenco" value="${params.findAll { k, v -> k in ['max', 'offset', 'sort', 'order', 'q', 'id'] }}"/>
<g:set var="isPolizza" value="${applicazione.stato == polizze.StatoApplicazione.POLIZZA}"/>
<g:set var="isPagata" value="${applicazione.pagata}"/>
<g:set var="isEsclusa" value="${applicazione.stato == polizze.StatoApplicazione.ESCLUSA}"/>
<g:set var="altroVeicolo" value="${libromatricola.Veicolo.findByMarca("Altro")?.id}"/>
<html>
    <head>
        <title>${applicazione.id ? "Modifica $applicazione.numero" : 'Rinnovo'}</title>
        <r:external file="/less/applicazioni.less"/>
        <r:script>
                    $("#cvt").change(function() { $("#k").attr("disabled", !this.checked).attr("checked", this.checked ? $("#k").is("checked") : false); });
                    $("#i100").change(function() { if(this.checked) $("#i200:checked").attr("checked", false); });
                    $("#i200").change(function() { if(this.checked) $("#i100:checked").attr("checked", false); });
                    $("#dealer-id").change(function() { $.post("${createLink(action: 'dealer')}", {id: this.value}, function(response) {
                        $("#tariffa-id").val(response.tariffaRinnovi).change();
                        $("#provincia").text(response.provincia);
                        $("#numero").text(response.numero);
                    }); });
                    $("#tipologiaVeicolo").change(function() {
                    var valore=this.value;
                    if(valore.startsWith('AUTOCARRO')){
                        $("#marca, #modello").prop("readonly", false);
                        $("#quintali").show();
                        $("#veicolo").select2("readonly", false);
                        $('#destinazioneUso').select2('data', {id:1, text:'Proprio'},{id:2, text:'Noleggio'}).removeAttr('disabled');
                    }
                    else if(valore.startsWith('TARGAPROVA')){
                        $('#marca, #modello').attr('readonly',true);
                        $('#destinazioneUso').select2('data', {id:1, text:'Targa prova'}).attr('disabled','disabled');
                        $("#quintali").hide();
                        $("#veicolo").val("${altroVeicolo}").select2("readonly", true);
                    }else if(valore.startsWith('MOTO')){
                        $("#marca, #modello").val("").prop("readonly", false);
                        $("#quintali").hide();
                        $("#veicolo").val("null").change().select2("readonly", false);
                        $('#destinazioneUso').select2('data', {id:1, text:'Proprio'},{id:2, text:'Noleggio'}).removeAttr('disabled');
                    }
                    else{
                        $("#quintali").hide();
                        $("#marca, #modello").prop("readonly", false);
                         $("#veicolo").val("null").change().select2("readonly", false);
                         $('#destinazioneUso').select2('data', {id:1, text:'Proprio'},{id:2, text:'Noleggio'}).removeAttr('disabled');
                    }
                });
                    $("#targaProva").change(function() {
                        if(this.checked) {
                            $("#veicolo").val("${altroVeicolo}").change().select2("readonly", true);
                            $("#marca, #modello").val("").prop("readonly", true);
                        } else {
                            $("#veicolo").val("null").change().select2("readonly", false);
                            $("#marca, #modello").prop("readonly", false);
                        }
                    });
            <g:if test="${applicazione.tipologiaVeicolo?.name()?.startsWith('AUTOCARRO')}">$("#quintali").show();</g:if>
            $("#veicolo").change(function() { $.post("${createLink(action: 'veicolo')}", {id: this.value, targaProva: $("#targaProva").is(":checked")}, function(response) { $("#marca-modello").html(response); }); })<%--<g:if test="${!applicazione.marca}">.change()</g:if>--%>;
            <g:if test="${applicazione.calcoli}">
                $(":input", "#applicazione-form").change(function() { $("#calcola").click(); });
            </g:if>
        </r:script>
    </head>
    <body>
        <form action="${createLink(action: 'salva', params: paramsElenco + [app: params.app, op: "rinnova"])}" method="post" id="applicazione">
            <flash:output/>
            <div id="legend">
                <label>(<span>*</span>) campo obbligatorio</label>
            </div>
            <div id="left">
                <f:field type="hidden" name="applicazioneRinnovata.id" bean="applicazione"/>
                <g:if test="${session.utente instanceof utenti.Admin}">
                    <g:if test="${dealers}">
                        <f:field type="select" name="dealer.id" bean="applicazione" label="Dealer" from="dealers" optionKey="id" optionValue="ragioneSociale" required="true"/>
                        <f:field type="output" name="dealer.numeroLibroMatricola" label="Numero libro matricola" bean="applicazione" id="numero" value="Scegli un dealer"/>
                        <f:field type="select" name="tariffa.id" bean="applicazione" label="Tariffa" from="tariffe" required="true"/>
                        <f:field type="output" name="provincia" bean="applicazione" value="Scegli un dealer"/>
                    </g:if><g:else>
                    <f:field type="output" label="Dealer" value="${dealer}"/>
                    <f:field type="output" label="Numero libro matricola" value="${dealer.numeroLibroMatricola}"/>
                    <f:field type="select" name="tariffa.id" bean="applicazione" label="Tariffa" from="tariffe" required="true"/>
                    <f:field type="output" name="provincia" bean="applicazione"/>
                </g:else>
                </g:if>
                <f:field type="select" name="tipologiaVeicolo" bean="applicazione" from="tipologieVeicolo" first="false"/>
                <f:field type="select" name="destinazioneUso"  bean="applicazione" from="destinazioniUso"   required="true" first="false"/>
                %{--<f:field type="checkbox" name="targaProva" bean="applicazione"/>--}%
                <f:field type="custom" name="prodotti" required="true">
                    <ul id="prodotti">
                        <g:set var="p" value="${prodotti ?: params.list('prodotti')}"/>
                        <li><input type="checkbox" name="prodotti" value="cvt" id="cvt" ${p.contains('cvt') ? 'checked' : ''}><label for="cvt">CVT</label></li>
                        <li><input type="checkbox" name="prodotti" value="kasko" id="k" ${p.contains('cvt') ? p.contains('kasko') ? 'checked' : '' : 'disabled'}><label for="k">Kasko</label></li>
                        <li><input type="checkbox" name="prodotti" value="rc" id="rc" ${p.contains('rc') ? 'checked' : ''}><label for="rc">RC</label></li>
                        <li><input type="checkbox" name="prodotti" value="tutela giudiziaria" id="tg" ${p.contains('tutela giudiziaria') ? 'checked' : ''}><label for="tg">Tutela giudiziaria</label></li>
                        <li><input type="checkbox" name="prodotti" value="infortuni 100.000" id="i100" ${p.contains('infortuni 100.000') ? 'checked' : ''}><label for="i100">Infortuni 100.000</label></li>
                        <li><input type="checkbox" name="prodotti" value="infortuni 200.000" id="i200" ${p.contains('infortuni 200.000') ? 'checked' : ''}><label for="i200">Infortuni 200.000</label></li>
                    </ul>
                </f:field>
                <f:field type="decimal" name="kw" bean="applicazione" required="true"/>
                <f:field type="decimal" name="cv" bean="applicazione" required="true" format="" readonly="true"/>
                <f:field type="text" name="cilindrata" bean="applicazione" required="true"/>
                <f:field type="text" name="nposti" bean="applicazione" label="No. posti" id="nposti"/>
            </div>
            <div id="right">
                <f:field type="select" name="alimentazione"  from="alimentazione" bean="applicazione" required="true"/>
                <f:field type="select" name="veicolo" value="${veicolo ?: params.veicolo}" from="veicoli" required="true"/>
                <div id="marca-modello" class="field">
                    <f:field type="text" name="marca" bean="applicazione" required="true"/>
                    <f:field type="text" name="modello" bean="applicazione" required="true"/>
                </div>
                <f:field type="integer" name="quintali" bean="applicazione" field-id="quintali" id="qli" value="0" required="true"/>
                <f:field type="currency" name="valoreAssicurato" bean="applicazione" value="0,00" required="true"/>
                <f:field type="uppercase" name="targa" bean="applicazione" required="${isPolizza}"/>
                <f:field type="uppercase" name="telaio" bean="applicazione"/>
                <f:field type="date" name="dataImmatricolazione" label="Immatricolato il" bean="applicazione" required="${isPolizza}" maxDate="0"/>
                <f:field type="text" name="vincolo" bean="applicazione"/>
                <g:set var="minDecorrenza" value="${applicazione.applicazioneRinnovata && applicazione.applicazioneRinnovata.dataScadenza > new Date() ? use(groovy.time.TimeCategory) { applicazione.applicazioneRinnovata.dataScadenza + 1.day }.format() : 0}"/>
                <f:field type="date" name="dataDecorrenza" bean="applicazione" label="Decorre dal" value="${minDecorrenza ?: new Date().format()}" required="true" minDate="${minDecorrenza}"/>
                <g:set var="maxScadenza" value="${"31-12-${(minDecorrenza ? minDecorrenza.parseDate() : new Date()).format("yyyy")}".toString()}"/>
                <%--<f:field type="date" name="dataScadenza" bean="applicazione" label="Scade il" value="${maxScadenza}" required="true" minDate="0" maxDate="${maxScadenza}" changeMonth="true"/>--%>
                <f:field type="date" name="dataScadenza" bean="applicazione" label="Scade il" value="${maxScadenza}" required="true" minDate="0" changeMonth="true"/>
                <g:if test="${isPagata}"><f:field type="date" name="dataPagamento" bean="applicazione" label="Pagata il" required="true"/></g:if>
                <g:if test="${isEsclusa}"><f:field type="date" name="dataEsclusione" bean="applicazione" label="Esclusa il" required="true"/></g:if>
                <f:field type="text" name="vincololeasing" label="Vincolo leasing" bean="applicazione"/>
                <f:field type="date" name="scadenzaVincolo" bean="applicazione" label="Scadenza vincolo" value="${maxScadenza}" required="true" minDate="0" changeMonth="true"/>
                <f:field type="textarea" name="note" bean="applicazione"/>
            </div>
            <div id="form-footer">
                <ui:button action="elenco" text="Torna all'elenco" id="back-button" params="${paramsElenco}"/>
                <f:submit name="submit" id="calcola" value="Calcola"/>
                <g:hasErrors bean="${applicazione}" field="calcoli"><div class="flash ui-corner-all error"><g:renderErrors bean="${applicazione}" field="calcoli"/></div></g:hasErrors>
                <g:if test="${applicazione.calcoli}">
                    <%
                        def calcoli = applicazione.calcoli + ["TOTALE": [
                            pcl: applicazione.calcoli.values().pcl.sum(),
                            imponibile: applicazione.calcoli.values().imponibile.sum(),
                            imposte: applicazione.calcoli.values().imposte.sum(),
                            pdu: applicazione.calcoli.values().pdu.sum(),
                            provvigioniVW: applicazione.calcoli.values().provvigioniVW.sum(),
                            provvigioniMach1: applicazione.calcoli.values().provvigioniMach1.sum()
                        ]]
                    %>
                    <grid:table data="${calcoli}" id="calcoli">
                        <grid:column name="key" header="Prodotti" tag="th" class="right-align-column prodotto-column"/>
                        <grid:column header="Premio cliente" class="centered-column">${value.value.pcl.format()} €</grid:column>
                        <grid:column header="Imponibile" class="centered-column">${value.value.imponibile.format()} €</grid:column>
                        <grid:column header="Imposte" class="centered-column">${value.value.imposte.format()} €</grid:column>
                        <g:if test="${session.utente instanceof utenti.Admin}">
                            <grid:column header="PDU" class="centered-column">${value.value.pdu.format()} €</grid:column>
                            <grid:column header="Provvigioni VW" class="centered-column">${value.value.provvigioniVW.format()} €</grid:column>
                            <grid:column header="Provvigioni Mach1" class="centered-column">${value.value.provvigioniMach1.format()} €</grid:column>
                        </g:if>
                    </grid:table>
                    <g:if test="${applicazione.id}">
                        <f:submit name="submit" value="Salva"/>
                    </g:if><g:else>
                    <f:submit name="submit" id="s-c" value="Salva e continua"/>
                    <f:submit name="submit" id="s-e" value="Salva e torna all'elenco"/>
                </g:else>
                </g:if>
            </div>
        </form>
    </body>
</html>