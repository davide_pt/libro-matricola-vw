
%{--<%@ page import="polizze.Alimentazione" contentType="text/html;charset=UTF-8" %>--}%
<g:set var="paramsElenco" value="${params.findAll { k, v -> k in ['max', 'offset', 'sort', 'order', 'q', 'id'] }}"/>
<g:set var="isPolizza" value="${applicazione.stato == polizze.StatoApplicazione.POLIZZA}"/>
<g:set var="isPagata" value="${applicazione.pagata}"/>
<g:set var="isEsclusa" value="${applicazione.stato == polizze.StatoApplicazione.ESCLUSA}"/>
<g:set var="altroVeicolo" value="${libromatricola.Veicolo.findByMarca("Altro")?.id}"/>
<g:set var="destinU" value="${polizze.DestUso.findByNome("Targa prova")?.nome}"/>
<g:set var="destinP" value="${applicazione.destinazioneUso}"/>
<g:set var="destinN" value="${polizze.DestUso.findByNome("Noleggio")?.nome}"/>
<html>
<head>
    <title>${applicazione.id ? "Modifica $applicazione.numero" : 'Preventivo'}</title>
    <r:external file="/less/applicazioni.less"/>
    <r:script>
                $( document ).ready(function() {
                    var valore=jQuery("#tipologiaVeicolo").val();
                    var valdest=jQuery("#destinazioneUso").val();
                    //alert(valdest);
                    if(valore.startsWith('TARGAPROVA')){
                        $('#marca, #modello').attr('readonly',true);
                        //$('#destinazioneUso').select2('data', {id:1, text:'Targa prova'}).attr('disabled','disabled');
                        $duso = $("select[name='destinazioneUso']");
                        $("select[name='destinazioneUso'] option").remove(); //remove all options from Contact names
                        $("<option>Targa prova</option>").appendTo($duso); //add an option to Contact names
                        $('#destinazioneUso').attr('disabled','disabled');
                        $duso.val("Targa prova").change();
                        $("#quintali").hide();
                        $("#veicolo").val("${altroVeicolo}").select2("readonly", true);
                        $("#alimentazione").select2("readonly", true);
                    }else{
                        $duso = $("select[name='destinazioneUso']");
                        $("select[name='destinazioneUso'] option").remove(); //remove all options from Contact names
                        $("<option>Proprio</option>").appendTo($duso); //add an option to Contact names
                        $("<option>Noleggio</option>").appendTo($duso);
                        if("${destinP}"=="Noleggio"){
                            $duso.val("Noleggio").change();
                        }else {
                            $duso.val("Proprio").change();

                        }
                        $('#destinazioneUso').removeAttr('disabled');
                    }
                });
                $("#cvt").change(function() { $("#k").attr("disabled", !this.checked).attr("checked", this.checked ? $("#k").is("checked") : false); });
                $("#i100").change(function() { if(this.checked) $("#i200:checked").attr("checked", false); });
                $("#i200").change(function() { if(this.checked) $("#i100:checked").attr("checked", false); });
                $("#versione-tariffa").change(function() { $.post("${createLink(action: "tariffe")}", {id: this.value}, function(response) {
                    var tariffe = $("#tariffa-id").empty();
                    $.each(response.tariffe, function(index, tariffa) { tariffe.append($("<option>", {value: tariffa.id}).text(tariffa.text)); });
                    //tariffe.select2("val", null);
                    tariffe.change();
                }); });
                $("#dealer-id").change(function() { $.post("${createLink(action: 'dealer')}", {id: this.value}, function(response) {
                    $("#tariffa-id").val(response.tariffaEmissioni).change();
                    $("#provincia").text(response.provincia);
                    $("#numero").text(response.numero);
                }); });
                $("#tipologiaVeicolo").change(function() {
                    var valore=this.value;
                    if(valore.startsWith('AUTOCARRO')){
                        $("#marca, #modello").prop("readonly", false);
                        $("#quintali").show();
                        $("#veicolo").val("null").change().select2("readonly", false);
                        $('#alimentazione').val("null").change().select2("readonly", false);
                        var group = "input:checkbox[name='tariffeACRC']";
                        $(group).attr("disabled", false);
                        //$('#destinazioneUso').select2('data', {id:1, text:'Proprio'},{id:2, text:'Noleggio'}).removeAttr('disabled');
                        $duso = $("select[name='destinazioneUso']");
                        $("select[name='destinazioneUso'] option").remove(); //remove all options
                        $("<option>Proprio</option>").appendTo($duso); //add an option
                        $("<option>Noleggio</option>").appendTo($duso);
                        if("${destinP}"=="Noleggio"){
                            $duso.val("Noleggio").change();
                        }else{
                            $("#destinazioneUso").val("Proprio").change();
                        }
                        $('#destinazioneUso').removeAttr('disabled');
                    }
                    else if(valore.startsWith('TARGAPROVA')){
                        $('#marca, #modello').attr('readonly',true);
                        $('#marca, #modello').attr('required', false);
                        //$('#destinazioneUso').select2('data', {id:1, text:'Targa prova'}).attr('disabled','disabled');
                        $duso = $("select[name='destinazioneUso']");
                        $("select[name='destinazioneUso'] option").remove(); //remove all options
                        $("<option>Targa prova</option>").appendTo($duso); //add an option
                        $('#destinazioneUso').val("Targa prova").change();
                        $('#destinazioneUso').attr('disabled','disabled');
                        $('#alimentazione').select2('data', {id:1, text:'Altro'}).select2("readonly", true);
                        $("#quintali").hide();
                        $("#veicolo").val("${altroVeicolo}").select2("readonly", true);
                        var group = "input:checkbox[name='tariffeACRC']";
                        $(group).attr("disabled", true);
                        $(group).prop("checked", false);
                    }else if(valore.startsWith('MOTO')){
                        $("#marca, #modello").val("").prop("readonly", false);
                        $("#quintali").hide();
                        $("#veicolo").val("null").change().select2("readonly", false);
                        //$('#destinazioneUso').select2('data', {id:1, text:'Proprio'},{id:2, text:'Noleggio'}).removeAttr('disabled');
                        $duso = $("select[name='destinazioneUso']");
                        $("select[name='destinazioneUso'] option").remove(); //remove all options
                        $("<option>Proprio</option>").appendTo($duso); //add an option
                        $("<option>Noleggio</option>").appendTo($duso);
                        $('#destinazioneUso').removeAttr('disabled');
                         if("${destinP}"=="Noleggio"){
                            $duso.val("Noleggio").change();
                        }else{
                            $("#destinazioneUso").val("Proprio").change();
                        }
                        $('#alimentazione').val("null").change().select2("readonly", false);
                        var group = "input:checkbox[name='tariffeACRC']";
                        $(group).attr("disabled", true);
                        $(group).prop("checked", false);
                    }else if(valore.startsWith('AUTOVETTURA')){
                        $("#marca, #modello").val("").prop("readonly", false);
                        $("#quintali").hide();
                        $("#veicolo").val("null").change().select2("readonly", false);
                        //$('#destinazioneUso').select2('data', {id:1, text:'Proprio'},{id:2, text:'Noleggio'}).removeAttr('disabled');
                        $duso = $("select[name='destinazioneUso']");
                        $("select[name='destinazioneUso'] option").remove(); //remove all options from Contact names
                        $("<option>Proprio</option>").appendTo($duso); //add an option to Contact names
                        $("<option>Noleggio</option>").appendTo($duso);
                        $('#destinazioneUso').removeAttr('disabled');
                         if("${destinP}"=="Noleggio"){
                            $duso.val("Noleggio").change();
                        }else{
                            $("#destinazioneUso").val("Proprio").change();
                        }
                        $('#alimentazione').val("null").change().select2("readonly", false);
                        var group = "input:checkbox[name='tariffeACRC']";
                        $(group).attr("disabled", true);
                        $(group).prop("checked", false);
                    }
                    else{
                        $("#quintali").hide();
                        $("#marca, #modello").prop("readonly", false);
                         $("#veicolo").val("null").change().select2("readonly", false);
                         //$('#destinazioneUso').select2('data', {id:1, text:'Proprio'},{id:2, text:'Noleggio'}).removeAttr('disabled');
                         var valdest=jQuery("#destinazioneUso").val();
                         $duso = $("select[name='destinazioneUso']");
                        $("select[name='destinazioneUso'] option").remove(); //remove all options from Contact names
                        $("<option>Proprio</option>").appendTo($duso); //add an option to Contact names
                        $("<option>Noleggio</option>").appendTo($duso);
                        $('#destinazioneUso').removeAttr('disabled');
                         if("${destinP}"=="Noleggio"){
                            $duso.val("Noleggio").change();
                        }else{
                            $("#destinazioneUso").val("Proprio").change();
                        }
                         $('#alimentazione').val("null").change().select2("readonly", false);
                         var group = "input:checkbox[name='tariffeACRC']";
                        $(group).attr("disabled", true);
                        $(group).prop("checked", false);
                    }
                });
                $("#targaProva").change(function() {
                    if(this.checked) {
                        $("#nposti").attr('required', 'required');
                        $("#veicolo").val("${altroVeicolo}").change().select2("readonly", true);
                        $("#marca, #modello").val("").prop("readonly", true);
                    } else {
                        $("#veicolo").val("null").change().select2("readonly", false);
                        $("#marca, #modello").prop("readonly", false);
                    }
                });
                $("#cilindrata").change(function() {
                    var valcil=this.value;
                    var cfval
                    if(valcil !='' && valcil>0) {
                        if(valcil>=19.8 && valcil<=57.1){
                        cfval=1;
                        }else if(valcil>=57.2 && valcil<=106.1){
                        cfval=2;
                        }else if(valcil>=106.2 && valcil<=164.8){
                        cfval=3;
                        }else if(valcil>=164.9 && valcil<=231.8){
                        cfval=4;
                        }else if(valcil>=231.9 && valcil<=306.4){
                        cfval=5;
                        }else if(valcil>=306.5 && valcil<=387.8){
                        cfval=6;
                        }else if(valcil>=387.9 && valcil<=475.6){
                        cfval=7;
                        }else if(valcil>=475.7 && valcil<=569.5){
                        cfval=8;
                        }else if(valcil>=569.6 && valcil<=669.0){
                        cfval=9;
                        }else if(valcil>=669.1 && valcil<=774.0){
                        cfval=10;
                        }else if(valcil>=774.1 && valcil<=884.1){
                        cfval=11;
                        }else if(valcil>=884.2 && valcil<=999.2){
                        cfval=12;
                        }else if(valcil>=999.3 && valcil<=1119.1){
                        cfval=13;
                        }else if(valcil>=1119.2 && valcil<=1243.6){
                        cfval=14;
                        }else if(valcil>=1243.7 && valcil<=1372.5){
                        cfval=15;
                        }else if(valcil>=1372.6 && valcil<=1505.8){
                        cfval=16;
                        }else if(valcil>=1505.9 && valcil<=1643.3){
                        cfval=17;
                        }else if(valcil>=1643.4 && valcil<=1784.9){
                        cfval=18;
                        }else if(valcil>=1785.0 && valcil<=1930.5){
                        cfval=19;
                        }else if(valcil>=1930.6 && valcil<=2080.1){
                        cfval=20;
                        }else if(valcil>=2080.2 && valcil<=2233.4){
                        cfval=21;
                        }else if(valcil>=2233.5 && valcil<=2390.4){
                        cfval=22;
                        }else if(valcil>=2390.5 && valcil<=2551.1){
                        cfval=23;
                        }else if(valcil>=2551.2 && valcil<=2715.4){
                        cfval=24;
                        }else if(valcil>=2715.5 && valcil<=2883.2){
                        cfval=25;
                        }else if(valcil>=2883.3 && valcil<=3054.5){
                        cfval=26;
                        }else if(valcil>=3054.6 && valcil<=3229.1){
                        cfval=27;
                        }else if(valcil>=3229.2 && valcil<=3407.1){
                        cfval=28;
                        }else if(valcil>=3407.2 && valcil<=3588.4){
                        cfval=29;
                        }else if(valcil>=3588.5 && valcil<=3772.8){
                        cfval=30;
                        }else if(valcil>=3772.9 && valcil<=3960.5){
                        cfval=31;
                        }else if(valcil>=3960.6 && valcil<=4151.2){
                        cfval=32;
                        }else if(valcil>=4151.3 && valcil<=4345.1){
                        cfval=33;
                        }else if(valcil>=4345.2 && valcil<=4542.0){
                        cfval=34;
                        }else if(valcil>=4542.1 && valcil<=4741.9){
                        cfval=35;
                        }else if(valcil>=4742.0 && valcil<=4944.7){
                        cfval=36;
                        }else if(valcil>=4944.8 && valcil<=5150.5){
                        cfval=37;
                        }else if(valcil>=5150.6 && valcil<=5359.2){
                        cfval=38;
                        }else if(valcil>=5359.3 && valcil<=5570.7){
                        cfval=39;
                        }else if(valcil>=5570.8 && valcil<=5785.0){
                        cfval=40;
                        }else if(valcil>=5785.1 && valcil<=6002.1){
                        cfval=41;
                        }else if(valcil>=6002.2 && valcil<=6221.9){
                        cfval=42;
                        }else if(valcil>=6222.0 && valcil<=6444.5){
                        cfval=43;
                        }else if(valcil>=6444.6 && valcil<=6669.8){
                        cfval=44;
                        }else if(valcil>=6669.9 && valcil<=6897.7){
                        cfval=45;
                        }else if(valcil>=6897.8 && valcil<=7128.2){
                        cfval=46;
                        }else if(valcil>=7128.3 && valcil<=7361.4){
                        cfval=47;
                        }else if(valcil>=7361.5 && valcil<=7597.2){
                        cfval=48;
                        }else if(valcil>=7597.3 && valcil<=7835.5){
                        cfval=49;
                        }else if(valcil>=7835.6){
                        cfval=50;  }
                        $("#cv").val(cfval);
                    } else {$("#cv").val("inserire un valore numerico nel campo cilindrata");
                    }
                });
                $("#scadenzaVincolo").change(function() {
                    var scadDate=$("#dataScadenza").val().split('-');
                    var dateformat1= new Date(parseInt(scadDate[2], 10), parseInt(scadDate[1], 10)-1, parseInt(scadDate[0], 10));
                    var scade= this.value.split('-');
                    var dateformat2= new Date(parseInt(scade[2], 10), parseInt(scade[1], 10)-1, parseInt(scade[0], 10));
                    /*alert(dateformat1.getTime());
                    alert(dateformat2.getTime());*/
                    if(scade !=''){
                        if(dateformat2>dateformat1){
                            $(".messaggio").after("<div class=\"flash ui-corner-all error\">la data di scadenza vincolo non puo\' essere superiore alla data di scadenza della polizza</div>");
                        }
                    }
                });
                $("input:checkbox[name='tariffeACRC']").on('click', function() {
                      var $box = $(this);
                      if ($box.is(":checked")) {
                        var group = "input:checkbox[name='tariffeACRC']";
                        $(group).prop("checked", false);
                        $box.prop("checked", true);
                      } else {
                        $box.prop("checked", false);
                      }
                });
                $("input:checkbox[id='cvt']").on('click', function() {
                      var $box = $(this);
                      if (!$box.is(":checked")) {
                         $("#valoreAssicurato").prop("readonly", true);
                      } else {
                          $("#valoreAssicurato").prop("readonly", false);
                      }
                });
        <g:if test="${applicazione.tipologiaVeicolo?.name()?.startsWith('AUTOCARRO')}">$("#quintali").show();</g:if>
        $("#veicolo").change(function() { $.post("${createLink(action: 'veicolo')}", {id: this.value, targaProva: $("#targaProva").is(":checked")}, function(response) { $("#marca-modello").html(response); }); })<%--<g:if test="${!applicazione.marca}">.change()</g:if>--%>;
        <g:if test="${applicazione.calcoli}">
            $(":input", "#applicazione-form").change(function() { $("#calcola").click(); });
        </g:if>
    </r:script>
</head>
<body>
<form action="${createLink(action: 'salva', params: paramsElenco + [app: params.app])}" method="post" id="applicazione">
    <flash:output/>
    <div class="messaggio"></div>
    <div id="legend">
        <label>(<span>*</span>) campo obbligatorio</label>
    </div>
    <div id="left">
        <f:field type="hidden" name="applicazioneEsclusa.id" bean="applicazione"/>
        <g:if test="${session.utente instanceof utenti.Admin}">
            <g:if test="${dealers}">
                <f:field type="select" name="dealer.id" bean="applicazione" label="Dealer" from="dealers" optionKey="id" optionValue="ragioneSociale" required="true"/>
                <f:field type="output" name="dealer.numeroLibroMatricola" label="Numero libro matricola" bean="applicazione" id="numero" value="Scegli un dealer"/>
                <f:field type="select" id="versione-tariffa" label="Versione tariffa" from="versioniTariffa" value="${applicazione.tariffa?.versione}" first="false"/>
                <f:field type="select" name="tariffa.id" bean="applicazione" label="Tariffa" from="tariffe" required="true"/>
                <f:field type="output" name="provincia" bean="applicazione" value="Scegli un dealer"/>
            </g:if><g:else>
            <f:field type="output" label="Dealer" value="${dealer}"/>
            <f:field type="output" label="Numero libro matricola" value="${dealer?.numeroLibroMatricola}"/>
            <f:field type="select" id="versione-tariffa" label="Versione tariffa" from="versioniTariffa" value="${applicazione.tariffa?.versione}" first="false"/>
            <f:field type="select" name="tariffa.id" bean="applicazione" label="Tariffa" from="tariffe" required="true"/>
            <f:field type="output" name="provincia" bean="applicazione"/>
        </g:else>
        </g:if>

        <f:field type="select" name="tipologiaVeicolo" bean="applicazione" from="tipologieVeicolo" first="false" id="tipologiaVeicolo"/>
        <f:field type="select" name="destinazioneUso"  bean="applicazione" from="destinazioniUso" id="destinazioneUso" first="false"/>

    %{--NOVEMBRE 2016--}%
    %{-- <f:field type="checkbox" name="targaProva" bean="applicazione"/>--}%
        <f:field type="custom" name="prodotti" required="true">
            <ul id="prodotti">
                <g:set var="p" value="${prodotti ?: params.list('prodotti')}"/>
                <li><input type="checkbox" name="prodotti" value="cvt" id="cvt" ${p.contains('cvt') ? 'checked' : ''}><label for="cvt">CVT</label></li>
                <li><input type="checkbox" name="prodotti" value="kasko" id="k" ${p.contains('cvt') ? p.contains('kasko') ? 'checked' : '' : 'disabled'}><label for="k">Kasko</label></li>
                <li><input type="checkbox" name="prodotti" value="rc" id="rc" ${p.contains('rc') ? 'checked' : ''}><label for="rc">RC</label></li>
                <li><input type="checkbox" name="prodotti" value="tutela giudiziaria" id="tg" ${p.contains('tutela giudiziaria') ? 'checked' : ''}><label for="tg">Tutela giudiziaria</label></li>
                <li><input type="checkbox" name="prodotti" value="infortuni 100.000" id="i100" ${p.contains('infortuni 100.000') ? 'checked' : ''}><label for="i100">Infortuni 100.000</label></li>
                <li><input type="checkbox" name="prodotti" value="infortuni 200.000" id="i200" ${p.contains('infortuni 200.000') ? 'checked' : ''}><label for="i200">Infortuni 200.000</label></li>
            </ul>
        </f:field>
       <f:field type="custom" name="tariffeACRC"  label="Tariffe autocarri" required="true">
                <g:set var="trc" value="${tariffeACRC ?: params.list('tariffeACRC')}"/>
                <%System.out.println "applicazione ${trc}"%>
                <%System.out.println "applicazione ${applicazione.tipologiaVeicolo}"%>
            <ul id="tariffeACRC">
            <g:if test="${(trc.contains('T1') || trc.contains('T2') || trc.contains('T3')) && applicazione.tipologiaVeicolo == libromatricola.TipologiaVeicolo.AUTOCARRO}" >
                <li id="tar"><input type="checkbox" class="radio" name="tariffeACRC" value="T1" id="t1" ${trc.contains('T1') ? 'checked' : ''}  ><label for="t1">T1</label></li>
                <li id="tar"><input type="checkbox" class="radio" name="tariffeACRC" value="T2" id="t2" ${trc.contains('T2') ? 'checked' : ''}  ><label for="t2">T2</label></li>
                <li id="tar"><input type="checkbox" class="radio" name="tariffeACRC" value="T3" id="t3" ${trc.contains('T3') ? 'checked' : ''}  ><label for="t3">T3</label></li>
            </g:if>
            <g:elseif test="${applicazione.tipologiaVeicolo == libromatricola.TipologiaVeicolo.AUTOCARRO}">
                <li id="tar"><input type="checkbox" class="radio" name="tariffeACRC" value="T1" id="t1" ${trc.contains('T1') ? 'checked' : ''}  ><label for="t1">T1</label></li>
                <li id="tar"><input type="checkbox" class="radio" name="tariffeACRC" value="T2" id="t2" ${trc.contains('T2') ? 'checked' : ''}  ><label for="t2">T2</label></li>
                <li id="tar"><input type="checkbox" class="radio" name="tariffeACRC" value="T3" id="t3" ${trc.contains('T3') ? 'checked' : ''}  ><label for="t3">T3</label></li>
            </g:elseif>
            <g:else>
                <li id="tar"><input type="checkbox" class="radio" name="tariffeACRC" value="T1" id="t1" ${trc.contains('T1') ? 'checked' : ''} disabled ><label for="t1">T1</label></li>
                <li id="tar"><input type="checkbox" class="radio" name="tariffeACRC" value="T2" id="t2" ${trc.contains('T2') ? 'checked' : ''} disabled ><label for="t2">T2</label></li>
                <li id="tar"><input type="checkbox" class="radio" name="tariffeACRC" value="T3" id="t3" ${trc.contains('T3') ? 'checked' : ''} disabled ><label for="t3">T3</label></li>
            </g:else>

                    </ul>
        </f:field>
        <f:field type="decimal" name="kw" bean="applicazione" required="true"/>
        <f:field type="decimal" name="cilindrata" bean="applicazione" required="true"/>
        <f:field type="decimal" name="cv" bean="applicazione" label="CF" required="true" format="" readonly="true"/>
        <f:field type="text" name="nposti" bean="applicazione" label="No. posti" id="nposti"/>

    </div>
    <div id="right">
        <f:field type="select" name="alimentazione" from="alimentazione" id="alimentazione" bean="applicazione" required="true"/>
        <f:field type="select" name="veicolo" value="${veicolo ?: params.veicolo}" from="veicoli" required="true"/>
        <div id="marca-modello" class="field">
            <f:field type="text" name="marca" bean="applicazione" label="Marca"  required="true" id="marca"/>
            <f:field type="text" name="modello" bean="applicazione" required="true" id="modello"/>
        </div>
        <f:field type="integer" name="quintali" bean="applicazione" field-id="quintali" id="qli" value="0" required="true"/>
        <f:field type="currency" name="valoreAssicurato" bean="applicazione" value="0,00" required="true"/>
        <f:field type="uppercase" name="targa" bean="applicazione" required="${isPolizza}"/>
        <f:field type="uppercase" name="telaio" bean="applicazione"/>
        <f:field type="date" name="dataImmatricolazione" label="Immatricolato il" bean="applicazione" required="${isPolizza}" maxDate="0"/>
        <f:field type="text" name="vincolo" bean="applicazione"/>
        <g:set var="minDecorrenza" value="${applicazione.applicazioneEsclusa && applicazione.applicazioneEsclusa.dataEsclusione > new Date() ? applicazione.applicazioneEsclusa.dataEsclusione.format() : 0}"/>
        <%--<f:field type="date" name="dataDecorrenza" bean="applicazione" label="Decorre dal" value="${minDecorrenza ?: new Date().format()}" required="true" minDate="${minDecorrenza}" maxDate="31-12-2014"/>--%>
        <f:field type="date" name="dataDecorrenza" bean="applicazione" label="Decorre dal" value="${minDecorrenza ?: new Date().format()}" required="true" minDate="${minDecorrenza}"/>
        <g:set var="maxScadenza" value="${"31-12-${(minDecorrenza ?: new Date().format()).parseDate().format("yyyy")}".toString()}"/>
        <%--<f:field type="date" name="dataScadenza" bean="applicazione" label="Scade il" value="${maxScadenza}" required="true" minDate="0" maxDate="${maxScadenza}" changeMonth="true"/>--%>
        <f:field type="date" name="dataScadenza" bean="applicazione" label="Scade il" value="${maxScadenza}" required="true" minDate="0" changeMonth="true" changeYear="true"/>
        <g:if test="${isPagata}"><f:field type="date" name="dataPagamento" bean="applicazione" label="Pagata il" required="true"/></g:if>
        <g:if test="${isEsclusa}"><f:field type="date" name="dataEsclusione" bean="applicazione" label="Esclusa il" required="true"/></g:if>
        <f:field type="text" name="vincololeasing" bean="applicazione" label="Vincolo leasing"/>
        <f:field type="date" name="scadenzaVincolo" label="Scadenza vincolo" bean="applicazione" value="" minDate="${minDecorrenza}" changeMonth="true" changeYear="true"/>
        <f:field type="textarea" name="note" bean="applicazione"/>

    </div>
    <div id="form-footer">
        <ui:button action="elenco" text="Torna all'elenco" id="back-button" params="${paramsElenco}"/>
        <f:submit name="submit" id="calcola" value="Calcola"/>
        <g:hasErrors bean="${applicazione}" field="calcoli"><div class="flash ui-corner-all error"><g:renderErrors bean="${applicazione}" field="calcoli"/></div></g:hasErrors>
        <g:if test="${applicazione.calcoli}">
            <%
                def calcoli = applicazione.calcoli + ["TOTALE": [
                        pcl: applicazione.calcoli.values().pcl.sum(),
                        imponibile: applicazione.calcoli.values().imponibile.sum(),
                        imposte: applicazione.calcoli.values().imposte.sum(),
                        pdu: applicazione.calcoli.values().pdu.sum(),
                        provvigioniVW: applicazione.calcoli.values().provvigioniVW.sum(),
                        provvigioniMach1: applicazione.calcoli.values().provvigioniMach1.sum()
                ]]
            %>
            <grid:table data="${calcoli}" id="calcoli">
                <grid:column name="key" header="Prodotti" tag="th" class="right-align-column prodotto-column"/>
                <grid:column header="Premio cliente" class="centered-column">${value.value.pcl.format()} €</grid:column>
                <grid:column header="Imponibile" class="centered-column">${value.value.imponibile.format()} €</grid:column>
                <grid:column header="Imposte" class="centered-column">${value.value.imposte.format()} €</grid:column>
                <g:if test="${session.utente instanceof utenti.Admin}">
                    <grid:column header="PDU" class="centered-column">${value.value.pdu.format()} €</grid:column>
                    <grid:column header="Provvigioni VW" class="centered-column">${value.value.provvigioniVW.format()} €</grid:column>
                    <grid:column header="Provvigioni Mach1" class="centered-column">${value.value.provvigioniMach1.format()} €</grid:column>
                </g:if>
            </grid:table>
            <g:if test="${applicazione.id}">
                <f:submit name="submit" value="Salva"/>
            </g:if><g:else>
            <f:submit name="submit" id="s-c" value="Salva e continua"/>
            <f:submit name="submit" id="s-e" value="Salva e torna all'elenco"/>
        </g:else>
        </g:if>
    </div>
</form>
</body>
</html>