
<%@ page contentType="text/html;charset=UTF-8" %>
<g:set var="paramsElenco" value="${params.findAll { k, v -> k in ['max', 'offset', 'sort', 'order', 'q', 'id'] }}"/>
<g:set var="today" value="${new Date().clearTime()}"/>
<html>
<head>
    <title>Emissione applicazione</title>
    <r:external file="/less/applicazioni.less"/>
    <r:external file="/js/moment.min.js"/>
    <r:script>
        moment.lang("it");
        $("#dataDecorrenza").change(function() { $("#dataScadenza").datepicker("option", "minDate", moment(this.value, "DD-MM-YYYY").add("d", 1).format("DD-MM-YYYY")); });
    </r:script>
</head>
<body>
<form action="${createLink(action: 'emetti', params: paramsElenco + [app: params.app])}" method="post" id="applicazione">
    <flash:output/>
    <div id="legend">
        <label>(<span>*</span>) campo obbligatorio</label>
    </div>
    <div id="left">
        <g:if test="${session.utente instanceof utenti.Admin}">
            <f:field type="output" name="dealer" bean="applicazione"/>
            <f:field type="output" name="dealer.numeroLibroMatricola" label="Numero libro matricola" bean="applicazione" id="numero"/>
            <f:field type="output" label="Tariffa" value="${applicazione.tariffa.codice + " (" + applicazione.tariffa.versione + ")"}"/>
        </g:if>
        <f:field type="output" name="provincia" bean="applicazione"/>
        <f:field type="output" name="tipologiaVeicolo" bean="applicazione"/>
        <f:field type="output" name="destinazioneUso"  bean="applicazione" />
        <f:field type="output" name="tariffaAutocarri" bean="applicazione"/>
        <f:field type="output" name="marca" bean="applicazione"/>
        <f:field type="output" name="modello" bean="applicazione"/>
        <f:field type="output" name="rischio" bean="applicazione"/>
        <f:field type="output" name="kw" bean="applicazione" required="true"/>
        <f:field type="output" name="cv" bean="applicazione" required="true" format="" readonly="true"/>
        <f:field type="output" name="cilindrata" bean="applicazione" required="true"/>
        <f:field type="output" name="nposti" bean="applicazione"/>
    </div>
    <div id="right">
        <f:field type="output" name="alimentazione" bean="applicazione"/>
        <f:field type="output" name="valoreAssicurato" bean="applicazione" class="currency"/>
        <f:field type="uppercase" name="targa" bean="applicazione" required="true"/>
        <f:field type="uppercase" name="telaio" bean="applicazione"/>
        <f:field type="date" name="dataImmatricolazione" bean="applicazione" required="true" maxDate="0"/>
        <f:field name="vincolo" bean="applicazione"/>
        <g:set var="minDecorrenza" value="${"01-01-${applicazione.dataDecorrenza.format("yyyy")}".toString()}"/>
        <f:field type="date" name="dataDecorrenza" bean="applicazione" label="Decorre dal" value="${new Date().format()}" required="true" minDate="${minDecorrenza}"/>
        <g:set var="maxScadenza" value="${"31-12-${minDecorrenza.parseDate().format("yyyy")}".toString()}"/>
        <%--<f:field type="date" name="dataScadenza" bean="applicazione" label="Scade il" value="${maxScadenza}" required="true" minDate="0" maxDate="${maxScadenza}" changeMonth="true"/>--%>
        <f:field type="date" name="dataScadenza" bean="applicazione" label="Scade il" value="${maxScadenza}" required="true" minDate="0" changeMonth="true"/>
        <f:field name="vincololeasing" bean="applicazione"/>
        <f:field type="date" name="scadenzaVincolo" bean="applicazione" label="Scadenza vincolo" value="${maxScadenza}" required="true" minDate="0" changeMonth="true"/>
        <f:field type="textarea" name="note" bean="applicazione"/>
    </div>
    <div id="form-footer">
        <%
            def calcoli = applicazione.calcoli + ["TOTALE": [
                    pcl: applicazione.calcoli.values().pcl.sum(),
                    imponibile: applicazione.calcoli.values().imponibile.sum(),
                    imposte: applicazione.calcoli.values().imposte.sum(),
                    pdu: applicazione.calcoli.values().pdu.sum(),
                    provvigioniVW: applicazione.calcoli.values().provvigioniVW.sum(),
                    provvigioniMach1: applicazione.calcoli.values().provvigioniMach1.sum()
            ]]
        %>
        <grid:table data="${calcoli}" id="calcoli" caption="Prodotti">
            <grid:column name="key" header="Prodotti" tag="th" class="right-align-column prodotto-column"/>
            <grid:column header="Premio cliente" class="centered-column">${value.value.pcl.format()} €</grid:column>
            <grid:column header="Imponibile" class="centered-column">${value.value.imponibile.format()} €</grid:column>
            <grid:column header="Imposte" class="centered-column">${value.value.imposte.format()} €</grid:column>
            <g:if test="${session.utente instanceof utenti.Admin}">
                <grid:column header="PDU" class="centered-column">${value.value.pdu.format()} €</grid:column>
                <grid:column header="Provvigioni VW" class="centered-column">${value.value.provvigioniVW.format()} €</grid:column>
                <grid:column header="Provvigioni Mach1" class="centered-column">${value.value.provvigioniMach1.format()} €</grid:column>
            </g:if>
        </grid:table>
        <ui:button action="elenco" text="Torna all'elenco" id="back-button" params="${paramsElenco}"/>
        <f:submit value="Emetti"/>
    </div>
</form>
</body>
</html>