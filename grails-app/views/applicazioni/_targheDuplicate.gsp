
<p>Sono gia presenti nel sistema applicazioni per la targa <b>${targa}</b>:</p>
<table class="grid-table" id="duplicati">
    <thead>
        <tr>
            <th>Numero</th>
            <th>Stato</th>
        </tr>
    </thead>
    <tbody>
        <g:each var="duplicato" in="${targheDuplicate}">
            <tr>
                <td><ui:link action="mostra" params="${[app: duplicato.id]}" text="${duplicato.numero}" target="_blank"/></td>
                <td>${duplicato.stato}</td>
            </tr>
        </g:each>
    </tbody>
</table>