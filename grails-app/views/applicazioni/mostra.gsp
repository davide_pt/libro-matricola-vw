
<%@ page import="polizze.StatoApplicazione" contentType="text/html;charset=UTF-8" %>
<g:set var="isAdmin" value="${session.utente instanceof utenti.Admin}"/>
<g:set var="paramsElenco" value="${params.findAll { k, v -> k in ['max', 'offset', 'sort', 'order', 'q', 'id', 'app'] }}"/>
<html>
    <head>
        <title>Applicazione</title>
        <r:external file="/less/applicazioni.less"/>
    </head>
    <body>
        <div id="applicazione" class="mostra">
            <div id="left">
                <g:if test="${isAdmin}">
                    <f:field type="output" name="dealer" bean="applicazione"/>
                    <f:field type="output" name="dealer.numeroLibroMatricola" label="Numero libro matricola" bean="applicazione" id="numero"/>
                    <f:field type="output" label="Tariffa" value="${applicazione.tariffa.codice + " (" + applicazione.tariffa.versione + ")"}"/>
                    <f:field type="output" name="provincia" bean="applicazione"/>
                    %{--<f:field type="output" name="provincia.zona" bean="applicazione"/>--}%
                </g:if>
                <f:field type="output" name="tipologiaVeicolo" bean="applicazione"/>
                <f:field type="output" name="destinazioneUso" bean="applicazione"/>
                <f:field type="output" name="tariffaAutocarri" bean="applicazione"/>
               %{-- <f:field type="output" label="Targa prova" value="${applicazione.targaProva ? "Si" : "No"}"/>--}%
                <f:field type="output" name="marca" bean="applicazione"/>
                <f:field type="output" name="modello" bean="applicazione"/>
                <f:field type="output" name="rischio" bean="applicazione"/>
                <f:field type="output" name="kw" bean="applicazione"/>
                <f:field type="output" name="cv" bean="applicazione"/>
                <f:field type="output" name="cilindrata" bean="applicazione"/>
                <f:field type="output" name="nposti" bean="applicazione"/>
            </div>
            <div id="right">
                <f:field type="output" name="alimentazione" bean="applicazione"/>
                <g:if test="${applicazione.tipologiaVeicolo.name().startsWith('AUTOCARRO')}"><f:field type="output" name="quintali" bean="applicazione"/></g:if>
                <f:field type="output" label="Valore assicurato" value="${applicazione.valoreAssicurato.format()} €"/>
                <f:field type="output" name="targa" bean="applicazione" value="&nbsp;"/>
                <f:field type="output" name="telaio" bean="applicazione" value="&nbsp;"/>
                <f:field type="output" name="dataImmatricolazione" bean="applicazione" value="&nbsp;" maxDate="0"/>
                <f:field type="output" name="vincolo" bean="applicazione" value="&nbsp;"/>
                <f:field type="output" name="vincololeasing"  label="Vincolo leasing" bean="applicazione" value="&nbsp;"/>
                <f:field type="output" name="scadenzaVincolo" bean="applicazione" value="&nbsp;" maxDate="0"/>
                <g:if test="${isAdmin}">
                    <f:field type="output" name="stato" bean="applicazione"/>
                </g:if>
                <g:if test="${applicazione.stato != StatoApplicazione.PREVENTIVO}">
                    <f:field type="output" name="numero" bean="applicazione"/>
                    <f:field type="output" name="dataDecorrenza" bean="applicazione" value="&nbsp;"/>
                    <f:field type="output" name="dataScadenza" bean="applicazione" value="&nbsp;"/>
                    <g:if test="${applicazione.pagata}"><f:field type="output" name="dataPagamento" bean="applicazione" value="&nbsp;"/></g:if>
                    <g:if test="${applicazione.stato == StatoApplicazione.ESCLUSA}"><f:field type="output" name="dataEsclusione" bean="applicazione" value="&nbsp;"/></g:if>
                </g:if>
                <f:field type="output" name="note" bean="applicazione" renderAs="textarea"/>
                <g:if test="${applicazione.applicazioneEsclusa}"><f:field type="custom" label="Riattivazione di"><ui:link action="mostra" params="${[id: applicazione.dealer.id, app: applicazione.applicazioneEsclusa.id]}" text="${applicazione.applicazioneEsclusa.numero}"/></f:field></g:if>
                %{--<g:set var="riattivata" value="${polizze.Applicazione.findByApplicazioneEsclusa(applicazione)}"/>
                <g:if test="${riattivata}"><f:field type="custom" label="Riattivata come"><ui:link action="mostra" params="${[id: riattivata.dealer.id, app: riattivata.id]}" text="${riattivata.numero}" class="field-content"/></f:field></g:if>
                <g:if test="${applicazione.applicazioneRinnovata}"><f:field type="custom" label="Rinnovo di"><ui:link action="mostra" params="${[id: applicazione.dealer.id, app: applicazione.applicazioneRinnovata.id]}" text="${applicazione.applicazioneRinnovata.numero}"/></f:field></g:if>
                <g:set var="rinnovata" value="${polizze.Applicazione.findByApplicazioneRinnovataAndStato(applicazione, StatoApplicazione.POLIZZA)}"/>
                <g:if test="${rinnovata}"><f:field type="custom" label="Rinnovata come"><ui:link action="mostra" params="${[id: rinnovata.dealer.id, app: rinnovata.id]}" text="${rinnovata.numero}" class="field-content"/></f:field></g:if>--}%
            </div>
            <div id="form-footer">
                <%
                    def calcoli = applicazione.calcoli + ["TOTALE": [
                        pcl: applicazione.calcoli.values().pcl.sum(),
                        imponibile: applicazione.calcoli.values().imponibile.sum(),
                        imposte: applicazione.calcoli.values().imposte.sum(),
                        pdu: applicazione.calcoli.values().pdu.sum(),
                        provvigioniVW: applicazione.calcoli.values().provvigioniVW.sum(),
                        provvigioniMach1: applicazione.calcoli.values().provvigioniMach1.sum()
                    ]]
                %>
                <grid:table data="${calcoli}" id="calcoli" caption="Prodotti">
                    <grid:column name="key" header="Prodotti" tag="th" class="right-align-column prodotto-column"/>
                    <grid:column header="Premio cliente" class="centered-column">${value.value.pcl.format()} €</grid:column>
                    <grid:column header="Imponibile" class="centered-column">${value.value.imponibile.format()} €</grid:column>
                    <grid:column header="Imposte" class="centered-column">${value.value.imposte.format()} €</grid:column>
                    <g:if test="${isAdmin}">
                        <grid:column header="PDU" class="centered-column">${value.value.pdu.format()} €</grid:column>
                        <grid:column header="Provvigioni VW" class="centered-column">${value.value.provvigioniVW.format()} €</grid:column>
                        <grid:column header="Provvigioni Mach1" class="centered-column">${value.value.provvigioniMach1.format()} €</grid:column>
                    </g:if>
                </grid:table>
                <grid:table data="${applicazione.tracciati?.sort { a, b -> a.operazione <=> b.operazione ?: a.tipo <=> b.tipo } ?: []}" id="tracciati" caption="Tracciati" emptyMessage="Non sono presenti tracciati per questa applicazione">
                    <grid:column name="dataEsportazione" header="Esportato il" emptyMessage="Non ancora esportato" th-class="data-esportazione-column"/>
                    <grid:column name="garanzia" th-class="garanzia-column"/>
                    <grid:column name="operazione" th-class="small-column"/>
                    <grid:column name="tipo" th-class="tipo-column"/>
                    <grid:column td-class="tracciato-column" var="tracciato">
                        <button class="mostra-tracciato" title="Mostra tracciato scomposto" data-id="${tracciato.id}"></button> ${tracciato.tracciato} <div id="tracciato-${tracciato.id}" class="tracciato-dialog" title="Tracciato #${tracciato.id}"></div>
                    </grid:column>
                </grid:table>
                <g:if test="${applicazione.tracciAssicur}">
                    <grid:table data="${applicazione.tracciAssicur?.sort { a, b -> a.operazione <=> b.operazione ?: a.tipo <=> b.tipo } ?: []}" id="tracciati" caption="Tracciati Annullamenti IAssicur " emptyMessage="Non sono presenti tracciati di annullamento per questa applicazione">
                        <grid:column name="dataEsportazione" header="Esportato il" emptyMessage="Non ancora esportato" th-class="data-esportazione-column"/>
                        <grid:column name="garanzia" th-class="garanzia-column"/>
                        <grid:column name="operazione" th-class="small-column"/>
                        <grid:column name="tipo" th-class="tipo-column"/>
                        <grid:column td-class="tracciato-column" var="tracciato">
                            <button class="mostra-tracciatoAnn" title="Mostra tracciato scomposto" data-id="${tracciato.id}"></button> ${tracciato.tracciato} <div id="tracciato-${tracciato.id}" class="tracciato-dialog" title="Tracciato #${tracciato.id}"></div>
                        </grid:column>
                    </grid:table>
                </g:if>

                <ui:button action="elenco" text="Torna all'elenco" id="back-button" params="${paramsElenco}"/>
                <ui:button action="modifica" text="Modifica" id="edit-button" params="${paramsElenco}"/>
            </div>
        </div>
        <r:script>
            $(".mostra-tracciato").button({icons: {primary: "ui-icon-search"}, text: false}).click(function() {
                var id = $(this).data("id");
                $.post("${createLink(action: "mostraTracciato")}", {id: id}, function(table) { $("#tracciato-" + id).html(table).dialog("open"); });
            });
            $(".mostra-tracciatoAnn").button({icons: {primary: "ui-icon-search"}, text: false}).click(function() {
                var id = $(this).data("id");
                $.post("${createLink(action: "mostraTracciatoAnn")}", {id: id}, function(table) { $("#tracciato-" + id).html(table).dialog("open"); });
            });
            $(".tracciato-dialog").dialog({autoOpen: false, modal: true, width: 700, height: 600, resizable: false});
        </r:script>
    </body>
</html>