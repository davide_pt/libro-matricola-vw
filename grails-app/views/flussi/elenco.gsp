
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title>Flussi</title>
        <style>
            .grid-table {
                width: 80%;
                margin: 0 auto;
                table-layout: fixed;
            }
            .short-column { width: 110px; }
        </style>
    </head>
    <body>
        <grid:table data="flussi">
            <grid:column name="dateCreated" header="Creato il" class="short-column centered-column" sortable="true"/>
            <grid:column name="size" header="Dimensione" class="short-column centered-column" sortable="true">${value} KB</grid:column>
            <grid:column header="Flusso"><ui:link action="download" params="${[id: value.id]}" text="${value.fileName}"/></grid:column>
        </grid:table>
    </body>
</html>